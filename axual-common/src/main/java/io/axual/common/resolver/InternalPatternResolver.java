package io.axual.common.resolver;

/*-
 * ========================LICENSE_START=================================
 * axual-common
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringSubstitutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.axual.common.exception.ConfigurationException;
import io.axual.common.exception.PatternException;
import io.axual.common.exception.ResolveException;
import io.axual.common.tools.MapUtil;

class InternalPatternResolver {
    public static final String DEFAULT_PREFIX = "default.";

    private static final Logger LOG = LoggerFactory.getLogger(InternalPatternResolver.class);
    private static final String EMPTY_STRING = "";
    private static final String REPLACEMENT_PATTERN = "(.*)";
    private static final String PLACEHOLDER_PREFIX = "{";
    private static final String PLACEHOLDER_SUFFIX = "}";
    private static final String ALPHANUM = "a-zA-Z0-9_";
    private static final String ALPHANUM_DOTS_AND_DASHES = ALPHANUM + "\\.\\-";
    private static final String REGEX_ALPHANUM = "([" + ALPHANUM + "]+?)";
    private static final String REGEX_ALPHANUM_DOTS_AND_DASHES = "([" + ALPHANUM_DOTS_AND_DASHES + "]+?)";

    private final String patternConfigKey;
    private final String defaultPlaceholderValue;

    private Map<String, String> context;
    private String pattern;

    private ResolveParams resolverParams = null;
    private UnresolveParams unresolveParams = null;

    private static class ResolveParams {
        private final String prefix;
        private final String suffix;

        public ResolveParams(String prefix, String suffix) {
            this.prefix = prefix;
            this.suffix = suffix;
        }
    }

    private static class UnresolveParams {
        private final List<String> placeholders;
        private final Map<String, String> fixedContext;
        private final Pattern unresolvePattern;

        public UnresolveParams(final List<String> placeholders, final Map<String, String> fixedContext, String regex) {
            this.placeholders = Collections.unmodifiableList(placeholders);
            this.fixedContext = Collections.unmodifiableMap(fixedContext);
            this.unresolvePattern = Pattern.compile(regex);
        }
    }

    InternalPatternResolver(final String patternConfigKey, final String defaultPlaceholderValue) {
        this.patternConfigKey = patternConfigKey;
        this.defaultPlaceholderValue = defaultPlaceholderValue;
    }

    void configure(Map<String, Object> configs) {
        // Save the String values as context for this resolver
        context = Collections.unmodifiableMap(MapUtil.stringValues(configs));

        final Object patternConfig = configs.get(patternConfigKey);
        if (!(patternConfig instanceof String)) {
            throw new ConfigurationException("Pattern field \"" + patternConfigKey + "\" not set");
        }

        pattern = (String) patternConfig;
        resolverParams = null;
        unresolveParams = null;

        prepareResolving();
    }

    private void prepareResolving() {
        if (resolverParams == null) {
            // Configure prefix and suffix for duplicate resolving detection
            final String resolvedName = resolveInternal(REPLACEMENT_PATTERN);
            final String[] splits = StringUtils.splitByWholeSeparator(resolvedName, REPLACEMENT_PATTERN);

            if (resolvedName.startsWith(REPLACEMENT_PATTERN)) {
                resolverParams = new ResolveParams(EMPTY_STRING, splits[0]);
            } else {
                resolverParams = new ResolveParams(
                        splits[0],
                        splits.length > 1 ? splits[1] : EMPTY_STRING);
            }
        }
    }

    private void prepareUnresolving() {
        if (unresolveParams == null) {
            // Build up the regular expression to match the resolvedName to
            final List<String> placeholders = new ArrayList<>();
            final Map<String, String> unresolveContext = new HashMap<>();
            final String regex = buildRegex(placeholders, unresolveContext);

            unresolveParams = new UnresolveParams(placeholders, unresolveContext, regex);
        }
    }


    String resolve(String name) {
        String result = resolveInternal(name);
        if (result.contains(PLACEHOLDER_PREFIX) || result.contains(PLACEHOLDER_SUFFIX)) {
            return null;
        }

        LOG.debug("Resolved pattern {} into {}", pattern, result);
        LOG.debug("Context: {}", context);
        return result;
    }

    // Unresolve (parse) the resolved name by using the configured pattern and provided context
    Map<String, String> unresolve(String name) {
        try {
            Map<String, String> result = unresolveInternal(name);

            if (result != null) {
                // Add all default entries for the placeholders that weren't unresolved
                MapUtil.putAllIfAbsent(result, MapUtil.unprefixItems(context, DEFAULT_PREFIX));
                LOG.debug("Unresolved name {} with pattern {} into {}", name, pattern, result);
            }
            return result;
        } catch (PatternException e) {
            return null;
        }
    }

    private String resolveInternal(String unresolvedName) {
        if (unresolvedName == null) {
            throw new ResolveException(defaultPlaceholderValue + " can not be empty");
        }

        if (resolverParams != null &&
                ((resolverParams.prefix.length() > 0 && unresolvedName.startsWith(resolverParams.prefix)) ||
                        (resolverParams.suffix.length() > 0 && unresolvedName.endsWith(resolverParams.suffix)))) {
            throw new ResolveException("Name is already resolved");
        }

        // Set up the context for resolving
        Map<String, String> values = new HashMap<>(context);
        // Fill in all defaults if not provided in the context (ie. default.environment is copied to
        MapUtil.putAllIfAbsent(values, MapUtil.unprefixItems(context, DEFAULT_PREFIX));
        values.put(defaultPlaceholderValue, unresolvedName);
        return new StringSubstitutor(values, PLACEHOLDER_PREFIX, PLACEHOLDER_SUFFIX).replace(pattern);
    }

    private Map<String, String> unresolveInternal(String resolvedName) {
        prepareUnresolving();

        Matcher m = unresolveParams.unresolvePattern.matcher(resolvedName);
        if (!m.matches()) {
            return null;
        }
        if (m.groupCount() != unresolveParams.placeholders.size()) {
            throw new PatternException("Illegal pattern", pattern);
        }

        int groupIndex = 0;
        final Map<String, String> result = new HashMap<>(unresolveParams.fixedContext);
        for (String placeholder : unresolveParams.placeholders) {
            String value = m.group(++groupIndex);
            result.put(placeholder, value);
        }

        return result;
    }

    private String buildRegex(List<String> placeholders, Map<String, String> matchContext) {
        final List<String> patternElements = parsePattern(pattern);

        // Build a regex to pattern-match the resolvedName
        StringBuilder regexBuilder = new StringBuilder();
        regexBuilder.append("^");
        for (String element : patternElements) {
            if (elementIsPlaceholder(element)) {
                String placeholderName = getPlaceholderName(element);
                // If the element is the placeholder (eg. "group" or "topic") then allow dashes
                if (placeholderName.equals(defaultPlaceholderValue)) {
                    regexBuilder.append(REGEX_ALPHANUM_DOTS_AND_DASHES);
                    placeholders.add(placeholderName);
                } else if (context.containsKey(placeholderName)) {
                    // If it does, append the fixed value to the regex
                    String value = context.get(placeholderName);
                    if (!value.matches("^[" + REGEX_ALPHANUM + "]+$")) {
                        throw new ResolveException("Context field " + placeholderName + " contains illegal characters: " + value);
                    }
                    regexBuilder.append(value);
                    // And put the placeholder value in the resulting context
                    matchContext.put(placeholderName, value);
                } else {
                    regexBuilder.append(REGEX_ALPHANUM);
                    placeholders.add(placeholderName);
                }
            } else {
                // The element is a literal string, so append it as literal to the regex builder
                regexBuilder.append(element);
            }
        }
        regexBuilder.append("$");
        return regexBuilder.toString();
    }

    private static List<String> parsePattern(final String pattern) {
        String remainingPattern = pattern;
        List<String> result = new ArrayList<>();

        while (remainingPattern.length() > 0) {
            int openBrace = remainingPattern.indexOf(PLACEHOLDER_PREFIX);

            // When no replacement patterns found, then add the remaining part of the pattern as
            // literal and exit
            if (openBrace < 0) {
                result.add(remainingPattern);
                return result;
            }
            if (openBrace > 0) {
                result.add(remainingPattern.substring(0, openBrace));
                remainingPattern = remainingPattern.substring(openBrace);
            }
            if (openBrace == 0) {
                int closingPos = remainingPattern.indexOf(PLACEHOLDER_SUFFIX);
                if (closingPos <= 0) {
                    throw new PatternException("Illegal pattern", remainingPattern);
                }
                result.add(remainingPattern.substring(0, closingPos + 1));
                remainingPattern = remainingPattern.substring(closingPos + 1);
            }
        }

        return result;
    }

    private boolean elementIsPlaceholder(String element) {
        return element.startsWith(PLACEHOLDER_PREFIX) && element.endsWith(PLACEHOLDER_SUFFIX);
    }

    private String getPlaceholderName(String element) {
        return element.substring(1, element.length() - 1);
    }
}
