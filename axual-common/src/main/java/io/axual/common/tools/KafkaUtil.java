package io.axual.common.tools;

/*-
 * ========================LICENSE_START=================================
 * axual-common
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.config.SslConfigs;
import org.apache.kafka.common.config.types.Password;
import org.apache.kafka.common.security.auth.SecurityProtocol;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import io.axual.common.config.ClientConfig;
import io.axual.common.config.CommonConfig;
import io.axual.common.config.PasswordConfig;
import io.axual.common.config.SslConfig;

import static io.axual.common.config.ConfigParser.parseStringConfig;

/**
 * Class with public Kafka configuration helper methods.
 */
public class KafkaUtil {
    private static final Logger LOG = LoggerFactory.getLogger(KafkaUtil.class);

    private static final String HTTPS_VERIFICATION = "HTTPS";
    private static final String SSL_PROTOCOL = "SSL";

    private KafkaUtil() {
        // All methods are static
    }

    /**
     * Translate a ClientConfig object into a set of Kafka properties in a key/value map.
     *
     * @param config the ClientConfig object with given parameters.
     * @return the Kafka properties for the client application in a key/value map.
     */
    public static Map<String, Object> getKafkaConfigs(ClientConfig config) {
        Map<String, Object> result = KafkaUtil.getKafkaConfigs(config.getSslConfig());

        // Properties passed down the Kafka Proxy chains
        result.put(CommonConfig.APPLICATION_ID, config.getApplicationId());
        result.put(CommonConfig.APPLICATION_VERSION, config.getApplicationVersion());
        result.put(CommonConfig.ENDPOINT, config.getEndpoint());
        result.put(CommonConfig.TENANT, config.getTenant());
        result.put(CommonConfig.ENVIRONMENT, config.getEnvironment());
        if (config.isIdempotenceEnabled()) {
            result.put("enable.idempotence", "true");
        }
        if (config.getTransactionalId() != null) {
            result.put("transactional.id", config.getTransactionalId());
        }
        return result;
    }

    /**
     * Translate an SslConfig object into a set of Kafka properties in a key/value map.
     *
     * @param config the SslConfig object with given parameters.
     * @return the Kafka properties for the client application in a key/value map.
     */
    public static Map<String, Object> getKafkaConfigs(SslConfig config) {
        Map<String, Object> result = new HashMap<>();
        getKafkaConfigs(config, result);
        return result;
    }

    /**
     * Translate an SslConfig object into a set of Kafka properties in a key/value map.
     *
     * @param config  the SslConfig object with given parameters.
     * @param configs the map to receive the Kafka properties for the client application in a
     *                key/value map.
     */
    public static void getKafkaConfigs(SslConfig config, Map<String, Object> configs) {
        if (config != null) {
            try {
                configs.put(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG, SSL_PROTOCOL);
                configs.put(SslConfigs.SSL_PROTOCOL_CONFIG, config.getSslProtocol());
                configs.put(SslConfigs.SSL_ENABLED_PROTOCOLS_CONFIG, Arrays.asList(SslConfigs.DEFAULT_SSL_ENABLED_PROTOCOLS.split(",")));
                configs.put(SslConfigs.SSL_ENDPOINT_IDENTIFICATION_ALGORITHM_CONFIG, config.getEnableHostnameVerification() ? HTTPS_VERIFICATION : "");
                configs.put(SslConfigs.SSL_KEYSTORE_TYPE_CONFIG, config.getKeystoreType() == null ? SslConfigs.DEFAULT_SSL_KEYSTORE_TYPE : config.getKeystoreType().name());
                configs.put(SslConfigs.SSL_TRUSTSTORE_TYPE_CONFIG, config.getTruststoreType() == null ? SslConfigs.DEFAULT_SSL_TRUSTSTORE_TYPE : config.getTruststoreType().name());

                getKafkaSslConfigs(config, configs);
            } catch (Exception e) {
                LOG.error("Could not set Kafka properties: {}", config);
                throw e;
            }
        }
    }

    private static void getKafkaSslConfigs(SslConfig config, Map<String, Object> configs) {
        // JKS Support:
        if (config.getKeyPassword() != null) {
            configs.put(SslConfigs.SSL_KEY_PASSWORD_CONFIG, new Password(config.getKeyPassword().getValue()));
        }
        if (config.getKeystoreLocation() != null) {
            configs.put(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG, config.getKeystoreLocation());
        }
        if (config.getKeystorePassword() != null) {
            configs.put(SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG, new Password(config.getKeystorePassword().getValue()));
        }
        if (config.getTruststoreLocation() != null) {
            configs.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, config.getTruststoreLocation());
        }
        if (config.getTruststorePassword() != null) {
            configs.put(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG, new Password(config.getTruststorePassword().getValue()));
        }

        // PEM Support:
        if (config.getKeystoreCertificateChain() != null) {
            configs.put(SslConfigs.SSL_KEYSTORE_CERTIFICATE_CHAIN_CONFIG, new Password(config.getKeystoreCertificateChain().getValue()));
        }
        if (config.getKeystoreKey() != null) {
            configs.put(SslConfigs.SSL_KEYSTORE_KEY_CONFIG, new Password(config.getKeystoreKey().getValue()));
        }
        if (config.getTruststoreCertificates() != null) {
            configs.put(SslConfigs.SSL_TRUSTSTORE_CERTIFICATES_CONFIG, new Password(config.getTruststoreCertificates().getValue()));
        }
    }

    /**
     * Parse an SslConfig object from a set of Kafka properties.
     *
     * @param configs the Kafka properties.
     * @return the parsed ssl configuration.
     */
    public static SslConfig parseSslConfig(Map<String, Object> configs) {
        if (!MapUtil.stringValue(configs, CommonClientConfigs.SECURITY_PROTOCOL_CONFIG, "").equals(SSL_PROTOCOL)
                && !SecurityProtocol.SSL.equals(configs.get(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG))) {
            return null;
        }

        return SslConfig.newBuilder()
                .setEnableHostnameVerification(!parseStringConfig(configs, SslConfigs.SSL_ENDPOINT_IDENTIFICATION_ALGORITHM_CONFIG, false, "").isEmpty())
                .setKeystoreType(SslConfig.KeystoreType.valueOf(parseStringConfig(configs, SslConfigs.SSL_KEYSTORE_TYPE_CONFIG, true, SslConfigs.DEFAULT_SSL_KEYSTORE_TYPE)))
                .setTruststoreType(SslConfig.TruststoreType.valueOf(parseStringConfig(configs, SslConfigs.SSL_TRUSTSTORE_TYPE_CONFIG, true, SslConfigs.DEFAULT_SSL_TRUSTSTORE_TYPE)))
                .setSslProtocol(MapUtil.stringValue(configs, SslConfigs.SSL_PROTOCOL_CONFIG, SslConfigs.DEFAULT_SSL_PROTOCOL))
                .setKeystoreKey(getPasswordConfig(configs, SslConfigs.SSL_KEYSTORE_KEY_CONFIG))
                .setKeystoreCertificateChain(getPasswordConfig(configs, SslConfigs.SSL_KEYSTORE_CERTIFICATE_CHAIN_CONFIG))
                .setTruststoreCertificates(getPasswordConfig(configs, SslConfigs.SSL_TRUSTSTORE_CERTIFICATES_CONFIG))
                .setKeyPassword(getPasswordConfig(configs, SslConfigs.SSL_KEY_PASSWORD_CONFIG))
                .setKeystoreLocation(MapUtil.stringValue(configs, SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG))
                .setKeystorePassword(getPasswordConfig(configs, SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG))
                .setTruststoreLocation(MapUtil.stringValue(configs, SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG))
                .setTruststorePassword(getPasswordConfig(configs, SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG))
                .build();
    }

    /**
     * The passwords in the configs can be stored as Kafka Password, Axual PasswordConfig or String
     * types. This will return a PasswordConfig if any of these are located
     *
     * @param configs the Kafka properties.
     * @param key     the key for the passwords to retrieve
     * @return A configured PasswordConfig object or null is not set, or set to unknown type
     */
    private static PasswordConfig getPasswordConfig(Map<String, Object> configs, String key) {
        Object passwordObject = configs.get(key);
        if (passwordObject instanceof Password) {
            Password password = (Password) passwordObject;
            if (password.value() == null) {
                return null;
            }
            return new PasswordConfig(password.value());
        } else if (passwordObject instanceof PasswordConfig) {
            PasswordConfig password = (PasswordConfig) passwordObject;
            if (password.getValue() == null) {
                return null;
            }
            return (PasswordConfig) passwordObject;
        } else if (passwordObject instanceof String) {
            return new PasswordConfig((String) passwordObject);
        } else {
            return null;
        }
    }

    /**
     * Parse a ClientConfig object from a set of Kafka properties.
     *
     * @param configs the Kafka properties.
     * @return the parsed client configuration.
     */
    public static ClientConfig parseClientConfig(Map<String, Object> configs) {
        return ClientConfig.newBuilder()
                .setApplicationId(MapUtil.stringValue(configs, CommonConfig.APPLICATION_ID))
                .setApplicationVersion(MapUtil.stringValue(configs, CommonConfig.APPLICATION_VERSION))
                .setTenant(MapUtil.stringValue(configs, CommonConfig.TENANT))
                .setEnvironment(MapUtil.stringValue(configs, CommonConfig.ENVIRONMENT))
                .setEndpoint(MapUtil.stringValue(configs, CommonConfig.ENDPOINT, MapUtil.stringValue(configs, ProducerConfig.BOOTSTRAP_SERVERS_CONFIG)))
                .setSslConfig(parseSslConfig(configs))
                .build();
    }

    /**
     * Export given Kafka properties to an externally representable version. This method converts
     * any non-String property values to Strings, including Password objects. Please be careful: the
     * resulting output map may contain plaintext passwords.
     *
     * @param configs the internal Kafka properties
     * @return the exportable Kafka properties with key/value of String/String
     */
    public static Map<String, String> exportConfigs(Map<String, Object> configs) {
        Map<String, String> result = new HashMap<>();
        for (Map.Entry<String, Object> config : configs.entrySet()) {
            if (config.getValue() instanceof Password) {
                result.put(config.getKey(), ((Password) config.getValue()).value());
            } else {
                result.put(config.getKey(), config.getValue().toString());
            }
        }
        return result;
    }
}
