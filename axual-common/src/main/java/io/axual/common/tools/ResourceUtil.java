package io.axual.common.tools;

/*-
 * ========================LICENSE_START=================================
 * axual-common
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;

import io.axual.common.exception.ResourceNotFoundException;

/**
 * Class with resource helper methods.
 */
public final class ResourceUtil {
    private static final ClassLoader CLASSLOADER_SYSTEM = ClassLoader.getSystemClassLoader();
    private static final ClassLoader CLASSLOADER_RESOURCEUTIL = ResourceUtil.class.getClassLoader();
    private static final Logger LOG = LoggerFactory.getLogger(ResourceUtil.class);

    private ResourceUtil() {
    }

    private static URL getClassLoaderResource(String resourceName) {
        URL resource = CLASSLOADER_RESOURCEUTIL.getResource(resourceName);

        if (resource == null) {
            resource = CLASSLOADER_SYSTEM.getResource(resourceName);
            if (resource == null) {
                final ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
                resource = contextClassLoader.getResource(resourceName);
            }
        }

        return resource;

    }

    /**
     * Gets a URL to the resource with given name.
     *
     * @param resourceName the resource name
     * @return the resource path
     */
    public static URL getResourcePath(String resourceName) {
        try {
            File resourceFile = new File(resourceName);
            if (!resourceFile.isAbsolute()) {
                URL resourceUrl = getClassLoaderResource(resourceName);
                if (resourceUrl != null) {
                    return resourceUrl;
                }
            }
            return resourceFile.toURI().toURL();
        } catch (MalformedURLException e) {
            // This should not ever happen
            throw new ResourceNotFoundException(resourceName, e);
        }
    }

    /**
     * Gets resource as a file name.
     *
     * @param resourceName the name of the resource to load
     * @return the path to use to access the resource
     */
    public static String getResourceAsFile(final String resourceName) {
        return getResourceAsFile(resourceName, null);
    }

    /**
     * Gets resource as a file name.
     *
     * @param resourceName the resource name
     * @param tempDir the temp directory path
     * @return the resource file name
     */
    public static String getResourceAsFile(final String resourceName, String tempDir) {
        try {
            File temp = null;
            if (tempDir != null && !tempDir.trim().isEmpty()) {
                LOG.debug("Using user provided directory to create temporary keystore/truststore file.");
                temp = new File(tempDir);
            }

            File tmpFile = File.createTempFile("btf-", ".tmp" , temp);
            tmpFile.deleteOnExit();
            try (OutputStream os = new FileOutputStream(tmpFile);
                 InputStream is = ResourceUtil.getResourcePath(resourceName).openStream()
            ) {

                copy(is, os);
            }
            return tmpFile.getPath();
        } catch (IOException e) {
            LOG.warn(String.format("Could not get resource %s as file", resourceName), e);
        }
        return null;
    }

    /**
     * Copy bytes from an <code>InputStream</code> to an
     * <code>OutputStream</code>.
     *
     * @param input  the <code>InputStream</code> to read from
     * @param output the <code>OutputStream</code> to write to
     * @return the number of bytes copied
     * @throws IOException In case of an I/O problem
     */
    public static int copy(
            final InputStream input,
            final OutputStream output)
            throws IOException {
        final byte[] buffer = new byte[4096];
        int count = 0;
        int n = 0;
        while (-1 != (n = input.read(buffer))) {
            output.write(buffer, 0, n);
            count += n;
        }
        return count;
    }
}
