package io.axual.common.tools;

/*-
 * ========================LICENSE_START=================================
 * axual-common
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.KafkaException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Enumeration;
import java.util.Map;

import javax.net.ssl.SSLContext;
import org.apache.http.ssl.SSLContexts;

import io.axual.common.config.PasswordConfig;
import io.axual.common.config.SslConfig;
import io.axual.common.config.SslEngineConfig;
import io.axual.common.exception.ClientException;
import io.axual.common.exception.InvalidKeystoreException;

import static io.axual.common.tools.ResourceUtil.getResourcePath;

/**
 * Class with Ssl helper methods.
 */
public final class SslUtil {
    private static final Logger LOG = LoggerFactory.getLogger(SslUtil.class);
    private static final String DEFAULT_KEYSTORE_TYPE = "jks";

    /**
     * Private constructor to properly seal this class
     */
    private SslUtil() {
        // All methods are static
    }

    /**
     * Instantiate an SslContext object from an SslConfig object.
     *
     * @param config the config from which to extract the key and trust material.
     * @return an initialized SSLContext object.
     */
    public static SSLContext createSslContext(SslConfig config) {
        try {
            Map<String, Object> configMap = KafkaUtil.getKafkaConfigs(config);
            SslEngineConfig sslEngineConfig = new SslEngineConfig(configMap);

            KeyStore keyStore = sslEngineConfig.keystore();
            KeyStore truststore = sslEngineConfig.truststore();

            validateKeystore(keyStore);
            if (config.getEnableValidateTruststore()) {
                validateKeystore(truststore);
            }

            return SSLContexts.custom()
                    .loadTrustMaterial(sslEngineConfig.truststore(), null)
                    .loadKeyMaterial(sslEngineConfig.keystore(), keyPassword(config))
                    .build();
        } catch (NoSuchAlgorithmException | UnrecoverableKeyException | KeyStoreException
                | KeyManagementException | KafkaException e) {
            throw new ClientException(String.format("Exception occurred while creating SSLContext from %s", e.getMessage()), e);
        }
    }

    public static void validateKeystore(KeyStore keystore) {
        Date now = new Date();
        try {
            Enumeration<String> aliases = keystore.aliases();
            while (aliases.hasMoreElements()) {
                String alias = aliases.nextElement();
                Certificate certificate = keystore.getCertificate(alias);
                if (certificate instanceof X509Certificate) {
                    X509Certificate x509 = ((X509Certificate) certificate);
                    String principal = x509.getSubjectX500Principal().toString();
                    logValidationResult(now, alias, x509, principal);
                }
            }
        } catch (KeyStoreException e) {
            throw new InvalidKeystoreException("Could not validate keystore", e);
        }
    }

    private static void logValidationResult(Date now, String alias, X509Certificate x509, String principal) {
        if (now.before(x509.getNotBefore())) {
            LOG.error("Key with alias \"{}\": {} will become valid at {}", alias, principal, x509.getNotBefore());
        } else if (now.after(x509.getNotAfter())) {
            LOG.error("Key with alias \"{}\": {} became invalid at {}", alias, principal, x509.getNotAfter());
        } else {
            long diff = x509.getNotAfter().getTime() - now.getTime();
            long diffDays = diff / (24 * 60 * 60 * 1000);
            if (diffDays < 30) {
                LOG.warn("Key expiration imminent. Key with alias \"{}\": {} will expire in {} days", alias, principal, x509.getNotAfter());
            }
        }
    }

    /**
     * Return key password as byte[] if present.
     *
     * @param config the config from which to extract the password.
     * @return the password if present, else null.
     */
    private static char[] keyPassword(final SslConfig config) {
        if (config.getKeyPassword() == null || config.getKeyPassword().getValue() == null) {
            return null;
        }
        return config.getKeyPassword().getValue().toCharArray();
    }


    /**
     * Load a keystore into a KeyStore object.
     *
     * @param location the location of the keystore file
     * @param password the password of the keystore file
     * @return the key store
     * @throws KeyStoreException        a key store exception if the format of the store was faulty
     * @throws IOException              an io exception if the file could not be read correctly
     * @throws CertificateException     a certificate exception if the certificate inside the
     *                                  keystore was faulty
     * @throws NoSuchAlgorithmException a no such algorithm exception if the certificate uses an
     *                                  unsupported algorithm
     * @deprecated
     */
    @Deprecated
    public static KeyStore loadKeyStore(String location, PasswordConfig password) throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException {
        return loadKeyStore(location, password, null);
    }

    /**
     * @deprecated
     */
    @Deprecated
    public static KeyStore loadKeyStore(String location, PasswordConfig password, String type) throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException {
        type = type == null || type.trim().isEmpty() ? DEFAULT_KEYSTORE_TYPE : type;
        LOG.debug("Getting {} keystore instance...", type);
        KeyStore keyStore = KeyStore.getInstance(type);
        try (InputStream is = getResourcePath(location).openStream()) {
            keyStore.load(is, password.getValue().toCharArray());
        }
        return keyStore;
    }

    /**
     * @deprecated
     * Log the contents of a key store.
     *
     * @param type     a human-readable keystore type (ie. "keystore" or "truststore")
     * @param location the location of the keystore
     * @param password the password of the keystore
     */
    @Deprecated
    public static void printKeyStore(String type, String location, PasswordConfig password) {
        LOG.info("Using {} at location {}", type, location);
        try {
            KeyStore keystore = loadKeyStore(location, password);
            Enumeration<String> aliases = keystore.aliases();
            while (aliases.hasMoreElements()) {
                String alias = aliases.nextElement();
                Certificate certificate = keystore.getCertificate(alias);
                LOG.info("Found key with alias \"{}\": {}", alias, certificate);
            }
        } catch (Exception e) {
            LOG.error("Error during loading and printing of keystore: {}", e.getMessage(), e);
        }
        LOG.info("End of {} at location {}", type, location);
    }

    /**
     * @deprecated
     * This will try to load the keystore and throws an exception if invalid
     *
     * @param type          a human-readable keystore type (ie. "keystore" or "truststore")
     * @param storeLocation the location of the keystore
     * @param storePassword the password of the keystore
     */
    @Deprecated
    public static void validateCertificateStore(String type, String storeLocation, PasswordConfig storePassword, boolean performCertificateValidation) {
        validateCertificateStore(type, storeLocation, storePassword, null, performCertificateValidation);
    }

    /**
     * @deprecated
     */
    @Deprecated
    public static void validateCertificateStore(String type, String storeLocation, PasswordConfig storePassword, String storeType, boolean performCertificateValidation) {
        Date now = new Date();
        try {
            KeyStore keystore = loadKeyStore(storeLocation, storePassword, storeType);
            Enumeration<String> aliases = keystore.aliases();
            while (aliases.hasMoreElements()) {
                String alias = aliases.nextElement();
                Certificate certificate = keystore.getCertificate(alias);
                if (certificate instanceof X509Certificate) {
                    X509Certificate x509 = ((X509Certificate) certificate);
                    String principal = x509.getSubjectX500Principal().toString();
                    if (performCertificateValidation) {
                        logValidationResult(now, alias, x509, principal);
                    }
                }
            }
        } catch (KeyStoreException | CertificateException | NoSuchAlgorithmException | IOException e) {
            throw new InvalidKeystoreException(String.format("Could not validate %s from %s", type, storeLocation), e);
        }
    }
}
