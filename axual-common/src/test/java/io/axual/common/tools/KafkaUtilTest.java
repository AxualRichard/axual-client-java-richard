package io.axual.common.tools;

/*-
 * ========================LICENSE_START=================================
 * axual-common
 * %%
 * Copyright (C) 2020 - 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.security.auth.SecurityProtocol;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import io.axual.common.config.SslConfig;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertNull;
import static org.apache.kafka.clients.CommonClientConfigs.SECURITY_PROTOCOL_CONFIG;
import static org.apache.kafka.common.config.SslConfigs.*;

public class KafkaUtilTest {

    @Test
    public void parseSslConfig_defaults() {
        Map<String, Object> config = new HashMap<>();
        config.put(SECURITY_PROTOCOL_CONFIG, SecurityProtocol.SSL);

        // JKS
        config.put(SSL_KEYSTORE_LOCATION_CONFIG, "KEYSTORE_LOCATION");
        config.put(SSL_KEYSTORE_PASSWORD_CONFIG, "KEYSTORE_PASSWORD");
        config.put(SSL_KEY_PASSWORD_CONFIG, "KEY_PASSWORD");
        config.put(SSL_TRUSTSTORE_LOCATION_CONFIG, "TRUSTSTORE_LOCATION");
        config.put(SSL_TRUSTSTORE_PASSWORD_CONFIG, "TRUSTSTORE_PASSWORD");

        SslConfig sslConfig = KafkaUtil.parseSslConfig(config);

        assertEquals(SslConfig.KeystoreType.JKS, sslConfig.getKeystoreType());
        assertEquals(SslConfig.TruststoreType.JKS, sslConfig.getTruststoreType());
        assertEquals(SslConfig.DEFAULT_SSL_PROTOCOL, sslConfig.getSslProtocol());
        assertFalse(sslConfig.getEnableHostnameVerification());
    }

    @Test
    public void parseSslConfig_typeStrings() {
        Map<String, Object> config = new HashMap<>();
        config.put(SECURITY_PROTOCOL_CONFIG, SecurityProtocol.SSL);

        // PEM
        config.put(SSL_KEYSTORE_TYPE_CONFIG, "PEM");
        config.put(SSL_KEYSTORE_CERTIFICATE_CHAIN_CONFIG, "KEYSTORE_CERTIFICATE_CHAIN");
        config.put(SSL_KEYSTORE_KEY_CONFIG, "KEY");
        config.put(SSL_TRUSTSTORE_TYPE_CONFIG, "PEM");
        config.put(SSL_TRUSTSTORE_CERTIFICATES_CONFIG, "TRUSTSTORE_CERTIFICATES");

        SslConfig sslConfig = KafkaUtil.parseSslConfig(config);

        assertEquals(SslConfig.KeystoreType.PEM, sslConfig.getKeystoreType());
        assertEquals(SslConfig.TruststoreType.PEM, sslConfig.getTruststoreType());
    }

    @Test
    public void parseSslConfig_emptySecurityProtocol() {
        Map<String, Object> config = new HashMap<>();
        config.put(SECURITY_PROTOCOL_CONFIG, "");
        assertNull(KafkaUtil.parseSslConfig(config));
    }

    @Test
    public void parseSslConfig_noSecurityProtocol() {
        Map<String, Object> config = new HashMap<>();
        assertNull(KafkaUtil.parseSslConfig(config));
    }
}
