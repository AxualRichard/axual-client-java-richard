package io.axual.common.tools;

/*-
 * ========================LICENSE_START=================================
 * axual-common
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.junit.Test;

import java.io.IOException;
import java.security.KeyStore;

import javax.net.ssl.SSLContext;

import io.axual.common.config.PasswordConfig;
import io.axual.common.config.SslConfig;
import io.axual.common.exception.ClientException;

import static io.axual.common.test.TestUtils.DEFAULT_PASSWORD;
import static io.axual.common.test.TestUtils.getAbsolutePath;
import static io.axual.common.test.TestUtils.getFileContentsAsString;
import static org.junit.Assert.assertNotNull;

public class SslUtilTest {

    protected final static String KEYSTORELOCATION_EXISTING = "ssl/axual.client.keystore.jks";
    protected final static String PKCS12_KEYSTORELOCATION = "ssl/axual.client.keystore.p12";
    protected final static PasswordConfig KEYSTOREPASSWORD_CORRECT = new PasswordConfig("notsecret");
    protected final static PasswordConfig KEYSTOREPASSWORD_INCORRECT = new PasswordConfig("verysecret");

    public static final String KEY_PEM_UNENCRYPTED_CONTENTS = getFileContentsAsString(getAbsolutePath("ssl/axual.client.key.pem"));
    public static final String KEYSTORE_PEM_CONTENTS = getFileContentsAsString(getAbsolutePath("ssl/axual.client.keystore.pem"));
    public static final String TRUSTSTORE_PEM_CONTENTS = getFileContentsAsString(getAbsolutePath("ssl/axual.client.truststore.pem"));

    @Test
    public void loadKeyStore_correct() throws Exception {
        KeyStore keyStore = SslUtil.loadKeyStore(KEYSTORELOCATION_EXISTING, KEYSTOREPASSWORD_CORRECT);
        assertNotNull(keyStore);
    }

    @Test
    public void loadPKCS12TypeKeyStore() throws Exception {
        KeyStore keyStore = SslUtil.loadKeyStore(PKCS12_KEYSTORELOCATION, KEYSTOREPASSWORD_CORRECT, SslConfig.KeystoreType.PKCS12.name());
        assertNotNull(keyStore);
    }

    @Test(expected = IOException.class)
    public void loadKeyStore_nonExisting() throws Exception {
        SslUtil.loadKeyStore("ssl/no.such.file.jks", KEYSTOREPASSWORD_CORRECT);
    }

    @Test(expected = IOException.class)
    public void loadKeyStore_incorrectPassword() throws Exception {
        SslUtil.loadKeyStore(KEYSTORELOCATION_EXISTING, KEYSTOREPASSWORD_INCORRECT);
    }

    @Test
    public void createSslContext_pem() {
        SslConfig sslConfig = SslConfig.newBuilder()
                .setKeystoreType(SslConfig.KeystoreType.PEM)
                .setTruststoreType(SslConfig.TruststoreType.PEM)
                .setKeystoreKey(KEY_PEM_UNENCRYPTED_CONTENTS)
                .setKeystoreCertificateChain(KEYSTORE_PEM_CONTENTS)
                .setTruststoreCertificates(TRUSTSTORE_PEM_CONTENTS)
                .build();

        final SSLContext sslContext = SslUtil.createSslContext(sslConfig);

        assertNotNull(sslContext);
    }

    @Test
    public void createSslContext_jks() {
        SslConfig sslConfig = SslConfig.newBuilder()
                .setKeystoreType(SslConfig.KeystoreType.JKS)
                .setKeyPassword(DEFAULT_PASSWORD)
                .setTruststoreType(SslConfig.TruststoreType.JKS)
                .setKeystoreLocation(getAbsolutePath("ssl/axual.client.keystore.jks"))
                .setKeystorePassword(DEFAULT_PASSWORD)
                .setTruststoreLocation(getAbsolutePath("ssl/axual.client.truststore.jks"))
                .setTruststorePassword(DEFAULT_PASSWORD)
                .build();

        final SSLContext sslContext = SslUtil.createSslContext(sslConfig);

        assertNotNull(sslContext);
    }

    @Test(expected = ClientException.class)
    public void createSslContext_jksNoSuchFile() {
        SslConfig sslConfig = SslConfig.newBuilder()
                .setKeystoreType(SslConfig.KeystoreType.JKS)
                .setKeyPassword(DEFAULT_PASSWORD)
                .setTruststoreType(SslConfig.TruststoreType.JKS)
                .setKeystoreLocation("ssl/no.such.file.jks")
                .setKeystorePassword(DEFAULT_PASSWORD)
                .setTruststoreLocation("ssl/no.such.file.jks")
                .setTruststorePassword(DEFAULT_PASSWORD)
                .build();

        SslUtil.createSslContext(sslConfig);
    }
}
