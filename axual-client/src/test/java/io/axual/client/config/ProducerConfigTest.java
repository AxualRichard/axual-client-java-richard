package io.axual.client.config;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ProducerConfigTest {
    @Test(expected = IllegalStateException.class)
    public void testBuildGenericProducerConfigWithoutSettingStrategy_ShouldThrowAnException() {
        ProducerConfig.builder().build();
    }

    @Test
    public void testBuildGenericProducerConfigWithoutSettingStream_ShouldReturnSetOptions() {
        final String dummyClass = "io.axual.dummy.class";

        DeliveryStrategy expectedDeliveryStrategy = DeliveryStrategy.AT_LEAST_ONCE;
        OrderingStrategy expectedOrderingStrategy = OrderingStrategy.KEEPING_ORDER;

        ProducerConfig producerConfig = ProducerConfig.builder()
                .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                .setOrderingStrategy(OrderingStrategy.KEEPING_ORDER)
                .setKeySerializer(dummyClass)
                .setValueSerializer(dummyClass)
                .build();
        assertEquals(expectedDeliveryStrategy, producerConfig.getDeliveryStrategy());
        assertEquals(expectedOrderingStrategy, producerConfig.getOrderingStrategy());
        assertEquals(dummyClass, producerConfig.getKeySerializer());
        assertEquals(dummyClass, producerConfig.getValueSerializer());
    }

    @Test
    public void testBuildConfigWithoutSettingBatchSize_ShouldReturnDefaultOptions() {
        final String dummyClass = "io.axual.dummy.class";
        final DeliveryStrategy expectedDeliveryStrategy = DeliveryStrategy.AT_LEAST_ONCE;
        final OrderingStrategy expectedOrderingStrategy = OrderingStrategy.KEEPING_ORDER;

        BaseProducerConfig baseProducerConfigBuilder = new BaseProducerConfig.Builder()
                .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                .setOrderingStrategy(OrderingStrategy.KEEPING_ORDER)
                .setKeySerializer(dummyClass)
                .setValueSerializer(dummyClass)
                .build();

        assertEquals(expectedDeliveryStrategy, baseProducerConfigBuilder.getDeliveryStrategy());
        assertEquals(expectedOrderingStrategy, baseProducerConfigBuilder.getOrderingStrategy());
        assertEquals(Integer.valueOf(BaseProducerConfig.DEFAULT_BATCH_SIZE), baseProducerConfigBuilder.getBatchSize());
    }
}
