package io.axual.client.producer;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.internals.RecordHeaders;

import java.util.UUID;

import io.axual.client.proxy.generic.producer.ExtendedProducerRecord;
import io.axual.client.proxy.lineage.LineageHeaders;
import io.axual.common.exception.ClientException;
import io.axual.serde.utils.HeaderUtils;

public class ProducerMessage<K, V> {
    private final ExtendedProducerRecord<K, V> producerRecord;

    private ProducerMessage(Builder<K, V> builder) {
        producerRecord = new ExtendedProducerRecord<>(
                builder.stream,
                null,
                builder.timestamp,
                builder.key,
                builder.value,
                new RecordHeaders(),
                null);

        if (builder.messageId != null) {
            HeaderUtils.addUuidHeader(
                    producerRecord.headers(),
                    LineageHeaders.MESSAGE_ID_HEADER,
                    builder.messageId);
        }
    }

    public String getStream() {
        return producerRecord.topic();
    }

    public K getKey() {
        return producerRecord.key();
    }

    public V getValue() {
        return producerRecord.value();
    }

    public Long getTimestamp() {
        return producerRecord.timestamp();
    }

    public UUID getMessageId() {
        return HeaderUtils.decodeUuidHeader(producerRecord.headers().lastHeader(LineageHeaders.MESSAGE_ID_HEADER));
    }

    public ProducerRecord<K, V> getProducerRecord() {
        return producerRecord;
    }

    public static <K, V> Builder<K, V> newBuilder() {
        return new Builder<>();
    }

    public static class Builder<K, V> {
        private String stream = null;
        private K key = null;
        private V value = null;
        private Long timestamp = null;
        private UUID messageId = null;

        protected void validate() {
            if (stream == null) {
                throw new ClientException("Producer stream cannot be null");
            }

            if (messageId == null) {
                messageId = UUID.randomUUID();
            }
        }

        public ProducerMessage<K, V> build() {
            validate();
            return new ProducerMessage<>(this);
        }

        public String getStream() {
            return stream;
        }

        public Builder<K, V> setStream(String stream) {
            this.stream = stream;
            return this;
        }

        public K getKey() {
            return key;
        }

        public Builder<K, V> setKey(K key) {
            this.key = key;
            return this;
        }

        public V getValue() {
            return value;
        }

        public Builder<K, V> setValue(V value) {
            this.value = value;
            return this;
        }

        public Long getTimestamp() {
            return timestamp;
        }

        public Builder<K, V> setTimestamp(Long timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public UUID getMessageId() {
            return messageId;
        }

        public Builder<K, V> setMessageId(UUID messageId) {
            this.messageId = messageId;
            return this;
        }
    }
}
