package io.axual.client.producer.generic;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Future;

import io.axual.client.config.BaseProducerConfig;
import io.axual.client.janitor.Janitor;
import io.axual.client.producer.ProduceCallback;
import io.axual.client.producer.ProducedMessage;
import io.axual.client.producer.Producer;
import io.axual.client.producer.ProducerMessage;
import io.axual.common.config.ClientConfig;

/**
 * Class used for all the producers, containing all the producer specific methods.
 *
 * @param <K> Type of the Key to be produced
 * @param <V> Type of the Value to be produced
 */
public class GenericProducer<K, V> extends Janitor.ManagedCloseable implements Producer<K, V>, AutoCloseable {
    private static final Logger LOG = LoggerFactory.getLogger(GenericProducer.class);

    private final ProducerWorker<K, V> worker;
    private final ProduceJobFactory<K, V> jobFactory;
    private final ProducerWorkerManager producerWorkerManager;

    public GenericProducer(ClientConfig clientConfig, BaseProducerConfig<K, V> producerConfig, ProducerWorkerManager producerWorkerManager) {
        this(clientConfig, producerConfig, producerWorkerManager, ProduceJob::new);
    }

    public GenericProducer(ClientConfig clientConfig, BaseProducerConfig<K, V> producerConfig, ProducerWorkerManager producerWorkerManager, ProduceJobFactory<K, V> jobFactory) {
        this.producerWorkerManager = producerWorkerManager;
        this.worker = this.producerWorkerManager.claimWorker(clientConfig, this, producerConfig);
        LOG.info("Created producer of type {}/{}.", producerConfig.getDeliveryStrategy(), producerConfig.getOrderingStrategy());
        this.jobFactory = jobFactory;
    }

    @Override
    public void close() {
        producerWorkerManager.releaseWorker(this);
        super.close();
    }

    @Override
    public Future<ProducedMessage<K, V>> produce(final ProducerMessage<K, V> message) {
        return produce(message, null);
    }

    @Override
    public Future<ProducedMessage<K, V>> produce(final ProducerMessage<K, V> message, final ProduceCallback<K, V> produceCallback) {
        return worker.queueJob(jobFactory.createNewJob(message, produceCallback));
    }
}
