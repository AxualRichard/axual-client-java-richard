package io.axual.client.exception;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.time.Duration;

import io.axual.common.exception.ClientException;

/**
 * Exception that signals processing a message failed, but it's okay to retry after a specific
 * timeout.
 */
public class RetriableException extends ClientException {
    // Default sleep time of one second
    private static final Duration DEFAULT_SLEEPTIME = Duration.ofMillis(1000);
    private final Duration sleepTime;

    /**
     * Creates a new RetriableException with default sleeptime
     *
     * @param cause the original error
     */
    public RetriableException(Throwable cause) {
        this(cause, DEFAULT_SLEEPTIME);
    }

    /**
     * Creates a new RetriableException with specified sleeptime
     *
     * @param cause     the original error
     * @param sleepTime the time to sleep before retrying to process the message
     * @deprecated
     */
    @Deprecated
    public RetriableException(Throwable cause, long sleepTime) {
        this(cause, Duration.ofMillis(sleepTime));
    }

    /**
     * Creates a new RetriableException with specified sleeptime
     *
     * @param cause     the original error
     * @param sleepTime the time to sleep before retrying to process the message
     */
    public RetriableException(Throwable cause, Duration sleepTime) {
        super(String.format("Error during processing of message, retrying in %s.%nOriginal error: %s",
                sleepTime, cause.getMessage()), cause);
        this.sleepTime = sleepTime;
    }

    /**
     * Creates a new RetriableException with specified sleeptime
     *
     * @param sleepTime the time to sleep in millis
     * @deprecated
     */
    @Deprecated
    public RetriableException(long sleepTime) {
        this(Duration.ofMillis(sleepTime));
    }

    /**
     * Creates a new RetriableException with specified sleeptime
     *
     * @param sleepTime the time to sleep in millis
     */
    public RetriableException(Duration sleepTime) {
        super(String.format("Error during processing of message, retrying in %s.", sleepTime));
        this.sleepTime = sleepTime;
    }

    public Duration getSleepTime() {
        return sleepTime;
    }
}
