package io.axual.client.config;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import io.axual.common.annotation.InterfaceStability;

/**
 * This is the base for all AxualClient Avro Consumer configurations. It contains the settings all
 * Avro consumers have in common. A Builder pattern is used for initialization.
 *
 * @param <K> the type of the key that will be consumed. A compatible key deserializer is provided
 *            to deserialize the objects
 * @param <V> the type of the value that will be consumed. A compatible value deserializer is
 *            provided to deserialize the objects
 */
@InterfaceStability.Evolving
public class BaseAvroConsumerConfig<K, V> extends BaseConsumerConfig<K, V> {
    /**
     * Instantiates a new Base Avro consumer config.
     *
     * @param builder the builder containing the configuration values
     */
    protected BaseAvroConsumerConfig(Builder<K, V, ?> builder) {
        super(builder);
    }

    /**
     * The Builder is used to set the configuration options.
     *
     * @param <K> the type of the key that will be consumed.
     * @param <V> the type of the value that will be consumed.
     * @param <T> the type of the Builder that is to be used.
     */
    public static class Builder<K, V, T extends Builder<K, V, T>> extends BaseConsumerConfig.Builder<K, V, T> {
        /**
         * Validates and builds the {@link BaseAvroConsumerConfig} object which is used to create a
         * consumer
         *
         * @return a {@link BaseAvroConsumerConfig} object
         */
        @Override
        public BaseAvroConsumerConfig<K, V> build() {
            validate();
            return new BaseAvroConsumerConfig<>(this);
        }
    }
}
