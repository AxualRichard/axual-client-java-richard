package io.axual.client.consumer.avro;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.avro.generic.GenericContainer;

import io.axual.client.consumer.Processor;
import io.axual.client.consumer.base.BaseConsumer;

/**
 * Generic consumer class, the strategy is implemented in the used source
 */
public class AvroConsumer<K extends GenericContainer, V extends GenericContainer> extends BaseConsumer<K, V> {
    public AvroConsumer(AvroMessageSource<K, V> messageSource, Processor<K, V> processor) {
        super(messageSource, processor);
    }
}
