package io.axual.client.consumer.base;

/*-
 * ========================LICENSE_START=================================
 * axual-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.List;

public class CommitStrategyAMO<K, V> implements CommitStrategy<K, V> {
    private final Committer<K, V> committer;

    public CommitStrategyAMO(Committer<K, V> committer) {
        this.committer = committer;
    }

    @Override
    public void close() {
        committer.commitProcessedOffsets(true);
    }

    @Override
    public void onAfterFetchBatch(List<BaseMessage<K, V>> messages) {
        // Mark all messages in the batch as processed
        for (BaseMessage<K, V> message : messages) {
            committer.markAsProcessed(message);
        }
        // And commit right away to make sure we never see messages twice
        committer.commitProcessedOffsets(true);
    }

    @Override
    public void onAfterProcessBatch() {
        // Do nothing
    }

    @Override
    public void onAfterProcessMessage(BaseMessage<K, V> message, Throwable error) {
        // Do nothing
    }
}
