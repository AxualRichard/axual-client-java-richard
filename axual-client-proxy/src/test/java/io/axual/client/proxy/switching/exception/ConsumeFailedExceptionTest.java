package io.axual.client.proxy.switching.exception;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ConsumeFailedExceptionTest {
    protected final static String INFO = "Unit Test Information";
    protected final static String CAUSE_INFO = "Unit Test Exception";
    protected static final Exception CAUSE = new RuntimeException(CAUSE_INFO);

    @Test
    public void construction_with_info() {
        final String expectedMessage = String.format(ConsumeFailedException.MESSAGE_WITH_INFO_FORMAT, INFO, CAUSE_INFO);
        ConsumeFailedException consumeFailedException = new ConsumeFailedException(CAUSE, INFO);
        assertEquals(expectedMessage, consumeFailedException.getMessage());
        assertEquals(CAUSE, consumeFailedException.getCause());
    }

    @Test
    public void construction_without_info() {
        final String expectedMessage = String.format(ConsumeFailedException.MESSAGE_WITHOUT_INFO_FORMAT, CAUSE_INFO);
        ConsumeFailedException consumeFailedException = new ConsumeFailedException(CAUSE);
        assertEquals(expectedMessage, consumeFailedException.getMessage());
        assertEquals(CAUSE, consumeFailedException.getCause());
    }
}
