package io.axual.client.proxy.wrapped.consumer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 - 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.commons.lang3.tuple.Triple;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerGroupMetadata;
import org.apache.kafka.clients.consumer.ConsumerRebalanceListener;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.clients.consumer.OffsetAndTimestamp;
import org.apache.kafka.clients.consumer.OffsetCommitCallback;
import org.apache.kafka.common.Metric;
import org.apache.kafka.common.MetricName;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.TopicPartition;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.Duration;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.OptionalLong;
import java.util.Set;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class WrappedConsumerTest {

    @Mock
    public Consumer<String, String> mockedConsumer;

    public WrappedConsumer<String, String> wrappedConsumer;


    @Before
    public void setup() {
        wrappedConsumer = new WrappedConsumer<>(mockedConsumer, Collections.emptyMap());
    }

    @Test
    public void assignment() {
        Set<TopicPartition> toReturn = Collections.emptySet();
        doReturn(toReturn).when(mockedConsumer).assignment();

        Set<TopicPartition> result = wrappedConsumer.assignment();
        assertEquals(toReturn, result);
        verify(mockedConsumer, times(1)).assignment();
    }

    @Test
    public void subscription() {
        Set<String> toReturn = Collections.emptySet();
        doReturn(toReturn).when(mockedConsumer).subscription();

        Set<String> result = wrappedConsumer.subscription();
        assertEquals(toReturn, result);
        verify(mockedConsumer, times(1)).subscription();
    }

    @Test
    public void subscribe_CollectionWithoutListener() {
        Collection<String> topics = Collections.emptyList();
        doNothing().when(mockedConsumer).subscribe(topics);

        wrappedConsumer.subscribe(topics);
        verify(mockedConsumer, times(1)).subscribe(topics);
    }

    @Test
    public void subscribe_CollectionWithListener() {
        Collection<String> topics = Collections.emptyList();
        ConsumerRebalanceListener listener = mock(ConsumerRebalanceListener.class);
        doNothing().when(mockedConsumer).subscribe(topics, listener);

        wrappedConsumer.subscribe(topics, listener);
        verify(mockedConsumer, times(1)).subscribe(topics, listener);
    }

    @Test
    public void assign() {
        Collection<TopicPartition> partitions = Collections.emptyList();
        doNothing().when(mockedConsumer).assign(partitions);

        wrappedConsumer.assign(partitions);
        verify(mockedConsumer, times(1)).assign(partitions);
    }

    @Test
    public void subscribe_PatternWithoutListener() {
        Pattern pattern = Pattern.compile(".*");
        doNothing().when(mockedConsumer).subscribe(pattern);

        wrappedConsumer.subscribe(pattern);
        verify(mockedConsumer, times(1)).subscribe(pattern);

    }

    @Test
    public void subscribe_PatternWithListener() {
        Pattern pattern = Pattern.compile(".*");
        ConsumerRebalanceListener listener = mock(ConsumerRebalanceListener.class);
        doNothing().when(mockedConsumer).subscribe(pattern, listener);

        wrappedConsumer.subscribe(pattern, listener);
        verify(mockedConsumer, times(1)).subscribe(pattern, listener);
    }

    @Test
    public void unsubscribe() {
        doNothing().when(mockedConsumer).unsubscribe();
        wrappedConsumer.unsubscribe();
        verify(mockedConsumer, times(1)).unsubscribe();
    }

    @Test
    public void poll_WithTimeout() {
        long timeout = 0L;
        ConsumerRecords<String, String> toReturn = mock(ConsumerRecords.class);
        doReturn(toReturn).when(mockedConsumer).poll(timeout);

        ConsumerRecords<String, String> result = wrappedConsumer.poll(timeout);

        assertEquals(toReturn, result);
        verify(mockedConsumer, times(1)).poll(timeout);
    }

    @Test
    public void testPoll_WithDuration() {
        Duration duration = Duration.ofMillis(0L);
        ConsumerRecords<String, String> toReturn = mock(ConsumerRecords.class);
        doReturn(toReturn).when(mockedConsumer).poll(duration);

        ConsumerRecords<String, String> result = wrappedConsumer.poll(duration);

        assertEquals(toReturn, result);
        verify(mockedConsumer, times(1)).poll(duration);
    }

    @Test
    public void commitSync_WithoutOffsetsWithoutDuration() {
        doNothing().when(mockedConsumer).commitSync();

        wrappedConsumer.commitSync();
        verify(mockedConsumer, times(1)).commitSync();
    }

    @Test
    public void commitSync_WithoutOffsetsWithDuration() {
        Duration duration = Duration.ofMillis(0L);
        doNothing().when(mockedConsumer).commitSync(duration);

        wrappedConsumer.commitSync(duration);

        verify(mockedConsumer, times(1)).commitSync(duration);
    }

    @Test
    public void commitSync_WithOffsetsWithoutDuration() {
        Map<TopicPartition, OffsetAndMetadata> offsets = Collections.emptyMap();
        doNothing().when(mockedConsumer).commitSync(offsets);

        wrappedConsumer.commitSync(offsets);

        verify(mockedConsumer, times(1)).commitSync(offsets);
    }

    @Test
    public void commitSync_WithOffsetsWithDuration() {
        Duration duration = Duration.ofMillis(0L);
        Map<TopicPartition, OffsetAndMetadata> offsets = Collections.emptyMap();
        doNothing().when(mockedConsumer).commitSync(offsets, duration);

        wrappedConsumer.commitSync(offsets, duration);

        verify(mockedConsumer, times(1)).commitSync(offsets, duration);
    }

    @Test
    public void commitAsync_WithoutOffsetsWithoutCallback() {
        doNothing().when(mockedConsumer).commitAsync();

        wrappedConsumer.commitAsync();

        verify(mockedConsumer, times(1)).commitAsync();
    }

    @Test
    public void commitAsync_WithoutOffsetsWithCallback() {
        OffsetCommitCallback callback = mock(OffsetCommitCallback.class);
        doNothing().when(mockedConsumer).commitAsync(callback);

        wrappedConsumer.commitAsync(callback);

        verify(mockedConsumer, times(1)).commitAsync(callback);
    }

    @Test
    public void commitAsync_WithOffsetsWithCallback() {
        Map<TopicPartition, OffsetAndMetadata> offsets = Collections.emptyMap();
        OffsetCommitCallback callback = mock(OffsetCommitCallback.class);
        doNothing().when(mockedConsumer).commitAsync(offsets, callback);

        wrappedConsumer.commitAsync(offsets, callback);

        verify(mockedConsumer, times(1)).commitAsync(offsets, callback);
    }

    @Test
    public void seek_WithOffset() {
        long offset = 1L;
        TopicPartition topicPartition = new TopicPartition("tp", 0);
        doNothing().when(mockedConsumer).seek(topicPartition, offset);

        wrappedConsumer.seek(topicPartition, offset);

        verify(mockedConsumer, times(1)).seek(topicPartition, offset);
    }

    @Test
    public void testSeek_WithOffsetAndMetadata() {
        OffsetAndMetadata offsetAndMetadata = new OffsetAndMetadata(1L);
        TopicPartition topicPartition = new TopicPartition("tp", 0);
        doNothing().when(mockedConsumer).seek(topicPartition, offsetAndMetadata);

        wrappedConsumer.seek(topicPartition, offsetAndMetadata);

        verify(mockedConsumer, times(1)).seek(topicPartition, offsetAndMetadata);
    }

    @Test
    public void seekToBeginning() {
        Collection<TopicPartition> topicPartitions = Collections.emptySet();
        doNothing().when(mockedConsumer).seekToBeginning(topicPartitions);

        wrappedConsumer.seekToBeginning(topicPartitions);

        verify(mockedConsumer, times(1)).seekToBeginning(topicPartitions);
    }

    @Test
    public void seekToEnd() {
        Collection<TopicPartition> topicPartitions = Collections.emptySet();
        doNothing().when(mockedConsumer).seekToEnd(topicPartitions);

        wrappedConsumer.seekToEnd(topicPartitions);

        verify(mockedConsumer, times(1)).seekToEnd(topicPartitions);
    }

    @Test
    public void position_WithoutDuration() {
        TopicPartition topicPartition = new TopicPartition("tp", 0);
        Long toReturn = 12L;
        doReturn(toReturn).when(mockedConsumer).position(topicPartition);

        Long result = wrappedConsumer.position(topicPartition);

        assertEquals(toReturn, result);
        verify(mockedConsumer, times(1)).position(topicPartition);
    }

    @Test
    public void position_WithDuration() {
        TopicPartition topicPartition = new TopicPartition("tp", 0);
        Duration duration = Duration.ofMillis(123L);
        Long toReturn = 12L;
        doReturn(toReturn).when(mockedConsumer).position(topicPartition, duration);

        Long result = wrappedConsumer.position(topicPartition, duration);

        assertEquals(toReturn, result);
        verify(mockedConsumer, times(1)).position(topicPartition, duration);
    }


    private Set<Triple<TopicPartition, OffsetAndMetadata, OffsetAndMetadata>> getCommittedTestData() {
        Set<Triple<TopicPartition, OffsetAndMetadata, OffsetAndMetadata>> testSet = new HashSet<>();
        final long offset = 66L;
        final TopicPartition KEY_T1 = new TopicPartition("test", 1);
        final OffsetAndMetadata VALUE_T1 = new OffsetAndMetadata(offset, null);
        final OffsetAndMetadata EXPECTED_VALUE_T1 = VALUE_T1;
        testSet.add(Triple.of(KEY_T1, VALUE_T1, EXPECTED_VALUE_T1));

        final TopicPartition KEY_T2 = new TopicPartition("test", 2);
        final OffsetAndMetadata VALUE_T2 = new OffsetAndMetadata(offset, "");
        final OffsetAndMetadata EXPECTED_VALUE_T2 = VALUE_T2;
        testSet.add(Triple.of(KEY_T2, VALUE_T2, EXPECTED_VALUE_T2));

        final TopicPartition KEY_T3 = new TopicPartition("test", 3);
        final OffsetAndMetadata VALUE_T3 = new OffsetAndMetadata(offset, "ShouldNotChange");
        final OffsetAndMetadata EXPECTED_VALUE_T3 = VALUE_T3;
        testSet.add(Triple.of(KEY_T3, VALUE_T3, EXPECTED_VALUE_T3));

        final TopicPartition KEY_T4 = new TopicPartition("test", 4);
        final OffsetAndMetadata VALUE_T4 = new OffsetAndMetadata(offset, "{ This is not JSON, just a trick start }");
        final OffsetAndMetadata EXPECTED_VALUE_T4 = VALUE_T4;
        testSet.add(Triple.of(KEY_T4, VALUE_T4, EXPECTED_VALUE_T4));

        final TopicPartition KEY_T5 = new TopicPartition("test", 5);
        final OffsetAndMetadata VALUE_T5 = new OffsetAndMetadata(offset, "{ \"copyFlags\":1 }");
        final OffsetAndMetadata EXPECTED_VALUE_T5 = new OffsetAndMetadata(offset, null);
        testSet.add(Triple.of(KEY_T5, VALUE_T5, EXPECTED_VALUE_T5));

        return testSet;
    }

    @Test
    public void committed_SinglePartitionWithoutDuration() {
        Set<Triple<TopicPartition, OffsetAndMetadata, OffsetAndMetadata>> testSet = getCommittedTestData();
        for (Triple<TopicPartition, OffsetAndMetadata, OffsetAndMetadata> testData : testSet) {
            doReturn(testData.getMiddle()).when(mockedConsumer).committed(testData.getLeft());
            OffsetAndMetadata result = wrappedConsumer.committed(testData.getLeft());
            verify(mockedConsumer, times(1)).committed(testData.getLeft());
            assertEquals(testData.getRight(), result);
        }
    }

    @Test
    public void committed_SinglePartitionWithDuration() {
        Set<Triple<TopicPartition, OffsetAndMetadata, OffsetAndMetadata>> testSet = getCommittedTestData();
        Duration duration = Duration.ofMillis(123L);

        for (Triple<TopicPartition, OffsetAndMetadata, OffsetAndMetadata> testData : testSet) {
            doReturn(testData.getMiddle()).when(mockedConsumer).committed(testData.getLeft(), duration);
            OffsetAndMetadata result = wrappedConsumer.committed(testData.getLeft(), duration);
            verify(mockedConsumer, times(1)).committed(testData.getLeft(), duration);
            assertEquals(testData.getRight(), result);
        }
    }

    @Test
    public void committed_MultiplePartitionsWithoutDuration() {
        Set<TopicPartition> topicPartitions = Collections.emptySet();
        Set<Triple<TopicPartition, OffsetAndMetadata, OffsetAndMetadata>> testSet = getCommittedTestData();

        Map<TopicPartition, OffsetAndMetadata> toReturn = new HashMap<>();
        for (Triple<TopicPartition, OffsetAndMetadata, OffsetAndMetadata> testData : testSet) {
            toReturn.put(testData.getLeft(), testData.getMiddle());
        }

        doReturn(toReturn).when(mockedConsumer).committed(topicPartitions);

        Map<TopicPartition, OffsetAndMetadata> result = wrappedConsumer.committed(topicPartitions);

        verify(mockedConsumer, times(1)).committed(topicPartitions);

        assertNotNull(result);
        for (Triple<TopicPartition, OffsetAndMetadata, OffsetAndMetadata> testData : testSet) {
            assertEquals(testData.getRight(), result.get(testData.getLeft()));
        }
    }

    @Test
    public void committed_MultiplePartitionsWithDuration() {
        Set<TopicPartition> topicPartitions = Collections.emptySet();
        Duration duration = Duration.ofMillis(123L);

        Set<Triple<TopicPartition, OffsetAndMetadata, OffsetAndMetadata>> testSet = getCommittedTestData();

        Map<TopicPartition, OffsetAndMetadata> toReturn = new HashMap<>();
        for (Triple<TopicPartition, OffsetAndMetadata, OffsetAndMetadata> testData : testSet) {
            toReturn.put(testData.getLeft(), testData.getMiddle());
        }

        doReturn(toReturn).when(mockedConsumer).committed(topicPartitions, duration);

        Map<TopicPartition, OffsetAndMetadata> result = wrappedConsumer.committed(topicPartitions, duration);

        verify(mockedConsumer, times(1)).committed(topicPartitions, duration);

        assertNotNull(result);
        assertNotNull(result);
        for (Triple<TopicPartition, OffsetAndMetadata, OffsetAndMetadata> testData : testSet) {
            assertEquals(testData.getRight(), result.get(testData.getLeft()));
        }
    }

    @Test
    public void metrics() {
        Map<MetricName, Metric> toReturn = Collections.emptyMap();
        doReturn(toReturn).when(mockedConsumer).metrics();

        Map<MetricName, ? extends Metric> result = wrappedConsumer.metrics();
        assertEquals(toReturn, result);
        verify(mockedConsumer, times(1)).metrics();
    }

    @Test
    public void partitionsFor_WithoutDuration() {
        String topic = "tp";
        List<PartitionInfo> toReturn = Collections.emptyList();
        doReturn(toReturn).when(mockedConsumer).partitionsFor(topic);

        List<PartitionInfo> result = wrappedConsumer.partitionsFor(topic);

        assertEquals(toReturn, result);
        verify(mockedConsumer, times(1)).partitionsFor(topic);
    }

    @Test
    public void partitionsFor_WithDuration() {
        String topic = "tp";
        Duration duration = Duration.ofMillis(123L);
        List<PartitionInfo> toReturn = Collections.emptyList();
        doReturn(toReturn).when(mockedConsumer).partitionsFor(topic, duration);

        List<PartitionInfo> result = wrappedConsumer.partitionsFor(topic, duration);

        assertEquals(toReturn, result);
        verify(mockedConsumer, times(1)).partitionsFor(topic, duration);
    }

    @Test
    public void listTopics_WithoutDuration() {
        Map<String, List<PartitionInfo>> toReturn = Collections.emptyMap();
        doReturn(toReturn).when(mockedConsumer).listTopics();

        Map<String, List<PartitionInfo>> result = wrappedConsumer.listTopics();
        assertEquals(toReturn, result);
        verify(mockedConsumer, times(1)).listTopics();
    }

    @Test
    public void listTopics_WithDuration() {
        Duration duration = Duration.ofMillis(123L);
        Map<String, List<PartitionInfo>> toReturn = Collections.emptyMap();
        doReturn(toReturn).when(mockedConsumer).listTopics(duration);

        Map<String, List<PartitionInfo>> result = wrappedConsumer.listTopics(duration);
        assertEquals(toReturn, result);
        verify(mockedConsumer, times(1)).listTopics(duration);
    }

    @Test
    public void paused() {
        Set<TopicPartition> toReturn = Collections.emptySet();
        doReturn(toReturn).when(mockedConsumer).paused();

        Set<TopicPartition> result = wrappedConsumer.paused();
        assertEquals(toReturn, result);
        verify(mockedConsumer, times(1)).paused();
    }

    @Test
    public void pause() {
        Collection<TopicPartition> partitions = Collections.emptySet();
        doNothing().when(mockedConsumer).pause(partitions);

        wrappedConsumer.pause(partitions);
        verify(mockedConsumer, times(1)).pause(partitions);
    }

    @Test
    public void resume() {
        Collection<TopicPartition> partitions = Collections.emptySet();
        doNothing().when(mockedConsumer).resume(partitions);

        wrappedConsumer.resume(partitions);
        verify(mockedConsumer, times(1)).resume(partitions);
    }

    @Test
    public void offsetsForTimes_WithoutDuration() {
        Map<TopicPartition, Long> timestamps = Collections.emptyMap();
        Map<TopicPartition, OffsetAndTimestamp> toReturn = Collections.emptyMap();
        doReturn(toReturn).when(mockedConsumer).offsetsForTimes(timestamps);

        Map<TopicPartition, OffsetAndTimestamp> result = wrappedConsumer.offsetsForTimes(timestamps);
        assertEquals(toReturn, result);
        verify(mockedConsumer, times(1)).offsetsForTimes(timestamps);
    }

    @Test
    public void offsetsForTimes_WithDuration() {
        Duration duration = Duration.ofMillis(234L);
        Map<TopicPartition, Long> timestamps = Collections.emptyMap();
        Map<TopicPartition, OffsetAndTimestamp> toReturn = Collections.emptyMap();
        doReturn(toReturn).when(mockedConsumer).offsetsForTimes(timestamps, duration);

        Map<TopicPartition, OffsetAndTimestamp> result = wrappedConsumer.offsetsForTimes(timestamps, duration);
        assertEquals(toReturn, result);
        verify(mockedConsumer, times(1)).offsetsForTimes(timestamps, duration);
    }

    @Test
    public void beginningOffsets_WithoutDuration() {
        Collection<TopicPartition> topicPartitions = Collections.emptySet();
        Map<TopicPartition, Long> toReturn = Collections.emptyMap();
        doReturn(toReturn).when(mockedConsumer).beginningOffsets(topicPartitions);

        Map<TopicPartition, Long> result = wrappedConsumer.beginningOffsets(topicPartitions);
        assertEquals(toReturn, result);
        verify(mockedConsumer, times(1)).beginningOffsets(topicPartitions);
    }

    @Test
    public void beginningOffsets_WitDuration() {
        Collection<TopicPartition> topicPartitions = Collections.emptySet();
        Map<TopicPartition, Long> toReturn = Collections.emptyMap();
        Duration duration = Duration.ofMillis(234L);
        doReturn(toReturn).when(mockedConsumer).beginningOffsets(topicPartitions, duration);

        Map<TopicPartition, Long> result = wrappedConsumer.beginningOffsets(topicPartitions, duration);
        assertEquals(toReturn, result);
        verify(mockedConsumer, times(1)).beginningOffsets(topicPartitions, duration);
    }

    @Test
    public void endOffsets_WithoutDuration() {
        Collection<TopicPartition> topicPartitions = Collections.emptySet();
        Map<TopicPartition, Long> toReturn = Collections.emptyMap();
        doReturn(toReturn).when(mockedConsumer).endOffsets(topicPartitions);

        Map<TopicPartition, Long> result = wrappedConsumer.endOffsets(topicPartitions);
        assertEquals(toReturn, result);
        verify(mockedConsumer, times(1)).endOffsets(topicPartitions);
    }

    @Test
    public void endOffsets_WitDuration() {
        Collection<TopicPartition> topicPartitions = Collections.emptySet();
        Map<TopicPartition, Long> toReturn = Collections.emptyMap();
        Duration duration = Duration.ofMillis(234L);
        doReturn(toReturn).when(mockedConsumer).endOffsets(topicPartitions, duration);

        Map<TopicPartition, Long> result = wrappedConsumer.endOffsets(topicPartitions, duration);
        assertEquals(toReturn, result);
        verify(mockedConsumer, times(1)).endOffsets(topicPartitions, duration);
    }

    @Test
    public void groupMetadata() {
        ConsumerGroupMetadata toReturn = mock(ConsumerGroupMetadata.class);
        doReturn(toReturn).when(mockedConsumer).groupMetadata();

        ConsumerGroupMetadata result = wrappedConsumer.groupMetadata();
        assertEquals(toReturn, result);
        verify(mockedConsumer, times(1)).groupMetadata();
    }

    @Test
    public void enforceRebalance() {
        doNothing().when(mockedConsumer).enforceRebalance();

        wrappedConsumer.enforceRebalance();
        verify(mockedConsumer, times(1)).enforceRebalance();
    }

    @Test
    public void enforceRebalanceWithReason() {
        String reason = "reason";
        doNothing().when(mockedConsumer).enforceRebalance(reason);

        wrappedConsumer.enforceRebalance(reason);
        verify(mockedConsumer, times(1)).enforceRebalance(reason);
    }

    @Test
    public void close_WithDuration() {
        Duration duration = Duration.ofMillis(9L);
        doNothing().when(mockedConsumer).close(duration);

        wrappedConsumer.close(duration);
        verify(mockedConsumer, times(1)).close(duration);
    }

    @Test
    public void wakeup() {
        doNothing().when(mockedConsumer).wakeup();

        wrappedConsumer.wakeup();
        verify(mockedConsumer, times(1)).wakeup();
    }

    @Test
    public void currentLag() {
        TopicPartition topicPartition = new TopicPartition("topic", 0);
        when(mockedConsumer.currentLag(topicPartition)).thenReturn(OptionalLong.empty());

        wrappedConsumer.currentLag(topicPartition);
        verify(mockedConsumer, times(1)).currentLag(topicPartition);
    }
}