package io.axual.client.proxy.wrapped.admin;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 - 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.KafkaFuture;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.internals.KafkaFutureImpl;
import org.junit.Test;
import org.mockito.Mock;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import io.axual.client.proxy.wrapped.OffsetAndMetadataUtil;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class WrappedListConsumerGroupOffsetResultTest {

    @Mock
    public OffsetAndMetadataUtil util;

    @Test
    public void partitionsToOffsetAndMetadata() throws ExecutionException, InterruptedException {
        final Map<TopicPartition, OffsetAndMetadata> metadataMapInsert = Collections.singletonMap(new TopicPartition("Test",0), new OffsetAndMetadata(0L));
        final Map<TopicPartition, OffsetAndMetadata> metadataMapToReturn = new HashMap<>();

        OffsetAndMetadataUtil util = mock(OffsetAndMetadataUtil.class);
        doReturn(metadataMapToReturn).when(util).cleanMetadata(metadataMapInsert);

        KafkaFutureImpl<Map<TopicPartition, OffsetAndMetadata>> futures = new KafkaFutureImpl<>();

        WrappedListConsumerGroupOffsetResult toTest = new WrappedListConsumerGroupOffsetResult(futures, util);

        KafkaFuture<Map<TopicPartition, OffsetAndMetadata>> cleanedFuture = toTest.partitionsToOffsetAndMetadata();
        
        assertFalse(cleanedFuture.isDone());
        futures.complete(metadataMapInsert);
        assertTrue(cleanedFuture.isDone());

        assertEquals(metadataMapToReturn, cleanedFuture.get());
        verify(util, times(1)).cleanMetadata(metadataMapInsert);
    }
}