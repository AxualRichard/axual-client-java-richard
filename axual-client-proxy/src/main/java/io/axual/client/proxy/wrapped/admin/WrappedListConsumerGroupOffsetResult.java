package io.axual.client.proxy.wrapped.admin;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 - 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.admin.ExtendableListConsumerGroupOffsetsResult;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.KafkaFuture;
import org.apache.kafka.common.TopicPartition;

import java.util.Map;

import io.axual.client.proxy.wrapped.OffsetAndMetadataUtil;

public class WrappedListConsumerGroupOffsetResult extends ExtendableListConsumerGroupOffsetsResult {
    protected final OffsetAndMetadataUtil util;

    /**
     * For testing, mocks can be injected here
     *
     * @param future the future object containing the OffsetAndMetaData map
     * @param util   the OffsetAndMetadataUtil used for cleaning,
     */
    WrappedListConsumerGroupOffsetResult(
            KafkaFuture<Map<TopicPartition, OffsetAndMetadata>> future, OffsetAndMetadataUtil util) {
        super(future.thenApply(util::cleanMetadata));
        this.util = util;
    }

    /**
     * Build the result for the ListConsumerGroupOffsets and cleans the metadata parts
     *
     * @param future the original OffsetAndMetadata map to clean
     */
    public WrappedListConsumerGroupOffsetResult(
            KafkaFuture<Map<TopicPartition, OffsetAndMetadata>> future) {
        this(future, OffsetAndMetadataUtil.INSTANCE);
    }
}
