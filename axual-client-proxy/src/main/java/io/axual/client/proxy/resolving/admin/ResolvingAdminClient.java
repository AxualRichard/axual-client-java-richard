package io.axual.client.proxy.resolving.admin;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.admin.AlterClientQuotasOptions;
import org.apache.kafka.clients.admin.AlterClientQuotasResult;
import org.apache.kafka.clients.admin.AlterConfigOp;
import org.apache.kafka.clients.admin.AlterConfigsOptions;
import org.apache.kafka.clients.admin.AlterConfigsResult;
import org.apache.kafka.clients.admin.AlterConsumerGroupOffsetsOptions;
import org.apache.kafka.clients.admin.AlterConsumerGroupOffsetsResult;
import org.apache.kafka.clients.admin.AlterPartitionReassignmentsOptions;
import org.apache.kafka.clients.admin.AlterPartitionReassignmentsResult;
import org.apache.kafka.clients.admin.AlterReplicaLogDirsOptions;
import org.apache.kafka.clients.admin.AlterReplicaLogDirsResult;
import org.apache.kafka.clients.admin.Config;
import org.apache.kafka.clients.admin.CreateAclsOptions;
import org.apache.kafka.clients.admin.CreateAclsResult;
import org.apache.kafka.clients.admin.CreatePartitionsOptions;
import org.apache.kafka.clients.admin.CreatePartitionsResult;
import org.apache.kafka.clients.admin.CreateTopicsOptions;
import org.apache.kafka.clients.admin.CreateTopicsResult;
import org.apache.kafka.clients.admin.DeleteAclsOptions;
import org.apache.kafka.clients.admin.DeleteAclsResult;
import org.apache.kafka.clients.admin.DeleteConsumerGroupOffsetsOptions;
import org.apache.kafka.clients.admin.DeleteConsumerGroupOffsetsResult;
import org.apache.kafka.clients.admin.DeleteConsumerGroupsOptions;
import org.apache.kafka.clients.admin.DeleteConsumerGroupsResult;
import org.apache.kafka.clients.admin.DeleteRecordsOptions;
import org.apache.kafka.clients.admin.DeleteRecordsResult;
import org.apache.kafka.clients.admin.DeleteTopicsOptions;
import org.apache.kafka.clients.admin.DeleteTopicsResult;
import org.apache.kafka.clients.admin.DescribeAclsOptions;
import org.apache.kafka.clients.admin.DescribeAclsResult;
import org.apache.kafka.clients.admin.DescribeClientQuotasOptions;
import org.apache.kafka.clients.admin.DescribeClientQuotasResult;
import org.apache.kafka.clients.admin.DescribeConfigsOptions;
import org.apache.kafka.clients.admin.DescribeConfigsResult;
import org.apache.kafka.clients.admin.DescribeConsumerGroupsOptions;
import org.apache.kafka.clients.admin.DescribeConsumerGroupsResult;
import org.apache.kafka.clients.admin.DescribeLogDirsOptions;
import org.apache.kafka.clients.admin.DescribeLogDirsResult;
import org.apache.kafka.clients.admin.DescribeReplicaLogDirsOptions;
import org.apache.kafka.clients.admin.DescribeReplicaLogDirsResult;
import org.apache.kafka.clients.admin.DescribeTopicsOptions;
import org.apache.kafka.clients.admin.DescribeTopicsResult;
import org.apache.kafka.clients.admin.ElectLeadersOptions;
import org.apache.kafka.clients.admin.ElectLeadersResult;
import org.apache.kafka.clients.admin.ListConsumerGroupOffsetsOptions;
import org.apache.kafka.clients.admin.ListConsumerGroupOffsetsResult;
import org.apache.kafka.clients.admin.ListConsumerGroupsOptions;
import org.apache.kafka.clients.admin.ListConsumerGroupsResult;
import org.apache.kafka.clients.admin.ListOffsetsOptions;
import org.apache.kafka.clients.admin.ListOffsetsResult;
import org.apache.kafka.clients.admin.ListPartitionReassignmentsOptions;
import org.apache.kafka.clients.admin.ListPartitionReassignmentsResult;
import org.apache.kafka.clients.admin.ListTopicsOptions;
import org.apache.kafka.clients.admin.ListTopicsResult;
import org.apache.kafka.clients.admin.NewPartitionReassignment;
import org.apache.kafka.clients.admin.NewPartitions;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.admin.OffsetSpec;
import org.apache.kafka.clients.admin.RecordsToDelete;
import org.apache.kafka.clients.admin.RemoveMembersFromConsumerGroupOptions;
import org.apache.kafka.clients.admin.RemoveMembersFromConsumerGroupResult;
import org.apache.kafka.clients.admin.TopicDescription;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.ElectionType;
import org.apache.kafka.common.KafkaFuture;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.TopicPartitionReplica;
import org.apache.kafka.common.Uuid;
import org.apache.kafka.common.acl.AclBinding;
import org.apache.kafka.common.acl.AclBindingFilter;
import org.apache.kafka.common.config.ConfigResource;
import org.apache.kafka.common.quota.ClientQuotaAlteration;
import org.apache.kafka.common.quota.ClientQuotaFilter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

import io.axual.client.proxy.generic.admin.AdminProxy;
import io.axual.client.proxy.generic.admin.StaticAdminProxy;
import io.axual.common.resolver.GroupResolver;
import io.axual.common.resolver.TopicResolver;
import io.axual.common.tools.MapUtil;

public class ResolvingAdminClient extends StaticAdminProxy<ResolvingAdminConfig> implements
    AdminProxy {

  public ResolvingAdminClient(Map<String, Object> configs) {
    super(new ResolvingAdminConfig(configs));
  }

  public static ResolvingAdminClient create(Properties properties) {
    return new ResolvingAdminClient(MapUtil.objectToStringMap(properties));
  }

  public static ResolvingAdminClient create(Map<String, Object> configs) {
    return new ResolvingAdminClient(configs);
  }

  @Override
  public CreateTopicsResult createTopics(Collection<NewTopic> newTopics,
      CreateTopicsOptions options) {
    CreateTopicsResult result = super.createTopics(resolveNewTopics(newTopics), options);
    return new ResolvingCreateTopicsResult(result, config.getTopicResolver());
  }

  @Override
  public DeleteTopicsResult deleteTopics(Collection<String> topics) {
    return deleteTopics(topics, new DeleteTopicsOptions());
  }

  @Override
  public DeleteTopicsResult deleteTopics(Collection<String> topics, DeleteTopicsOptions options) {
    DeleteTopicsResult result = super
        .deleteTopics(config.getTopicResolver().resolveTopics(topics), options);

    TopicResolver resolver = config.getTopicResolver();
    Map<Uuid, KafkaFuture<Void>> topicIdValues = result.topicIdValues();
    Map<String, KafkaFuture<Void>> topicNameValues = result.topicNameValues();

    return new ResolvingDeleteTopicsResult(
            topicIdValues,
            topicNameValues != null ? ResolverUtil.unresolve(topicNameValues, resolver) : null);
  }

  @Override
  public ListTopicsResult listTopics(ListTopicsOptions options) {
    return new ResolvingListTopicsResult(proxiedObject.listTopics(options),
        config.getTopicResolver());
  }

  @Override
  public DescribeTopicsResult describeTopics(Collection<String> topicNames,
      DescribeTopicsOptions options) {
    DescribeTopicsResult result = super
        .describeTopics(config.getTopicResolver().resolveTopics(topicNames), options);

    TopicResolver resolver = config.getTopicResolver();
    Map<Uuid, KafkaFuture<TopicDescription>> topicIdValues = result.topicIdValues();
    Map<String, KafkaFuture<TopicDescription>> topicNameValues = result.topicNameValues();

    return new ResolvingDescribeTopicsResult(
            topicIdValues,
            topicNameValues != null ? ResolverUtil.unresolve(topicNameValues, resolver) : null,
            resolver);
  }

  @Override
  public DescribeAclsResult describeAcls(AclBindingFilter filter, DescribeAclsOptions options) {
    DescribeAclsResult result = super.describeAcls(
        ResolverUtil.resolve(filter, config.getTopicResolver(), config.getGroupResolver()),
        options);
    return result != null
        ? new ResolvingDescribeAclsResult(result.values(), config.getTopicResolver(),
        config.getGroupResolver())
        : null;
  }

  @Override
  public CreateAclsResult createAcls(Collection<AclBinding> acls, CreateAclsOptions options) {
    operationNotSupported("createAcls");
    return null;
  }

  @Override
  public DeleteAclsResult deleteAcls(Collection<AclBindingFilter> filters,
      DeleteAclsOptions options) {
    operationNotSupported("deleteAcls");
    return null;
  }

  @Override
  public DescribeConfigsResult describeConfigs(Collection<ConfigResource> resources,
      DescribeConfigsOptions options) {
    operationNotSupported("describeConfigs");
    return null;
  }

  /**
   * @deprecated
   */
  @Deprecated
  @Override
  public synchronized AlterConfigsResult alterConfigs(Map<ConfigResource, Config> configs,
      AlterConfigsOptions options) {
    operationNotSupported("alterConfigs");
    return null;
  }

  @Override
  public AlterConfigsResult incrementalAlterConfigs(
      Map<ConfigResource, Collection<AlterConfigOp>> configs, AlterConfigsOptions options) {
    operationNotSupported("incrementalAlterConfigs");
    return null;
  }

  @Override
  public AlterReplicaLogDirsResult alterReplicaLogDirs(
      Map<TopicPartitionReplica, String> replicaAssignment, AlterReplicaLogDirsOptions options) {
    operationNotSupported("alterReplicaLogDirs");
    return null;
  }

  @Override
  public DescribeLogDirsResult describeLogDirs(Collection<Integer> brokers,
      DescribeLogDirsOptions options) {
    operationNotSupported("describeLogDirs");
    return null;
  }

  @Override
  public DescribeReplicaLogDirsResult describeReplicaLogDirs(
      Collection<TopicPartitionReplica> replicas, DescribeReplicaLogDirsOptions options) {
    operationNotSupported("describeReplicaLogDirs");
    return null;
  }

  @Override
  public CreatePartitionsResult createPartitions(Map<String, NewPartitions> newPartitions,
      CreatePartitionsOptions options) {
    operationNotSupported("createPartitions");
    return null;
  }

  @Override
  public DeleteRecordsResult deleteRecords(Map<TopicPartition, RecordsToDelete> recordsToDelete,
      DeleteRecordsOptions options) {
    return super.deleteRecords(config.getTopicResolver().resolveTopics(recordsToDelete), options);
  }

  ///////////////////////////////////////////////////////////////////////////////////////////////
  // End of public interface of AdminClient
  ///////////////////////////////////////////////////////////////////////////////////////////////

  private Collection<NewTopic> resolveNewTopics(Collection<NewTopic> newTopics) {
    // Resolve all topic names into a new collection
    List<NewTopic> resolvedTopics = new ArrayList<>();
    for (NewTopic newTopic : newTopics) {
      final NewTopic resolvedNewTopic;
      if (newTopic.replicasAssignments() == null) {
        resolvedNewTopic = new NewTopic(
            config.getTopicResolver().resolveTopic(newTopic.name()),
            newTopic.numPartitions(),
            newTopic.replicationFactor());
      } else {
        resolvedNewTopic = new NewTopic(
            config.getTopicResolver().resolveTopic(newTopic.name()),
            newTopic.replicasAssignments());
      }
      resolvedTopics.add(resolvedNewTopic);
    }
    return resolvedTopics;
  }

  @Override
  public DescribeConsumerGroupsResult describeConsumerGroups(Collection<String> groupIds,
      DescribeConsumerGroupsOptions options) {
    final GroupResolver groupResolver = config.getGroupResolver();
    return new ResolvingDescribeConsumerGroupsResult(
        super.describeConsumerGroups(groupResolver.resolveGroups(groupIds), options)
            .describedGroups(), groupResolver);
  }

  @Override
  public ListConsumerGroupOffsetsResult listConsumerGroupOffsets(String groupId,
      ListConsumerGroupOffsetsOptions options) {
    ListConsumerGroupOffsetsOptions resolvedOptions = new ListConsumerGroupOffsetsOptions();

    if(options != null && options.topicPartitions() != null){
      TopicResolver resolver = config.getTopicResolver();
      List<TopicPartition> partitions = options.topicPartitions().stream().map(resolver::resolveTopic).collect(Collectors.toList());
      resolvedOptions.topicPartitions(partitions);
    }

    return new ResolvingListConsumerGroupOffsetsResult(
        super.listConsumerGroupOffsets(config.getGroupResolver().resolveGroup(groupId), resolvedOptions)
            .partitionsToOffsetAndMetadata(), config.getTopicResolver());
  }

  @Override
  public ListConsumerGroupsResult listConsumerGroups(ListConsumerGroupsOptions options) {
    return new ResolvingListConsumerGroupsResult(super.listConsumerGroups(options),
        config.getGroupResolver());
  }

  @Override
  public DeleteConsumerGroupsResult deleteConsumerGroups(Collection<String> groupIds,
      DeleteConsumerGroupsOptions options) {
    final GroupResolver groupResolver = config.getGroupResolver();
    return new ResolvingDeleteConsumerGroupsResult(
        super.deleteConsumerGroups(groupResolver.resolveGroups(groupIds), options).deletedGroups(),
        groupResolver);
  }

  @Override
  public DeleteConsumerGroupOffsetsResult deleteConsumerGroupOffsets(String groupId,
      Set<TopicPartition> partitions, DeleteConsumerGroupOffsetsOptions options) {
    final TopicResolver topicResolver = config.getTopicResolver();
    return new ResolvingDeleteConsumerGroupOffsetsResult(
        super.deleteConsumerGroupOffsets(config.getGroupResolver().resolveGroup(groupId)
            , topicResolver.resolveTopicPartitions(partitions), options), topicResolver);
  }

  @Override
  public ElectLeadersResult electLeaders(ElectionType electionType, Set<TopicPartition> partitions,
      ElectLeadersOptions options) {
    operationNotSupported("electLeaders");
    return null;
  }

  @Override
  public AlterPartitionReassignmentsResult alterPartitionReassignments(
      Map<TopicPartition, Optional<NewPartitionReassignment>> reassignments,
      AlterPartitionReassignmentsOptions options) {
    operationNotSupported("alterPartitionReassignments");
    return null;
  }

  @Override
  public ListPartitionReassignmentsResult listPartitionReassignments(
      Optional<Set<TopicPartition>> partitions, ListPartitionReassignmentsOptions options) {
    operationNotSupported("listPartitionReassignments");
    return null;
  }

  @Override
  public RemoveMembersFromConsumerGroupResult removeMembersFromConsumerGroup(String groupId,
      RemoveMembersFromConsumerGroupOptions options) {
    return super
        .removeMembersFromConsumerGroup(config.getGroupResolver().resolveGroup(groupId), options);
  }

  @Override
  public AlterConsumerGroupOffsetsResult alterConsumerGroupOffsets(String groupId,
      Map<TopicPartition, OffsetAndMetadata> offsets, AlterConsumerGroupOffsetsOptions options) {
    final TopicResolver topicResolver = config.getTopicResolver();
    return new ResolvingAlterConsumerGroupOffsetsResult(super.alterConsumerGroupOffsets(
        config.getGroupResolver().resolveGroup(groupId),
        topicResolver.resolveTopics(offsets), options), topicResolver);
  }

  @Override
  public ListOffsetsResult listOffsets(Map<TopicPartition, OffsetSpec> topicPartitionOffsets,
      ListOffsetsOptions options) {
    final TopicResolver topicResolver = config.getTopicResolver();
    return new ResolvingListOffsetsResult(
        super.listOffsets(topicResolver.resolveTopics(topicPartitionOffsets), options),
        topicResolver);
  }

  @Override
  public DescribeClientQuotasResult describeClientQuotas(ClientQuotaFilter filter,
      DescribeClientQuotasOptions options) {
    operationNotSupported("describeClientQuotas");
    return null;
  }

  @Override
  public AlterClientQuotasResult alterClientQuotas(Collection<ClientQuotaAlteration> entries,
      AlterClientQuotasOptions options) {
    operationNotSupported("alterClientQuotas");
    return null;
  }
}
