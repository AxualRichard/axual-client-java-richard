package io.axual.client.proxy.generic.admin;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import io.axual.client.proxy.axual.admin.AxualAdminClient;
import java.util.Map;
import java.util.Properties;

public interface Admin extends org.apache.kafka.clients.admin.Admin {

  /**
   * Create a new Admin with the given configuration.
   *
   * @param props The configuration.
   * @return The new KafkaAdminClient.
   */
  static org.apache.kafka.clients.admin.Admin create(Properties props) {
    return new AxualAdminClient(props);
  }

  /**
   * Create a new Admin with the given configuration.
   *
   * @param conf The configuration.
   * @return The new KafkaAdminClient.
   */
  static org.apache.kafka.clients.admin.Admin create(Map<String, Object> conf) {
    return new AxualAdminClient(conf);
  }

}
