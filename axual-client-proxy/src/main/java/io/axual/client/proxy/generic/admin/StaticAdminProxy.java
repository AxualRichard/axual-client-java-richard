package io.axual.client.proxy.generic.admin;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.admin.AbortTransactionOptions;
import org.apache.kafka.clients.admin.AbortTransactionResult;
import org.apache.kafka.clients.admin.AbortTransactionSpec;
import org.apache.kafka.clients.admin.AlterClientQuotasOptions;
import org.apache.kafka.clients.admin.AlterClientQuotasResult;
import org.apache.kafka.clients.admin.AlterConfigOp;
import org.apache.kafka.clients.admin.AlterConfigsOptions;
import org.apache.kafka.clients.admin.AlterConfigsResult;
import org.apache.kafka.clients.admin.AlterConsumerGroupOffsetsOptions;
import org.apache.kafka.clients.admin.AlterConsumerGroupOffsetsResult;
import org.apache.kafka.clients.admin.AlterPartitionReassignmentsOptions;
import org.apache.kafka.clients.admin.AlterPartitionReassignmentsResult;
import org.apache.kafka.clients.admin.AlterReplicaLogDirsOptions;
import org.apache.kafka.clients.admin.AlterReplicaLogDirsResult;
import org.apache.kafka.clients.admin.AlterUserScramCredentialsOptions;
import org.apache.kafka.clients.admin.AlterUserScramCredentialsResult;
import org.apache.kafka.clients.admin.Config;
import org.apache.kafka.clients.admin.CreateAclsOptions;
import org.apache.kafka.clients.admin.CreateAclsResult;
import org.apache.kafka.clients.admin.CreateDelegationTokenOptions;
import org.apache.kafka.clients.admin.CreateDelegationTokenResult;
import org.apache.kafka.clients.admin.CreatePartitionsOptions;
import org.apache.kafka.clients.admin.CreatePartitionsResult;
import org.apache.kafka.clients.admin.CreateTopicsOptions;
import org.apache.kafka.clients.admin.CreateTopicsResult;
import org.apache.kafka.clients.admin.DeleteAclsOptions;
import org.apache.kafka.clients.admin.DeleteAclsResult;
import org.apache.kafka.clients.admin.DeleteConsumerGroupOffsetsOptions;
import org.apache.kafka.clients.admin.DeleteConsumerGroupOffsetsResult;
import org.apache.kafka.clients.admin.DeleteConsumerGroupsOptions;
import org.apache.kafka.clients.admin.DeleteConsumerGroupsResult;
import org.apache.kafka.clients.admin.DeleteRecordsOptions;
import org.apache.kafka.clients.admin.DeleteRecordsResult;
import org.apache.kafka.clients.admin.DeleteTopicsOptions;
import org.apache.kafka.clients.admin.DeleteTopicsResult;
import org.apache.kafka.clients.admin.DescribeAclsOptions;
import org.apache.kafka.clients.admin.DescribeAclsResult;
import org.apache.kafka.clients.admin.DescribeClientQuotasOptions;
import org.apache.kafka.clients.admin.DescribeClientQuotasResult;
import org.apache.kafka.clients.admin.DescribeClusterOptions;
import org.apache.kafka.clients.admin.DescribeClusterResult;
import org.apache.kafka.clients.admin.DescribeConfigsOptions;
import org.apache.kafka.clients.admin.DescribeConfigsResult;
import org.apache.kafka.clients.admin.DescribeConsumerGroupsOptions;
import org.apache.kafka.clients.admin.DescribeConsumerGroupsResult;
import org.apache.kafka.clients.admin.DescribeDelegationTokenOptions;
import org.apache.kafka.clients.admin.DescribeDelegationTokenResult;
import org.apache.kafka.clients.admin.DescribeFeaturesOptions;
import org.apache.kafka.clients.admin.DescribeFeaturesResult;
import org.apache.kafka.clients.admin.DescribeLogDirsOptions;
import org.apache.kafka.clients.admin.DescribeLogDirsResult;
import org.apache.kafka.clients.admin.DescribeProducersOptions;
import org.apache.kafka.clients.admin.DescribeProducersResult;
import org.apache.kafka.clients.admin.DescribeReplicaLogDirsOptions;
import org.apache.kafka.clients.admin.DescribeReplicaLogDirsResult;
import org.apache.kafka.clients.admin.DescribeTopicsOptions;
import org.apache.kafka.clients.admin.DescribeTopicsResult;
import org.apache.kafka.clients.admin.DescribeTransactionsOptions;
import org.apache.kafka.clients.admin.DescribeTransactionsResult;
import org.apache.kafka.clients.admin.DescribeUserScramCredentialsOptions;
import org.apache.kafka.clients.admin.DescribeUserScramCredentialsResult;
import org.apache.kafka.clients.admin.ElectLeadersOptions;
import org.apache.kafka.clients.admin.ElectLeadersResult;
import org.apache.kafka.clients.admin.ExpireDelegationTokenOptions;
import org.apache.kafka.clients.admin.ExpireDelegationTokenResult;
import org.apache.kafka.clients.admin.FeatureUpdate;
import org.apache.kafka.clients.admin.FenceProducersOptions;
import org.apache.kafka.clients.admin.FenceProducersResult;
import org.apache.kafka.clients.admin.ListConsumerGroupOffsetsOptions;
import org.apache.kafka.clients.admin.ListConsumerGroupOffsetsResult;
import org.apache.kafka.clients.admin.ListConsumerGroupsOptions;
import org.apache.kafka.clients.admin.ListConsumerGroupsResult;
import org.apache.kafka.clients.admin.ListOffsetsOptions;
import org.apache.kafka.clients.admin.ListOffsetsResult;
import org.apache.kafka.clients.admin.ListPartitionReassignmentsOptions;
import org.apache.kafka.clients.admin.ListPartitionReassignmentsResult;
import org.apache.kafka.clients.admin.ListTopicsOptions;
import org.apache.kafka.clients.admin.ListTopicsResult;
import org.apache.kafka.clients.admin.ListTransactionsOptions;
import org.apache.kafka.clients.admin.ListTransactionsResult;
import org.apache.kafka.clients.admin.NewPartitionReassignment;
import org.apache.kafka.clients.admin.NewPartitions;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.admin.OffsetSpec;
import org.apache.kafka.clients.admin.RecordsToDelete;
import org.apache.kafka.clients.admin.RemoveMembersFromConsumerGroupOptions;
import org.apache.kafka.clients.admin.RemoveMembersFromConsumerGroupResult;
import org.apache.kafka.clients.admin.RenewDelegationTokenOptions;
import org.apache.kafka.clients.admin.RenewDelegationTokenResult;
import org.apache.kafka.clients.admin.UnregisterBrokerOptions;
import org.apache.kafka.clients.admin.UnregisterBrokerResult;
import org.apache.kafka.clients.admin.UpdateFeaturesOptions;
import org.apache.kafka.clients.admin.UpdateFeaturesResult;
import org.apache.kafka.clients.admin.UserScramCredentialAlteration;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.ElectionType;
import org.apache.kafka.common.Metric;
import org.apache.kafka.common.MetricName;
import org.apache.kafka.common.TopicCollection;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.TopicPartitionReplica;
import org.apache.kafka.common.acl.AclBinding;
import org.apache.kafka.common.acl.AclBindingFilter;
import org.apache.kafka.common.config.ConfigResource;
import org.apache.kafka.common.quota.ClientQuotaAlteration;
import org.apache.kafka.common.quota.ClientQuotaFilter;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import io.axual.client.proxy.generic.client.StaticClientProxy;
import io.axual.client.proxy.generic.config.BaseClientProxyConfig;

public class StaticAdminProxy<C extends BaseClientProxyConfig<AdminProxy>> extends
    StaticClientProxy<AdminProxy, C> implements AdminProxy {

  public StaticAdminProxy(C config) {
    super(config);
  }

  public StaticAdminProxy(ClientProxyInitializer<AdminProxy, C> initializer) {
    super(initializer);
  }

  @Override
  public CreateTopicsResult createTopics(Collection<NewTopic> newTopics,
      CreateTopicsOptions options) {
    return proxiedObject.createTopics(newTopics, options);
  }

  @Override
  public DeleteTopicsResult deleteTopics(Collection<String> topics, DeleteTopicsOptions options) {
    return proxiedObject.deleteTopics(topics, options);
  }

  @Override
  public DeleteTopicsResult deleteTopics(TopicCollection topicCollection, DeleteTopicsOptions deleteTopicsOptions) {
    return proxiedObject.deleteTopics(topicCollection, deleteTopicsOptions);
  }

  @Override
  public ListTopicsResult listTopics(ListTopicsOptions options) {
    return proxiedObject.listTopics(options);
  }

  @Override
  public DescribeTopicsResult describeTopics(Collection<String> topicNames,
      DescribeTopicsOptions options) {
    return proxiedObject.describeTopics(topicNames, options);
  }

  @Override
  public DescribeTopicsResult describeTopics(TopicCollection topicCollection, DescribeTopicsOptions describeTopicsOptions) {
    return proxiedObject.describeTopics(topicCollection, describeTopicsOptions);
  }

  @Override
  public DescribeClusterResult describeCluster(DescribeClusterOptions options) {
    return proxiedObject.describeCluster(options);
  }

  @Override
  public DescribeAclsResult describeAcls(AclBindingFilter filter, DescribeAclsOptions options) {
    return proxiedObject.describeAcls(filter, options);
  }

  @Override
  public CreateAclsResult createAcls(Collection<AclBinding> acls, CreateAclsOptions options) {
    return proxiedObject.createAcls(acls, options);
  }

  @Override
  public DeleteAclsResult deleteAcls(Collection<AclBindingFilter> filters,
      DeleteAclsOptions options) {
    return proxiedObject.deleteAcls(filters, options);
  }

  @Override
  public DescribeConfigsResult describeConfigs(Collection<ConfigResource> resources,
      DescribeConfigsOptions options) {
    return proxiedObject.describeConfigs(resources, options);
  }

  /**
   * @deprecated
   * @param configs
   * @param options
   * @return
   */
  @Deprecated
  @Override
  public AlterConfigsResult alterConfigs(Map<ConfigResource, Config> configs,
      AlterConfigsOptions options) {
    return proxiedObject.alterConfigs(configs, options);
  }

  @Override
  public AlterReplicaLogDirsResult alterReplicaLogDirs(
      Map<TopicPartitionReplica, String> replicaAssignment, AlterReplicaLogDirsOptions options) {
    return proxiedObject.alterReplicaLogDirs(replicaAssignment, options);
  }

  @Override
  public DescribeLogDirsResult describeLogDirs(Collection<Integer> brokers,
      DescribeLogDirsOptions options) {
    return proxiedObject.describeLogDirs(brokers, options);
  }

  @Override
  public DescribeReplicaLogDirsResult describeReplicaLogDirs(
      Collection<TopicPartitionReplica> replicas, DescribeReplicaLogDirsOptions options) {
    return proxiedObject.describeReplicaLogDirs(replicas, options);
  }

  @Override
  public CreatePartitionsResult createPartitions(Map<String, NewPartitions> newPartitions,
      CreatePartitionsOptions options) {
    return proxiedObject.createPartitions(newPartitions, options);
  }

  @Override
  public DeleteRecordsResult deleteRecords(Map<TopicPartition, RecordsToDelete> recordsToDelete,
      DeleteRecordsOptions options) {
    return proxiedObject.deleteRecords(recordsToDelete, options);
  }

  @Override
  public CreateDelegationTokenResult createDelegationToken(CreateDelegationTokenOptions options) {
    return proxiedObject.createDelegationToken(options);
  }

  @Override
  public RenewDelegationTokenResult renewDelegationToken(byte[] hmac,
      RenewDelegationTokenOptions options) {
    return proxiedObject.renewDelegationToken(hmac, options);
  }

  @Override
  public ExpireDelegationTokenResult expireDelegationToken(byte[] hmac,
      ExpireDelegationTokenOptions options) {
    return proxiedObject.expireDelegationToken(hmac, options);
  }

  @Override
  public DescribeDelegationTokenResult describeDelegationToken(
      DescribeDelegationTokenOptions options) {
    return proxiedObject.describeDelegationToken(options);
  }

  @Override
  public DescribeConsumerGroupsResult describeConsumerGroups(Collection<String> groupIds,
      DescribeConsumerGroupsOptions options) {
    return proxiedObject.describeConsumerGroups(groupIds, options);
  }

  @Override
  public ListConsumerGroupsResult listConsumerGroups(ListConsumerGroupsOptions options) {
    return proxiedObject.listConsumerGroups(options);
  }

  @Override
  public ListConsumerGroupOffsetsResult listConsumerGroupOffsets(String groupId,
      ListConsumerGroupOffsetsOptions options) {
    return proxiedObject.listConsumerGroupOffsets(groupId, options);
  }

  @Override
  public DeleteConsumerGroupsResult deleteConsumerGroups(Collection<String> groupIds,
      DeleteConsumerGroupsOptions options) {
    return proxiedObject.deleteConsumerGroups(groupIds, options);
  }

  @Override
  public DeleteConsumerGroupOffsetsResult deleteConsumerGroupOffsets(String groupId,
      Set<TopicPartition> partitions, DeleteConsumerGroupOffsetsOptions options) {
    return proxiedObject.deleteConsumerGroupOffsets(groupId, partitions, options);
  }

  @Override
  public AlterConfigsResult incrementalAlterConfigs(
      Map<ConfigResource, Collection<AlterConfigOp>> configs, AlterConfigsOptions options) {
    return proxiedObject.incrementalAlterConfigs(configs, options);
  }

  @Override
  public ElectLeadersResult electLeaders(ElectionType electionType,
      Set<TopicPartition> partitions,
      ElectLeadersOptions options) {
    return proxiedObject.electLeaders(electionType, partitions, options);
  }

  @Override
  public AlterPartitionReassignmentsResult alterPartitionReassignments(
      Map<TopicPartition, Optional<NewPartitionReassignment>> reassignments,
      AlterPartitionReassignmentsOptions options) {
    return proxiedObject.alterPartitionReassignments(reassignments, options);
  }

  @Override
  public ListPartitionReassignmentsResult listPartitionReassignments(
      Optional<Set<TopicPartition>> partitions, ListPartitionReassignmentsOptions options) {
    return proxiedObject.listPartitionReassignments(partitions, options);
  }

  @Override
  public RemoveMembersFromConsumerGroupResult removeMembersFromConsumerGroup(String groupId,
      RemoveMembersFromConsumerGroupOptions options) {
    return proxiedObject.removeMembersFromConsumerGroup(groupId, options);
  }

  @Override
  public AlterConsumerGroupOffsetsResult alterConsumerGroupOffsets(String groupId,
      Map<TopicPartition, OffsetAndMetadata> offsets, AlterConsumerGroupOffsetsOptions options) {
    return proxiedObject.alterConsumerGroupOffsets(groupId, offsets, options);
  }

  @Override
  public ListOffsetsResult listOffsets(Map<TopicPartition, OffsetSpec> topicPartitionOffsets,
      ListOffsetsOptions options) {
    return proxiedObject.listOffsets(topicPartitionOffsets, options);
  }

  @Override
  public DescribeClientQuotasResult describeClientQuotas(ClientQuotaFilter filter,
      DescribeClientQuotasOptions options) {
    return proxiedObject.describeClientQuotas(filter, options);
  }

  @Override
  public AlterClientQuotasResult alterClientQuotas(Collection<ClientQuotaAlteration> entries,
      AlterClientQuotasOptions options) {
    return proxiedObject.alterClientQuotas(entries, options);
  }

  @Override
  public DescribeUserScramCredentialsResult describeUserScramCredentials(List<String> list,
                                                                         DescribeUserScramCredentialsOptions options) {
    return proxiedObject.describeUserScramCredentials(list, options);
  }

  @Override
  public AlterUserScramCredentialsResult alterUserScramCredentials(List<UserScramCredentialAlteration> list,
                                                                   AlterUserScramCredentialsOptions options) {
    return proxiedObject.alterUserScramCredentials(list, options);
  }

  @Override
  public DescribeFeaturesResult describeFeatures(DescribeFeaturesOptions options) {
    return proxiedObject.describeFeatures(options);
  }

  @Override
  public UpdateFeaturesResult updateFeatures(Map<String, FeatureUpdate> map,
                                             UpdateFeaturesOptions options) {
    return proxiedObject.updateFeatures(map, options);
  }

  @Override
  public UnregisterBrokerResult unregisterBroker(int brokerId, UnregisterBrokerOptions options) {
    return proxiedObject.unregisterBroker(brokerId, options);
  }

  @Override
  public DescribeProducersResult describeProducers(Collection<TopicPartition> collection, DescribeProducersOptions describeProducersOptions) {
    return proxiedObject.describeProducers(collection, describeProducersOptions);
  }

  @Override
  public DescribeTransactionsResult describeTransactions(Collection<String> collection, DescribeTransactionsOptions describeTransactionsOptions) {
    return proxiedObject.describeTransactions(collection, describeTransactionsOptions);
  }

  @Override
  public AbortTransactionResult abortTransaction(AbortTransactionSpec abortTransactionSpec, AbortTransactionOptions abortTransactionOptions) {
    return proxiedObject.abortTransaction(abortTransactionSpec, abortTransactionOptions);
  }

  @Override
  public ListTransactionsResult listTransactions(ListTransactionsOptions listTransactionsOptions) {
    return proxiedObject.listTransactions(listTransactionsOptions);
  }

  @Override
  public FenceProducersResult fenceProducers(Collection<String> collection, FenceProducersOptions fenceProducersOptions) {
    return proxiedObject.fenceProducers(collection, fenceProducersOptions);
  }

  @Override
  public Map<MetricName, ? extends Metric> metrics() {
    return proxiedObject.metrics();
  }
}
