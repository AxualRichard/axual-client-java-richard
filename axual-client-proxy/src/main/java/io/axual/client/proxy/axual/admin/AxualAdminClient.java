package io.axual.client.proxy.axual.admin;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */


import io.axual.client.proxy.generic.admin.AdminProxy;
import io.axual.client.proxy.generic.admin.StaticAdminProxy;
import io.axual.client.proxy.generic.client.ClientProxyFactory;
import io.axual.client.proxy.generic.registry.ProxyChainUtil;
import io.axual.common.tools.MapUtil;
import java.util.Map;
import java.util.Properties;

public class AxualAdminClient extends StaticAdminProxy<AxualAdminConfig> implements AdminProxy {

  public AxualAdminClient(Map<String, Object> configs) {
    super(createChain(configs));
  }

  public AxualAdminClient(Properties properties) {
    this(MapUtil.objectToStringMap(properties));
  }

  private static ClientProxyInitializer<AdminProxy, AxualAdminConfig> createChain(
      Map<String, Object> configs) {
    AxualAdminConfig config = new AxualAdminConfig(configs);
    ClientProxyFactory<AdminProxy> factory = ProxyChainUtil
        .setupAdminFactoryChain(config.getProxyChain(), config.getBackingFactory());
    return new ClientProxyInitializer<>(config, factory.create(config.getDownstreamConfigs()));
  }
}
