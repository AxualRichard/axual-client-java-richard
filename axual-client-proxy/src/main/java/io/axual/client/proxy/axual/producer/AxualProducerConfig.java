package io.axual.client.proxy.axual.producer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.producer.ProducerConfig;

import java.util.Map;

import io.axual.client.proxy.axual.generic.AxualProxyConfig;
import io.axual.client.proxy.generic.producer.ProducerProxy;
import io.axual.client.proxy.wrapped.producer.WrappedProducerFactory;

public class AxualProducerConfig<K, V> extends AxualProxyConfig<ProducerProxy<K, V>> {
    public static final String BACKING_FACTORY_CONFIG = "axualproducer.backing.factory";
    public static final String CHAIN_CONFIG = "axualproducer.chain";

    public AxualProducerConfig(Map<String, Object> configs) {
        super(addDefaultFactory(configs, BACKING_FACTORY_CONFIG, WrappedProducerFactory.class),
                "producer",
                BACKING_FACTORY_CONFIG,
                CHAIN_CONFIG);

        // The type of acknowledgement we expect from the Kafka server
        putDownstreamIfAbsent(ProducerConfig.ACKS_CONFIG, "1");
        // Only allow a specific number of requests in flight
        putDownstreamIfAbsent(ProducerConfig.MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION, "5");
        // Don't retry
        putDownstreamIfAbsent(ProducerConfig.RETRIES_CONFIG, "0");
        // Relaxed retry backoff
        putDownstream(ProducerConfig.RETRY_BACKOFF_MS_CONFIG, "1000");
        // Relaxed reconnect
        putDownstream(ProducerConfig.RECONNECT_BACKOFF_MS_CONFIG, "1000");
    }
}
