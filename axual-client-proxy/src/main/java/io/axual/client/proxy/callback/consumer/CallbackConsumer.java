package io.axual.client.proxy.callback.consumer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.consumer.ConsumerGroupMetadata;
import org.apache.kafka.clients.consumer.ConsumerRebalanceListener;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.clients.consumer.OffsetAndTimestamp;
import org.apache.kafka.clients.consumer.OffsetCommitCallback;
import org.apache.kafka.common.Metric;
import org.apache.kafka.common.MetricName;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.Serializer;

import java.time.Duration;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.OptionalLong;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Pattern;

import io.axual.client.proxy.callback.client.CallbackClientProxy;
import io.axual.client.proxy.generic.consumer.ConsumerProxy;
import io.axual.client.proxy.generic.tools.SerdeUtil;
import io.axual.common.tools.MapUtil;

public class CallbackConsumer<K, V> extends CallbackClientProxy<ConsumerProxy<K, V>, CallbackConsumerConfig<K, V>> implements ConsumerProxy<K, V> {

    private static final String SUBSCRIBE = "subscribe";
    private static final String TOPICS = "topics";
    private static final String TIMEOUT = "timeout";
    private static final String COMMIT_SYNC = "commitSync";
    private static final String OFFSETS = "offsets";
    private static final String COMMIT_ASYNC = "commitAsync";
    private static final String PARTITION = "partition";
    private static final String PARTITIONS = "partitions";
    private static final String COMMITTED = "committed";

    public CallbackConsumer(Map<String, Object> configs) {
        super(new CallbackConsumerConfig<>(configs));
    }

    public CallbackConsumer(Map<String, Object> configs, Serializer<K> keySerializer, Serializer<V> valueSerializer) {
        this(SerdeUtil.addSerializersToConfigs(configs, keySerializer, valueSerializer));
    }

    public CallbackConsumer(Properties properties) {
        this(MapUtil.objectToStringMap(properties));
    }

    public CallbackConsumer(Properties properties, Serializer<K> keySerializer, Serializer<V> valueSerializer) {
        this(MapUtil.objectToStringMap(properties), keySerializer, valueSerializer);
    }

    @Override
    public Set<TopicPartition> assignment() {
        return interceptor.exec(this, "assignment", proxiedObject::assignment);
    }

    @Override
    public Set<String> subscription() {
        return interceptor.exec(this, "subscription", proxiedObject::subscription);
    }

    @Override
    public void subscribe(final Collection<String> topics) {
        interceptor.execProc(this, SUBSCRIBE, () -> proxiedObject.subscribe(topics), new String[]{TOPICS}, topics);
    }

    @Override
    public void subscribe(final Collection<String> topics, final ConsumerRebalanceListener listener) {
        interceptor.execProc(this, SUBSCRIBE, () -> proxiedObject.subscribe(topics, listener), new String[]{TOPICS, "listener"}, topics, listener);
    }

    @Override
    public void assign(final Collection<TopicPartition> topicPartitions) {
        interceptor.execProc(this, "assign", () -> proxiedObject.assign(topicPartitions), new String[]{"topicPartitions"}, topicPartitions);
    }

    @Override
    public void subscribe(final Pattern pattern, final ConsumerRebalanceListener listener) {
        interceptor.execProc(this, SUBSCRIBE, () -> proxiedObject.subscribe(pattern, listener), new String[]{"pattern", "listener"}, pattern, listener);
    }

    @Override
    public void subscribe(final Pattern pattern) {
        interceptor.execProc(this, SUBSCRIBE, () -> proxiedObject.subscribe(pattern), new String[]{"pattern"}, pattern);
    }

    @Override
    public void unsubscribe() {
        interceptor.execProc(this, "unsubscribe", proxiedObject::unsubscribe);
    }

    /**
     * @deprecated
     */
    @Deprecated
    @Override
    public ConsumerRecords<K, V> poll(final long timeout) {
        return interceptor.exec(this, "poll", () -> proxiedObject.poll(timeout), new String[]{TIMEOUT}, timeout);
    }

    @Override
    public ConsumerRecords<K, V> poll(Duration timeout) {
        return interceptor.exec(this, "poll", () -> proxiedObject.poll(timeout), new String[]{TIMEOUT}, timeout);
    }

    @Override
    public void commitSync() {
        interceptor.execProc(this, COMMIT_SYNC, proxiedObject::commitSync);
    }

    @Override
    public void commitSync(Duration timeout) {
        interceptor.execProc(this, COMMIT_SYNC, () -> proxiedObject.commitSync(timeout), new String[]{TIMEOUT}, timeout);

    }

    @Override
    public void commitSync(final Map<TopicPartition, OffsetAndMetadata> offsets) {
        interceptor.execProc(this, COMMIT_SYNC, () -> proxiedObject.commitSync(offsets), new String[]{OFFSETS}, offsets);
    }

    @Override
    public void commitSync(Map<TopicPartition, OffsetAndMetadata> offsets, Duration timeout) {
        interceptor.execProc(this, COMMIT_SYNC, () -> proxiedObject.commitSync(offsets, timeout), new String[]{OFFSETS, TIMEOUT}, offsets, timeout);
    }

    @Override
    public void commitAsync() {
        interceptor.execProc(this, COMMIT_ASYNC, proxiedObject::commitAsync);
    }

    @Override
    public void commitAsync(final OffsetCommitCallback callback) {
        interceptor.execProc(this, COMMIT_ASYNC, () -> proxiedObject.commitAsync(callback), new String[]{"callback"}, callback);
    }

    @Override
    public void commitAsync(final Map<TopicPartition, OffsetAndMetadata> offsets, final OffsetCommitCallback callback) {
        interceptor.execProc(this, COMMIT_ASYNC, () -> proxiedObject.commitAsync(offsets, callback), new String[]{OFFSETS, "callback"}, offsets, callback);
    }

    @Override
    public void seek(final TopicPartition partition, final long offset) {
        interceptor.execProc(this, "seek", () -> proxiedObject.seek(partition, offset), new String[]{PARTITION, "offset"}, partition, offset);
    }

    @Override
    public void seek(TopicPartition partition, OffsetAndMetadata offsetAndMetadata) {
        interceptor.execProc(this, "seek", () -> proxiedObject.seek(partition, offsetAndMetadata), new String[]{PARTITION, "offsetAndMetadata"}, partition, offsetAndMetadata);
    }

    @Override
    public void seekToBeginning(final Collection<TopicPartition> partitions) {
        interceptor.execProc(this, "seekToBeginning", () -> proxiedObject.seekToBeginning(partitions), new String[]{PARTITIONS}, partitions);
    }

    @Override
    public void seekToEnd(final Collection<TopicPartition> partitions) {
        interceptor.execProc(this, "seekToEnd", () -> proxiedObject.seekToEnd(partitions), new String[]{PARTITION}, partitions);
    }

    @Override
    public long position(final TopicPartition partition) {
        return interceptor.exec(this, "position", () -> proxiedObject.position(partition), new String[]{PARTITION}, partition);
    }

    @Override
    public long position(TopicPartition partition, Duration timeout) {
        return interceptor.exec(this, "position", () -> proxiedObject.position(partition, timeout), new String[]{PARTITION, TIMEOUT}, partition, timeout);
    }

    /**
     * @deprecated
     */
    @Deprecated
    @Override
    public OffsetAndMetadata committed(final TopicPartition partition) {
        return interceptor.exec(this, COMMITTED, () -> proxiedObject.committed(partition), new String[]{PARTITION}, partition);
    }

    /**
     * @deprecated
     */
    @Deprecated
    @Override
    public OffsetAndMetadata committed(TopicPartition partition, Duration timeout) {
        return interceptor.exec(this, COMMITTED, () -> proxiedObject.committed(partition, timeout), new String[]{PARTITION, TIMEOUT}, partition, timeout);
    }

    @Override
    public Map<TopicPartition, OffsetAndMetadata> committed(Set<TopicPartition> partitions) {
        return interceptor.exec(this, COMMITTED, () -> proxiedObject.committed(partitions), new String[]{PARTITIONS, TIMEOUT}, partitions);
    }

    @Override
    public Map<TopicPartition, OffsetAndMetadata> committed(Set<TopicPartition> partitions,
        Duration timeout) {
        return interceptor.exec(this, COMMITTED, () -> proxiedObject.committed(partitions, timeout), new String[]{PARTITIONS, TIMEOUT}, partitions, timeout);
    }

    @Override
    public Map<MetricName, ? extends Metric> metrics() {
        return interceptor.exec(this, "metrics", proxiedObject::metrics);
    }

    @Override
    public List<PartitionInfo> partitionsFor(final String topic) {
        return interceptor.exec(this, "partitionsFor", () -> proxiedObject.partitionsFor(topic), new String[]{"topic"}, topic);
    }

    @Override
    public List<PartitionInfo> partitionsFor(String topic, Duration timeout) {
        return interceptor.exec(this, "partitionsFor", () -> proxiedObject.partitionsFor(topic, timeout), new String[]{"topic", TIMEOUT}, topic, timeout);
    }

    @Override
    public Map<String, List<PartitionInfo>> listTopics() {
        return interceptor.exec(this, "listTopics", proxiedObject::listTopics);
    }

    @Override
    public Map<String, List<PartitionInfo>> listTopics(Duration timeout) {
        return interceptor.exec(this, "listTopics", () -> proxiedObject.listTopics(timeout), new String[]{TIMEOUT}, timeout);
    }

    @Override
    public Set<TopicPartition> paused() {
        return interceptor.exec(this, "paused", proxiedObject::paused);
    }

    @Override
    public void pause(final Collection<TopicPartition> topics) {
        interceptor.execProc(this, "pause", () -> proxiedObject.pause(topics), new String[]{TOPICS}, topics);
    }

    @Override
    public void resume(final Collection<TopicPartition> topics) {
        interceptor.execProc(this, "resume", () -> proxiedObject.resume(topics), new String[]{TOPICS}, topics);
    }

    @Override
    public Map<TopicPartition, OffsetAndTimestamp> offsetsForTimes(final Map<TopicPartition, Long> timestamps) {
        return interceptor.exec(this, "offsetsForTimes", () -> proxiedObject.offsetsForTimes(timestamps), new String[]{"timestamps"}, timestamps);
    }

    @Override
    public Map<TopicPartition, OffsetAndTimestamp> offsetsForTimes(Map<TopicPartition, Long> timestamps, Duration timeout) {
        return interceptor.exec(this, "offsetsForTimes", () -> proxiedObject.offsetsForTimes(timestamps, timeout), new String[]{"timestamps", TIMEOUT}, timestamps, timeout);
    }

    @Override
    public Map<TopicPartition, Long> beginningOffsets(final Collection<TopicPartition> partitions) {
        return interceptor.exec(this, "beginningOffsets", () -> proxiedObject.beginningOffsets(partitions), new String[]{PARTITIONS}, partitions);
    }

    @Override
    public Map<TopicPartition, Long> beginningOffsets(Collection<TopicPartition> partitions, Duration timeout) {
        return interceptor.exec(this, "beginningOffsets", () -> proxiedObject.beginningOffsets(partitions, timeout), new String[]{PARTITIONS, TIMEOUT}, partitions, timeout);
    }

    @Override
    public Map<TopicPartition, Long> endOffsets(final Collection<TopicPartition> partitions) {
        return interceptor.exec(this, "endOffsets", () -> proxiedObject.endOffsets(partitions), new String[]{PARTITIONS}, partitions);
    }

    @Override
    public Map<TopicPartition, Long> endOffsets(Collection<TopicPartition> partitions, Duration timeout) {
        return interceptor.exec(this, "endOffsets", () -> proxiedObject.endOffsets(partitions, timeout), new String[]{PARTITIONS, TIMEOUT}, partitions, timeout);
    }

    @Override
    public OptionalLong currentLag(TopicPartition topicPartition) {
        return interceptor.exec(this, "currentLag", () -> proxiedObject.currentLag(topicPartition), new String[]{"topicPartition"}, topicPartition);
    }

    @Override
    public ConsumerGroupMetadata groupMetadata() {
        return interceptor.exec(this, "groupMetadata", proxiedObject::groupMetadata);
    }

    @Override
    public void enforceRebalance() {
        interceptor.execProc(this, "enforceRebalance", proxiedObject::enforceRebalance);
    }

    @Override
    public void enforceRebalance(String reason) {
        interceptor.execProc(this, "enforceRebalance", () -> proxiedObject.enforceRebalance(reason), new String[]{"reason"}, reason);
    }

    @Override
    public void close(Duration timeout) {
        interceptor.execProc(this, "close", () -> super.close(timeout), new String[]{TIMEOUT}, timeout);
    }

    @Override
    public void wakeup() {
        interceptor.execProc(this, "wakeup", proxiedObject::wakeup);
    }
}
