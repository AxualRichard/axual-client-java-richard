package io.axual.client.proxy.generic.registry;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * The type Proxy chain. This class resembles the chain of proxies to set up for an AxualConsumer,
 * AxualProducer or AxualAdminClient. It has a String equivalent which adheres to the syntax:
 * <p>
 * PROXYNAME1:key1=value1,key2=value2;PROXYNAME2;PROXYNAME3:key3=value3
 * <p>
 * Conversion to and from Strings is supported by parse() and toString()
 */
public class ProxyChain {
    private static final String PROXY_SEPARATOR = ";";
    private static final String PROXY_ID_CONFIGS_SEPARATOR = ":";
    private static final String PROXY_CONFIG_SEPARATOR = ",";
    private static final String PROXY_CONFIG_KEY_VALUE_SEPARATOR = "=";

    private final List<ProxyChainElement> elements;

    private ProxyChain(Builder builder) {
        this(builder.elements);
    }

    public ProxyChain(List<ProxyChainElement> elements) {
        this.elements = Collections.unmodifiableList(elements);
    }

    public boolean containsElement(String proxyType) {
        for (ProxyChainElement element : elements) {
            if (element.getProxyId().equals(proxyType)) {
                return true;
            }
        }
        return false;
    }

    public List<ProxyChainElement> getElements() {
        return elements;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        for (ProxyChainElement element : elements) {
            if (builder.length() > 0) {
                builder.append(PROXY_SEPARATOR);
            }
            builder.append(element.getProxyId());
            if (element.getConfigs() != null && element.getConfigs().size() > 0) {
                builder.append(PROXY_ID_CONFIGS_SEPARATOR);
                boolean firstConfig = true;
                for (Map.Entry<String, Object> entry : element.getConfigs().entrySet()) {
                    if (!firstConfig) {
                        builder.append(PROXY_CONFIG_SEPARATOR);
                    } else {
                        firstConfig = false;
                    }
                    builder.append(entry.getKey());
                    builder.append(PROXY_CONFIG_KEY_VALUE_SEPARATOR);
                    builder.append(entry.getValue());
                }
            }
        }

        return builder.toString();
    }

    public static ProxyChain parse(String chainStr) {
        final List<ProxyChainElement> elements = new ArrayList<>();

        if (chainStr != null) {
            for (String elementStr : chainStr.split(PROXY_SEPARATOR)) {
                String[] proxyStr = elementStr.split(PROXY_ID_CONFIGS_SEPARATOR);
                if (proxyStr.length >= 1) {
                    Map<String, Object> configs = proxyStr.length >= 2 ? parseChainElementConfigs(proxyStr[1]) : new HashMap<>();
                    elements.add(ProxyChainElement.newBuilder()
                            .setProxyId(proxyStr[0])
                            .setConfigs(configs)
                            .build());
                }
            }
        }

        return new ProxyChain(elements);
    }

    private static Map<String, Object> parseChainElementConfigs(String configs) {
        Map<String, Object> result = new HashMap<>();
        for (String config : configs.split(PROXY_CONFIG_SEPARATOR)) {
            String key = config.substring(0, config.indexOf(PROXY_CONFIG_KEY_VALUE_SEPARATOR));
            String value = config.substring(key.length() + 1);
            result.put(key, value);
        }
        return result;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static Builder newBuilder(ProxyChain fromChain) {
        return new Builder(fromChain);
    }

    public interface Filter {
        boolean filter(ProxyChainElement element);
    }

    public static class Builder {
        private final List<ProxyChainElement> elements = new ArrayList<>();

        public Builder() {
        }

        public Builder(ProxyChain fromChain) {
            elements.addAll(fromChain.elements);
        }

        public Builder append(ProxyChainElement element) {
            elements.add(element);
            return this;
        }

        public Builder append(String proxyType) {
            elements.add(ProxyChainElement.newBuilder()
                    .setProxyId(proxyType)
                    .setConfigs(new HashMap<>())
                    .build());
            return this;
        }

        public Builder append(String proxyType, Map<String, Object> configs) {
            elements.add(ProxyChainElement.newBuilder()
                    .setProxyId(proxyType)
                    .setConfigs(configs)
                    .build());
            return this;
        }

        public Builder filter(Filter filter) {
            List<ProxyChainElement> newElements = new ArrayList<>(elements.size());
            Iterator<ProxyChainElement> i = elements.iterator();
            while (i.hasNext()) {
                ProxyChainElement element = i.next();
                if (filter.filter(element)) {
                    newElements.add(element);
                }
            }
            elements.clear();
            elements.addAll(newElements);
            return this;
        }

        public Builder remove(String proxyId) {
            return filter(element -> !element.getProxyId().equals(proxyId));
        }

        public ProxyChain build() {
            return new ProxyChain(this);
        }
    }
}
