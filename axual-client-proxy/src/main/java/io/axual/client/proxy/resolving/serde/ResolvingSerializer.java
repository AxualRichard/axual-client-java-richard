package io.axual.client.proxy.resolving.serde;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.header.Headers;

import java.util.Map;

import io.axual.client.proxy.generic.serde.BaseSerializerProxy;

public class ResolvingSerializer<T> extends BaseSerializerProxy<T, ResolvingSerializerConfig<T>> {
    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        // Instantiate topic resolver and backing serializer
        configure(new ResolvingSerializerConfig<>(configs, isKey));
    }

    @Override
    public byte[] serialize(String topic, Headers headers, T object) {
        // Resolve the topic and encode the object using the backing serializer
        return backingSerializer.serialize(config.getTopicResolver().resolveTopic(topic), headers, object);
    }
}
