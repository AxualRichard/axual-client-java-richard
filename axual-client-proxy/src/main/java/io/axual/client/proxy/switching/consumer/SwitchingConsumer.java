package io.axual.client.proxy.switching.consumer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.consumer.ConsumerGroupMetadata;
import org.apache.kafka.clients.consumer.ConsumerRebalanceListener;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.clients.consumer.OffsetAndTimestamp;
import org.apache.kafka.clients.consumer.OffsetCommitCallback;
import org.apache.kafka.common.Metric;
import org.apache.kafka.common.MetricName;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.Deserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.OptionalLong;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Pattern;

import io.axual.client.proxy.generic.consumer.ConsumerProxy;
import io.axual.client.proxy.generic.tools.SerdeUtil;
import io.axual.client.proxy.switching.discovery.DiscoverySubscriber;
import io.axual.client.proxy.switching.generic.SwitchingProxy;
import io.axual.common.concurrent.LockedObject;
import io.axual.common.tools.MapUtil;
import io.axual.discovery.client.tools.DiscoveryConfigParserV2;

public class SwitchingConsumer<K, V> extends SwitchingProxy<ConsumerProxy<K, V>, SwitchingConsumerConfig<K, V>> implements ConsumerProxy<K, V> {
    private static final Logger LOG = LoggerFactory.getLogger(SwitchingConsumer.class);

    private Set<TopicPartition> pausedPartitions = new HashSet<>();

    public SwitchingConsumer(Map<String, Object> configs) {
        super(new SwitchingConsumerConfig<>(configs),
                new DiscoverySubscriber<>(
                        new DiscoveryConfigParserV2().parse(configs),
                        SwitchingConsumer.class.getSimpleName(),
                        new ConsumerSwitcher<>(),
                        false));
    }

    public SwitchingConsumer(Map<String, Object> configs, Deserializer<K> keyDeserializer, Deserializer<V> valueDeserializer) {
        this(SerdeUtil.addDeserializersToConfigs(configs, keyDeserializer, valueDeserializer));
    }

    public SwitchingConsumer(Properties properties) {
        this(MapUtil.objectToStringMap(properties));
    }

    public SwitchingConsumer(Properties properties, Deserializer<K> keyDeserializer, Deserializer<V> valueDeserializer) {
        this(MapUtil.objectToStringMap(properties), keyDeserializer, valueDeserializer);
    }

    @Override
    public Set<TopicPartition> assignment() {
        try (LockedObject<ConsumerProxy<K, V>>.ReadLock lock = getReadLock()) {
            return lock.object.assignment();
        }
    }

    @Override
    public Set<String> subscription() {
        final Subscription<K, V> subscription = getConsumerSwitcher().getSubscription();
        if (subscription instanceof TopicSetSubscription) {
            Collection<String> topics = ((TopicSetSubscription<K, V>) subscription).getTopics();
            Set<String> result = new HashSet<>(topics.size());
            result.addAll(topics);
            return result;
        }

        return Collections.emptySet();
    }

    @Override
    public void subscribe(Collection<String> topics, ConsumerRebalanceListener listener) {
        if (getConsumerSwitcher().getAssignment() != null) {
            operationNotSupported("topic set subscription when manual partition assignment is active");
        }

        if (topics == null || topics.isEmpty()) {
            unsubscribe();
            return;
        }

        try (LockedObject<ConsumerProxy<K, V>>.ReadLock lock = getReadLock()) {
            LOG.info("Subscribing to topics: {}", topics);
            getConsumerSwitcher().setSubscription(lock.object, config, new TopicSetSubscription<>(topics, listener));
        }
    }

    @Override
    public void subscribe(Collection<String> topics) {
        subscribe(topics, null);
    }

    @Override
    public void subscribe(Pattern pattern, ConsumerRebalanceListener listener) {
        if (getConsumerSwitcher().getAssignment() != null) {
            operationNotSupported("pattern subscription when manual partition assignment is active");
        }

        if (pattern == null) {
            unsubscribe();
            return;
        }

        try (LockedObject<ConsumerProxy<K, V>>.ReadLock lock = getReadLock()) {
            LOG.info("Subscribing to pattern: {}", pattern);
            getConsumerSwitcher().setSubscription(lock.object, config, new TopicPatternSubscription<>(pattern, listener));
        }
    }

    @Override
    public void subscribe(Pattern pattern) {
        subscribe(pattern, null);
    }

    @Override
    public void unsubscribe() {
        try (LockedObject<ConsumerProxy<K, V>>.ReadLock lock = getReadLock()) {
            getConsumerSwitcher().unsubscribe(lock.object, config);
        }
    }

    @Override
    public void assign(Collection<TopicPartition> partitions) {
        if (getConsumerSwitcher().getSubscription() != null) {
            operationNotSupported("manual partition assignment when a subscription is active");
        }

        try (LockedObject<ConsumerProxy<K, V>>.ReadLock lock = getReadLock()) {
            getConsumerSwitcher().setAssignment(lock.object, config, new TopicPartitionSetAssignment<>(partitions));
        }
    }

    /**
     * @deprecated
     */
    @Deprecated
    @Override
    public ConsumerRecords<K, V> poll(long timeout) {
        // Apart from subscribe, this is the only logical place where we allow for a cluster switch
        maybeReplaceProxiedObject(false);

        try (LockedObject<ConsumerProxy<K, V>>.ReadLock lock = getReadLock()) {
            return lock.object.poll(timeout);
        }
    }

    @Override
    public ConsumerRecords<K, V> poll(Duration timeout) {
        // Apart from subscribe, this is the only logical place where we allow for a cluster switch
        maybeReplaceProxiedObject(false);

        try (LockedObject<ConsumerProxy<K, V>>.ReadLock lock = getReadLock()) {
            return lock.object.poll(timeout);
        }
    }

    @Override
    public void commitSync() {
        try (LockedObject<ConsumerProxy<K, V>>.ReadLock lock = getReadLock()) {
            lock.object.commitSync();
        }
    }

    @Override
    public void commitSync(Duration timeout) {
        try (LockedObject<ConsumerProxy<K, V>>.ReadLock lock = getReadLock()) {
            lock.object.commitSync(timeout);
        }
    }

    @Override
    public void commitSync(Map<TopicPartition, OffsetAndMetadata> offsets) {
        try (LockedObject<ConsumerProxy<K, V>>.ReadLock lock = getReadLock()) {
            lock.object.commitSync(offsets);
        }
    }

    @Override
    public void commitSync(Map<TopicPartition, OffsetAndMetadata> offsets, Duration timeout) {
        try (LockedObject<ConsumerProxy<K, V>>.ReadLock lock = getReadLock()) {
            lock.object.commitSync(offsets, timeout);
        }
    }

    @Override
    public void commitAsync() {
        try (LockedObject<ConsumerProxy<K, V>>.ReadLock lock = getReadLock()) {
            lock.object.commitAsync();
        }
    }

    @Override
    public void commitAsync(final OffsetCommitCallback callback) {
        try (LockedObject<ConsumerProxy<K, V>>.ReadLock lock = getReadLock()) {
            lock.object.commitAsync(callback);
        }
    }

    @Override
    public void commitAsync(Map<TopicPartition, OffsetAndMetadata> offsets, OffsetCommitCallback callback) {
        try (LockedObject<ConsumerProxy<K, V>>.ReadLock lock = getReadLock()) {
            lock.object.commitAsync(offsets, callback);
        }
    }

    @Override
    public void seek(TopicPartition partition, long offset) {
        try (LockedObject<ConsumerProxy<K, V>>.ReadLock lock = getReadLock()) {
            lock.object.seek(partition, offset);
        }
    }

    @Override
    public void seek(TopicPartition topicPartition, OffsetAndMetadata offsetAndMetadata) {
        try (LockedObject<ConsumerProxy<K, V>>.ReadLock lock = getReadLock()) {
            lock.object.seek(topicPartition, offsetAndMetadata);
        }
    }

    @Override
    public void seekToBeginning(Collection<TopicPartition> partitions) {
        try (LockedObject<ConsumerProxy<K, V>>.ReadLock lock = getReadLock()) {
            lock.object.seekToBeginning(partitions);
        }
    }

    @Override
    public void seekToEnd(Collection<TopicPartition> partitions) {
        try (LockedObject<ConsumerProxy<K, V>>.ReadLock lock = getReadLock()) {
            lock.object.seekToEnd(partitions);
        }
    }

    @Override
    public long position(TopicPartition partition) {
        try (LockedObject<ConsumerProxy<K, V>>.ReadLock lock = getReadLock()) {
            return lock.object.position(partition);
        }
    }

    @Override
    public long position(TopicPartition partition, Duration timeout) {
        try (LockedObject<ConsumerProxy<K, V>>.ReadLock lock = getReadLock()) {
            return lock.object.position(partition, timeout);
        }
    }

    /**
     * @deprecated
     */
    @Deprecated
    @Override
    public OffsetAndMetadata committed(TopicPartition partition) {
        try (LockedObject<ConsumerProxy<K, V>>.ReadLock lock = getReadLock()) {
            return lock.object.committed(partition);
        }
    }

    /**
     * @deprecated
     */
    @Deprecated
    @Override
    public OffsetAndMetadata committed(TopicPartition partition, Duration timeout) {
        try (LockedObject<ConsumerProxy<K, V>>.ReadLock lock = getReadLock()) {
            return lock.object.committed(partition, timeout);
        }
    }

    @Override
    public Map<TopicPartition, OffsetAndMetadata> committed(Set<TopicPartition> partitions) {
        try (LockedObject<ConsumerProxy<K, V>>.ReadLock lock = getReadLock()) {
            return lock.object.committed(partitions);
        }
    }

    @Override
    public Map<TopicPartition, OffsetAndMetadata> committed(Set<TopicPartition> partitions,
        Duration timeout) {
        try (LockedObject<ConsumerProxy<K, V>>.ReadLock lock = getReadLock()) {
            return lock.object.committed(partitions,timeout);
        }
    }

    @Override
    public Map<MetricName, ? extends Metric> metrics() {
        try (LockedObject<ConsumerProxy<K, V>>.ReadLock lock = getReadLock()) {
            return lock.object.metrics();
        }
    }

    @Override
    public List<PartitionInfo> partitionsFor(String topic) {
        try (LockedObject<ConsumerProxy<K, V>>.ReadLock lock = getReadLock()) {
            return lock.object.partitionsFor(topic);
        }
    }

    @Override
    public List<PartitionInfo> partitionsFor(String topic, Duration timeout) {
        try (LockedObject<ConsumerProxy<K, V>>.ReadLock lock = getReadLock()) {
            return lock.object.partitionsFor(topic, timeout);
        }
    }

    @Override
    public Map<String, List<PartitionInfo>> listTopics() {
        try (LockedObject<ConsumerProxy<K, V>>.ReadLock lock = getReadLock()) {
            return lock.object.listTopics();
        }
    }

    @Override
    public Map<String, List<PartitionInfo>> listTopics(Duration timeout) {
        try (LockedObject<ConsumerProxy<K, V>>.ReadLock lock = getReadLock()) {
            return lock.object.listTopics(timeout);
        }
    }

    @Override
    public void pause(Collection<TopicPartition> partitions) {
        try (LockedObject<ConsumerProxy<K, V>>.ReadLock lock = getReadLock()) {
            pausedPartitions.addAll(partitions);
            lock.object.pause(partitions);
        }
    }

    @Override
    public void resume(Collection<TopicPartition> partitions) {
        try (LockedObject<ConsumerProxy<K, V>>.ReadLock lock = getReadLock()) {
            lock.object.resume(partitions);
            pausedPartitions.removeAll(partitions);
        }
    }

    @Override
    public Set<TopicPartition> paused() {
        return new HashSet<>(pausedPartitions);
    }

    @Override
    public Map<TopicPartition, OffsetAndTimestamp> offsetsForTimes(Map<TopicPartition, Long> timestampsToSearch) {
        try (LockedObject<ConsumerProxy<K, V>>.ReadLock lock = getReadLock()) {
            return lock.object.offsetsForTimes(timestampsToSearch);
        }
    }

    @Override
    public Map<TopicPartition, OffsetAndTimestamp> offsetsForTimes(Map<TopicPartition, Long> timestampsToSearch, Duration timeout) {
        try (LockedObject<ConsumerProxy<K, V>>.ReadLock lock = getReadLock()) {
            return lock.object.offsetsForTimes(timestampsToSearch, timeout);
        }
    }

    @Override
    public Map<TopicPartition, Long> beginningOffsets(Collection<TopicPartition> partitions) {
        try (LockedObject<ConsumerProxy<K, V>>.ReadLock lock = getReadLock()) {
            return lock.object.beginningOffsets(partitions);
        }
    }

    @Override
    public Map<TopicPartition, Long> beginningOffsets(Collection<TopicPartition> partitions, Duration timeout) {
        try (LockedObject<ConsumerProxy<K, V>>.ReadLock lock = getReadLock()) {
            return lock.object.beginningOffsets(partitions, timeout);
        }
    }

    @Override
    public Map<TopicPartition, Long> endOffsets(Collection<TopicPartition> partitions) {
        try (LockedObject<ConsumerProxy<K, V>>.ReadLock lock = getReadLock()) {
            return lock.object.endOffsets(partitions);
        }
    }

    @Override
    public Map<TopicPartition, Long> endOffsets(Collection<TopicPartition> partitions, Duration timeout) {
        try (LockedObject<ConsumerProxy<K, V>>.ReadLock lock = getReadLock()) {
            return lock.object.endOffsets(partitions, timeout);
        }
    }

    @Override
    public OptionalLong currentLag(TopicPartition topicPartition) {
        try (LockedObject<ConsumerProxy<K, V>>.ReadLock lock = getReadLock()) {
            return lock.object.currentLag(topicPartition);
        }
    }

    @Override
    public ConsumerGroupMetadata groupMetadata() {
        try (LockedObject<ConsumerProxy<K, V>>.ReadLock lock = getReadLock()) {
            return lock.object.groupMetadata();
        }
    }

    @Override
    public void enforceRebalance() {
        try (LockedObject<ConsumerProxy<K, V>>.ReadLock lock = getReadLock()) {
            lock.object.enforceRebalance();
        }
    }

    @Override
    public void enforceRebalance(String reason) {
        try (LockedObject<ConsumerProxy<K, V>>.ReadLock lock = getReadLock()) {
            lock.object.enforceRebalance(reason);
        }
    }

    @Override
    public void wakeup() {
        try (LockedObject<ConsumerProxy<K, V>>.ReadLock lock = getReadLock()) {
            lock.object.wakeup();
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // End of public interface of KafkaConsumer
    ///////////////////////////////////////////////////////////////////////////////////////////////

    private ConsumerSwitcher<K, V> getConsumerSwitcher() {
        return (ConsumerSwitcher<K, V>) getSwitcher();
    }
}
