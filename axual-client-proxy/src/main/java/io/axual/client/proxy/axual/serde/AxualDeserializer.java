package io.axual.client.proxy.axual.serde;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.header.Headers;

import java.util.HashMap;
import java.util.Map;

import io.axual.client.proxy.generic.registry.ProxyChainUtil;
import io.axual.client.proxy.generic.serde.BaseDeserializerProxy;
import io.axual.client.proxy.generic.serde.DeserializerProxyFactory;
import io.axual.client.proxy.wrapped.serde.WrappedDeserializerConfig;

public class AxualDeserializer<T> extends BaseDeserializerProxy<T, AxualDeserializerConfig<T>> {
    @Override
    @SuppressWarnings("unchecked")
    public void configure(Map<String, ?> configs, boolean isKey) {
        configure(new AxualDeserializerConfig<>(new HashMap<>(configs), isKey));

        Map<String, Object> deserializerConfigs = new HashMap<>(configs);
        Object defaultDeserializer = configs.get(AxualDeserializerConfig.DEFAULT_DESERIALIZER_CONFIG);
        deserializerConfigs.put(WrappedDeserializerConfig.DESERIALIZER_CONFIG, defaultDeserializer);
        DeserializerProxyFactory<T> factory = ProxyChainUtil.setupDeserializerFactoryChain(config.getProxyChain());
        backingDeserializer = factory.create(deserializerConfigs, isKey);
    }

    @Override
    public T deserialize(String topic, Headers headers, byte[] data) {
        return backingDeserializer.deserialize(topic, headers, data);
    }
}
