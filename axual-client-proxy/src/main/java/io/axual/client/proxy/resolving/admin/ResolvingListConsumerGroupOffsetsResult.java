package io.axual.client.proxy.resolving.admin;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import io.axual.common.resolver.TopicResolver;
import java.util.Map;
import org.apache.kafka.clients.admin.ExtendableListConsumerGroupOffsetsResult;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.KafkaFuture;
import org.apache.kafka.common.TopicPartition;

public class ResolvingListConsumerGroupOffsetsResult extends
        ExtendableListConsumerGroupOffsetsResult {

  protected final KafkaFuture<Map<TopicPartition, OffsetAndMetadata>> kafkaFuture;

  public ResolvingListConsumerGroupOffsetsResult(
          KafkaFuture<Map<TopicPartition, OffsetAndMetadata>> future,
          TopicResolver topicResolver) {
    super(future);
    this.kafkaFuture = future.thenApply(topicResolver::unresolveTopics);
  }

  @Override
  public KafkaFuture<Map<TopicPartition, OffsetAndMetadata>> partitionsToOffsetAndMetadata() {
    return kafkaFuture;
  }
}
