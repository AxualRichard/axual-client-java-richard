package io.axual.client.proxy.generic.serde;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.time.Duration;

import io.axual.client.proxy.generic.proxy.BaseProxy;

public abstract class BaseSerializerProxy<T, C extends BaseSerializerProxyConfig<T>> extends BaseProxy<C> implements SerializerProxy<T> {
    protected SerializerProxy<T> backingSerializer;

    public BaseSerializerProxy() {
        super(null);
    }

    protected void configure(C config) {
        this.config = config;
        backingSerializer=config.getBackingSerializer();
    }

    @Override
    public void close(Duration timeout) {
        if (backingSerializer != null) {
            backingSerializer.close(timeout);
        }
        super.close(timeout);
    }

    @Override
    public byte[] serialize(String topic, T object) {
        // Ensure we always call the serialize() that uses headers
        return serialize(topic, null, object);
    }
}
