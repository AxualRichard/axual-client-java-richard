package io.axual.client.proxy.wrapped.consumer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerGroupMetadata;
import org.apache.kafka.clients.consumer.ConsumerRebalanceListener;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.clients.consumer.OffsetAndTimestamp;
import org.apache.kafka.clients.consumer.OffsetCommitCallback;
import org.apache.kafka.common.Metric;
import org.apache.kafka.common.MetricName;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.Deserializer;

import java.time.Duration;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.OptionalLong;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Pattern;

import io.axual.client.proxy.generic.consumer.ConsumerProxy;
import io.axual.client.proxy.generic.tools.SerdeUtil;
import io.axual.client.proxy.wrapped.OffsetAndMetadataUtil;
import io.axual.client.proxy.wrapped.generic.WrappedClientProxy;
import io.axual.client.proxy.wrapped.generic.WrappedClientProxyConfig;
import io.axual.common.tools.MapUtil;

public class WrappedConsumer<K, V> extends WrappedClientProxy<ConsumerProxy<K, V>, WrappedClientProxyConfig<ConsumerProxy<K, V>>> implements ConsumerProxy<K, V> {
    private final Consumer<K, V> consumer;

    public WrappedConsumer(Consumer<K, V> consumer, Map<String, Object> configs) {
        super(new WrappedClientProxyConfig<>(configs));
        this.consumer = consumer;
    }

    public WrappedConsumer(Map<String, Object> configs) {
        this(new KafkaConsumer<>(configs), configs);
    }

    public WrappedConsumer(Map<String, Object> configs, Deserializer<K> keyDeserializer, Deserializer<V> valueDeserializer) {
        this(SerdeUtil.addDeserializersToConfigs(configs, keyDeserializer, valueDeserializer));
    }

    public WrappedConsumer(Properties properties) {
        this(MapUtil.objectToStringMap(properties));
    }

    public WrappedConsumer(Properties properties, Deserializer<K> keyDeserializer, Deserializer<V> valueDeserializer) {
        this(MapUtil.objectToStringMap(properties), keyDeserializer, valueDeserializer);
    }

    @Override
    public Set<TopicPartition> assignment() {
        return consumer.assignment();
    }

    @Override
    public Set<String> subscription() {
        return consumer.subscription();
    }

    @Override
    public void subscribe(Collection<String> collection) {
        consumer.subscribe(collection);
    }

    @Override
    public void subscribe(Collection<String> collection, ConsumerRebalanceListener consumerRebalanceListener) {
        consumer.subscribe(collection, consumerRebalanceListener);
    }

    @Override
    public void assign(Collection<TopicPartition> collection) {
        consumer.assign(collection);
    }

    @Override
    public void subscribe(Pattern pattern, ConsumerRebalanceListener consumerRebalanceListener) {
        consumer.subscribe(pattern, consumerRebalanceListener);
    }

    @Override
    public void subscribe(Pattern pattern) {
        consumer.subscribe(pattern);
    }

    @Override
    public void unsubscribe() {
        consumer.unsubscribe();
    }

    /**
     * @deprecated
     */
    @Deprecated
    @Override
    public ConsumerRecords<K, V> poll(long timeout) {
        return consumer.poll(timeout);
    }

    @Override
    public ConsumerRecords<K, V> poll(Duration duration) {
        return consumer.poll(duration);
    }

    @Override
    public void commitSync() {
        consumer.commitSync();
    }

    @Override
    public void commitSync(Duration duration) {
        consumer.commitSync(duration);
    }

    @Override
    public void commitSync(Map<TopicPartition, OffsetAndMetadata> offsets) {
        consumer.commitSync(offsets);
    }

    @Override
    public void commitSync(Map<TopicPartition, OffsetAndMetadata> offsets, Duration duration) {
        consumer.commitSync(offsets, duration);
    }

    @Override
    public void commitAsync() {
        consumer.commitAsync();
    }

    @Override
    public void commitAsync(OffsetCommitCallback offsetCommitCallback) {
        consumer.commitAsync(offsetCommitCallback);
    }

    @Override
    public void commitAsync(Map<TopicPartition, OffsetAndMetadata> map, OffsetCommitCallback offsetCommitCallback) {
        consumer.commitAsync(map, offsetCommitCallback);
    }

    @Override
    public void seek(TopicPartition topicPartition, long offset) {
        consumer.seek(topicPartition, offset);
    }

    @Override
    public void seek(TopicPartition topicPartition, OffsetAndMetadata offsetAndMetadata) {
        consumer.seek(topicPartition, offsetAndMetadata);
    }

    @Override
    public void seekToBeginning(Collection<TopicPartition> collection) {
        consumer.seekToBeginning(collection);
    }

    @Override
    public void seekToEnd(Collection<TopicPartition> partitions) {
        consumer.seekToEnd(partitions);
    }

    @Override
    public long position(TopicPartition partition) {
        return consumer.position(partition);
    }

    @Override
    public long position(TopicPartition partition, Duration duration) {
        return consumer.position(partition, duration);
    }

    /**
     * @deprecated
     */
    @Deprecated
    @Override
    public OffsetAndMetadata committed(TopicPartition partition) {
        return OffsetAndMetadataUtil.INSTANCE.cleanMetadata(consumer.committed(partition));
    }

    /**
     * @deprecated
     */
    @Deprecated
    @Override
    public OffsetAndMetadata committed(TopicPartition partition, Duration duration) {
        return OffsetAndMetadataUtil.INSTANCE.cleanMetadata(consumer.committed(partition, duration));
    }

    @Override
    public Map<TopicPartition, OffsetAndMetadata> committed(Set<TopicPartition> partitions) {
        return OffsetAndMetadataUtil.INSTANCE.cleanMetadata(consumer.committed(partitions));
    }

    @Override
    public Map<TopicPartition, OffsetAndMetadata> committed(Set<TopicPartition> partitions,
                                                            Duration timeout) {
        return OffsetAndMetadataUtil.INSTANCE.cleanMetadata(consumer.committed(partitions, timeout));
    }

    @Override
    public Map<MetricName, ? extends Metric> metrics() {
        return consumer.metrics();
    }

    @Override
    public List<PartitionInfo> partitionsFor(String topic) {
        return consumer.partitionsFor(topic);
    }

    @Override
    public List<PartitionInfo> partitionsFor(String topic, Duration duration) {
        return consumer.partitionsFor(topic, duration);
    }

    @Override
    public Map<String, List<PartitionInfo>> listTopics() {
        return consumer.listTopics();
    }

    @Override
    public Map<String, List<PartitionInfo>> listTopics(Duration duration) {
        return consumer.listTopics(duration);
    }

    @Override
    public Set<TopicPartition> paused() {
        return consumer.paused();
    }

    @Override
    public void pause(Collection<TopicPartition> collection) {
        consumer.pause(collection);
    }

    @Override
    public void resume(Collection<TopicPartition> collection) {
        consumer.resume(collection);
    }

    @Override
    public Map<TopicPartition, OffsetAndTimestamp> offsetsForTimes(Map<TopicPartition, Long> timestamps) {
        return consumer.offsetsForTimes(timestamps);
    }

    @Override
    public Map<TopicPartition, OffsetAndTimestamp> offsetsForTimes(Map<TopicPartition, Long> timestamps, Duration duration) {
        return consumer.offsetsForTimes(timestamps, duration);
    }

    @Override
    public Map<TopicPartition, Long> beginningOffsets(Collection<TopicPartition> partitions) {
        return consumer.beginningOffsets(partitions);
    }

    @Override
    public Map<TopicPartition, Long> beginningOffsets(Collection<TopicPartition> partitions, Duration duration) {
        return consumer.beginningOffsets(partitions, duration);
    }

    @Override
    public Map<TopicPartition, Long> endOffsets(Collection<TopicPartition> partitions) {
        return consumer.endOffsets(partitions);
    }

    @Override
    public Map<TopicPartition, Long> endOffsets(Collection<TopicPartition> partitions, Duration duration) {
        return consumer.endOffsets(partitions, duration);
    }

    @Override
    public OptionalLong currentLag(TopicPartition topicPartition) {
        return consumer.currentLag(topicPartition);
    }

    @Override
    public ConsumerGroupMetadata groupMetadata() {
        return consumer.groupMetadata();
    }

    @Override
    public void enforceRebalance() {
        consumer.enforceRebalance();
    }

    @Override
    public void enforceRebalance(String reason) {
        consumer.enforceRebalance(reason);
    }

    @Override
    public void close(Duration timeout) {
        consumer.close(timeout);
    }

    @Override
    public void wakeup() {
        consumer.wakeup();
    }
}
