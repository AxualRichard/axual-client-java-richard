package io.axual.client.proxy.generic.client;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.axual.client.proxy.generic.config.DynamicClientProxyConfig;
import io.axual.client.proxy.generic.producer.ProducerProxy;
import io.axual.common.concurrent.LockedObject;

public abstract class DynamicClientProxy<T extends ClientProxy, C extends DynamicClientProxyConfig> extends BaseClientProxy<C> {
    private static final Logger LOG = LoggerFactory.getLogger(DynamicClientProxy.class);
    private final LockedObject<T> lockedProxy;

    // Handler that can replace backing proxies
    private final ClientProxyReplacer<T, C> replacer;

    public DynamicClientProxy(C config, ClientProxyReplacer<T, C> replacer) {
        super(config);
        this.replacer = replacer;
        lockedProxy = initializeProxiedObject();
    }

    @Override
    public Map<String, Object> getConfigs() {
        Map<String, Object> result = new HashMap<>();
        try (LockedObject<T>.ReadLock lock = getReadLock()) {
            if (lock.object != null) {
                Map<String, Object> configs = lock.object.getConfigs();
                result.putAll(configs);
            }
        }
        result.putAll(super.getConfigs());
        return result;
    }

    @Override
    public final Object getConfig(String key) {
        Object result = super.getConfig(key);
        if (result == null) {
            try (LockedObject<T>.ReadLock lock = getReadLock()) {
                if (lock.object != null) {
                    result = lock.object.getConfig(key);
                }
            }
        }
        return result;
    }

    /**
     * Close this proxy.
     *
     * @param timeout the timeout
     * @param unit    the unit
     * @deprecated
     */
    @Deprecated
    public final void close(long timeout, TimeUnit unit) {
        close(Duration.ofMillis(unit.toMillis(timeout)));
    }

    @Override
    public void close(Duration timeout) {
        replacer.close();
        try (LockedObject<T>.WriteLock lock = getWriteLock()) {
            if (lock.getObject() != null) {
                lock.getObject().close();
                lock.setObject(null);
            }
        }
        super.close(timeout);
    }

    private LockedObject<T> initializeProxiedObject() {
        try {
            return new LockedObject<>(replacer.replace(null, config));
        } catch (Exception e) {
            LOG.error("Exception occurred while initializing {}", config.getProxyType(), e);
            throw e;
        }
    }

    /**
     * Replace the proxied object, depending on if a change in Discovery result has been indicated, or
     * if a replace is forced (or both).
     * @param force <code>true</code> if the replace should be done regardless of discovery change or not.
     * @return <code>true</code> if a replace was done, else <code>false</code>.
     */
    protected synchronized boolean maybeReplaceProxiedObject(boolean force) {
        return this.maybeReplaceProxiedObject(force, false);
    }

    /**
     * Replace the proxied object, depending on if a change in Discovery result has been indicated, or
     * if a replace is forced (or both).
     * @param force <code>true</code> if the replace should be done regardless of discovery change or not.
     * @param isTransactional indicates if the proxied object has transactions active.
     * @return <code>true</code> if a replace was done, else <code>false</code>.
     */
    protected synchronized boolean maybeReplaceProxiedObject(boolean force, boolean isTransactional) {
        final boolean needToReplace = replacer.needToReplace();

        if (needToReplace) {
            LOG.info("Something changed, replacing backing {}", config.getProxyType());
        }
        if (force) {
            LOG.info("Forcefully replacing backing {}", config.getProxyType());
        }

        if (needToReplace || force) {
            replaceProxiedObject(isTransactional);
            return true;
        }

        return false;
    }

    /**
     * Replace the proxied object.
     * If the proxied object has transactions active, silently abort them on the old cluster before
     * doing the actual switch. This is to prevent zombie transactions on the old cluster.
     * @param isTransactional if transactions are activated (initTransactions() called on proxied producer).
     */
    private void replaceProxiedObject(boolean isTransactional) {
        try (LockedObject<T>.WriteLock lock = getWriteLock()) {
            if (isTransactional) {
                final T object = lock.getObject();
                if (ProducerProxy.class.isAssignableFrom(object.getClass())) {
                    silentlyAbortTransactions((ProducerProxy) object);
                }
            }
            lock.setObject(replacer.replace(lock.getObject(), config));
        } catch (Exception e) {
            LOG.error("Exception occurred while switching backing {}", config.getProxyType(), e);
        }
    }

    /**
     * Silently abort possible transactions on a given ProducerProxy.
     * @param producerProxy the proxy.
     */
    private void silentlyAbortTransactions(ProducerProxy producerProxy) {
        try {
            producerProxy.abortTransaction();
        } catch (IllegalStateException e) {
            // we can't really know if a transaction is active, just that initTransactions() was called.
            LOG.info("Tried to silently abort transaction, but probably none active: {}", e.getMessage());
        }
    }

    protected LockedObject<T>.ReadLock getReadLock() {
        return lockedProxy.getReadLock();
    }

    protected LockedObject<T>.WriteLock getWriteLock() {
        return lockedProxy.getWriteLock();
    }
}
