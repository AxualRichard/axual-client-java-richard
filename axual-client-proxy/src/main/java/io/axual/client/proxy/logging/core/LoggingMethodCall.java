package io.axual.client.proxy.logging.core;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.UUID;

import io.axual.client.proxy.callback.core.MethodCall;

public class LoggingMethodCall implements MethodCall {
    private final LevelLogger log;
    private final String methodName;

    public LoggingMethodCall(LoggingConfig config, LevelLogger logger, UUID callId, String method) {
        if (config.getMethods().isEmpty() || config.getMethods().contains(method)) {
            log = logger;
            methodName = method
                    + " ("
                    + (config.getName() != null && config.getName().length() > 0 ? config.getName() + "," : "")
                    + "id=" + callId.toString()
                    + ")";
        } else {
            log = null;
            methodName = "";
        }
    }

    @Override
    public void close() {
        // Nothing to do
    }

    @Override
    public void onEnter(String[] paramNames, Object... paramValues) {
        if (log != null) {
            final String paramOutput = getParameterOutput(paramNames, paramValues);
            log.log("ENTER " + methodName + (paramOutput.length() > 0 ? ": " : "") + paramOutput);
        }
    }

    @Override
    public void onException(Throwable t) {
        if (log != null) {
            log.log("EXCEPTION IN " + methodName, t);
        }
    }

    @Override
    public void onResult(Object result) {
        if (log != null) {
            String resultStr = result != null
                    ? "(" + result.getClass().getSimpleName() + ") " + result.toString()
                    : "null";
            log.log("RETURN VALUE OF " + methodName + ": " + resultStr);
        }
    }

    @Override
    public void onExit() {
        if (log != null) {
            log.log("EXIT " + methodName);
        }
    }

    private String getParameterOutput(Object[] paramNames, Object[] paramValues) {
        StringBuilder result = new StringBuilder();
        if (paramValues != null) {
            for (int index = 0; index < paramValues.length; index++) {
                if (index > 0) {
                    result.append(", ");
                }
                result.append(paramNames != null && paramNames.length > index ? paramNames[index] : "param" + index)
                        .append("=")
                        .append(paramValues[index] != null ? paramValues[index] : "null");
            }
        }
        return result.toString();
    }
}
