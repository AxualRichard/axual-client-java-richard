package io.axual.client.proxy.switching.producer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 - 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

/**
 * Exception signalling that a cluster switch occurred in the middle of a Kafka transaction.
 * This is an unchecked exception.
 */
public class TransactionSwitchedException extends RuntimeException {

    public TransactionSwitchedException() {
    }

    public TransactionSwitchedException(String message) {
        super(message);
    }

    public TransactionSwitchedException(String message, Throwable cause) {
        super(message, cause);
    }

    public TransactionSwitchedException(Throwable cause) {
        super(cause);
    }

    public TransactionSwitchedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
