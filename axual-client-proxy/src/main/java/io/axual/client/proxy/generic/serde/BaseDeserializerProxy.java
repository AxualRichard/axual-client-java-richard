package io.axual.client.proxy.generic.serde;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.time.Duration;

import io.axual.client.proxy.generic.proxy.BaseProxy;

public abstract class BaseDeserializerProxy<T, C extends BaseDeserializerProxyConfig<T>> extends BaseProxy<C> implements DeserializerProxy<T> {
    protected DeserializerProxy<T> backingDeserializer;

    public BaseDeserializerProxy() {
        super(null);
    }

    protected void configure(C config) {
        this.config = config;
        backingDeserializer = config.getBackingDeserializer();
    }

    @Override
    public void close(Duration timeout) {
        backingDeserializer.close();
        super.close(timeout);
    }

    @Override
    public T deserialize(String topic, byte[] data) {
        // Ensure we always call the deserialize() that uses headers
        return deserialize(topic, null, data);
    }
}
