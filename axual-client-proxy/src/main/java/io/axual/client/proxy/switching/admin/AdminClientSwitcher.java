package io.axual.client.proxy.switching.admin;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Map;

import io.axual.client.proxy.generic.admin.AdminProxy;
import io.axual.client.proxy.generic.client.ClientProxyFactory;
import io.axual.client.proxy.switching.generic.BaseClientProxySwitcher;
import io.axual.discovery.client.DiscoveryResult;

public class AdminClientSwitcher extends BaseClientProxySwitcher<AdminProxy, SwitchingAdminConfig> {
    private static final Logger LOG = LoggerFactory.getLogger(AdminClientSwitcher.class);

    @Override
    @SuppressWarnings("unchecked")
    protected AdminProxy createProxyObject(SwitchingAdminConfig config, DiscoveryResult discoveryResult) {
        Map<String, Object> properties = config.getDownstreamConfigs();

        // Add all discovery properties
        properties.putAll(discoveryResult.getConfigs());

        LOG.info("Creating a new {} with properties: {}", config.getProxyType(), properties);
        ClientProxyFactory<AdminProxy> factory = config.getBackingFactory();
        return factory.create(properties);
    }

    @Override
    protected Duration getSwitchTimeout(SwitchingAdminConfig config, DiscoveryResult oldResult, DiscoveryResult newResult) {
        // AdminClients don't need to wait when switching
        return Duration.ZERO;
    }
}
