package io.axual.client.proxy.generic.client;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.axual.client.proxy.generic.config.BaseClientProxyConfig;

public abstract class StaticClientProxy<T extends ClientProxy, C extends BaseClientProxyConfig<T>> extends BaseClientProxy<C> {
    protected final T proxiedObject;
    private boolean closed = false;

    public static class ClientProxyInitializer<T extends ClientProxy, C extends BaseClientProxyConfig> {
        private final C config;
        private final T proxiedObject;

        public ClientProxyInitializer(C config, T proxiedObject) {
            this.config = config;
            this.proxiedObject = proxiedObject;
        }
    }

    public StaticClientProxy(C config) {
        super(config);
        ClientProxyFactory<T> factory = config.getBackingFactory();
        if (factory == null) {
            factory = config.getBackingFactory();
        }
        this.proxiedObject = factory.create(config.getDownstreamConfigs());
    }

    public StaticClientProxy(ClientProxyInitializer<T, C> initializer) {
        super(initializer.config);
        this.proxiedObject = initializer.proxiedObject;
        initialize(initializer);
    }

    protected void initialize(ClientProxyInitializer<T, C> initializer) {
        // Empty by default, but overridden by classes that add stuff onto their initializers
    }

    @Override
    public Map<String, Object> getConfigs() {
        Map<String, Object> result = new HashMap<>(proxiedObject.getConfigs());
        result.putAll(super.getConfigs());
        return result;
    }

    @Override
    public Object getConfig(String key) {
        Object result = super.getConfig(key);
        if (result == null) {
            result = proxiedObject.getConfig(key);
        }
        return result;
    }

    /**
     * Close this proxy.
     *
     * @param timeout the timeout
     * @param unit    the unit
     * @deprecated
     */
    @Deprecated
    public final void close(long timeout, TimeUnit unit) {
        close(Duration.ofMillis(unit.toMillis(timeout)));
    }

    @Override
    public void close(Duration timeout) {
        if (!closed) {
            closed = true;
            proxiedObject.close(timeout);
        }
    }
}
