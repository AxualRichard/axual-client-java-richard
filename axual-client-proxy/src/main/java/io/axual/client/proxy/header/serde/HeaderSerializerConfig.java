package io.axual.client.proxy.header.serde;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.Map;

import io.axual.client.proxy.generic.serde.BaseSerializerProxyConfig;

public class HeaderSerializerConfig<T> extends BaseSerializerProxyConfig<T> {
    public static final String BACKING_SERIALIZER_CONFIG = "headerserializer.backing.serializer";
    // If value headers are enabled, a number of bytes is prepended in the value of every message.
    // Those bytes contain the "value header", see {@link ValueHeader} for more information. When
    // set to false, metadata of every message will be encoded via Kafka headers only.
    public static final String ENABLE_VALUE_HEADERS_CONFIG = "enable.value.headers";
    public static final String ENABLE_VALUE_HEADERS_FALSE = "false";
    public static final String ENABLE_VALUE_HEADERS_TRUE = "true";

    private final boolean enableValueHeaders;

    public HeaderSerializerConfig(Map<String, Object> configs, boolean isKey) {
        super(configs, isKey, BACKING_SERIALIZER_CONFIG);
        filterDownstream(ENABLE_VALUE_HEADERS_CONFIG);

        enableValueHeaders = ENABLE_VALUE_HEADERS_TRUE.equals(
                parseAndFilterStringConfig(ENABLE_VALUE_HEADERS_CONFIG, false, ENABLE_VALUE_HEADERS_FALSE));
    }

    public boolean valueHeadersEnabled() {
        return enableValueHeaders;
    }
}
