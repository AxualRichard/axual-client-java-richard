package io.axual.client.proxy.generic.consumer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.consumer.ConsumerGroupMetadata;
import org.apache.kafka.clients.consumer.ConsumerRebalanceListener;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.clients.consumer.OffsetAndTimestamp;
import org.apache.kafka.clients.consumer.OffsetCommitCallback;
import org.apache.kafka.common.Metric;
import org.apache.kafka.common.MetricName;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.TopicPartition;

import java.time.Duration;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.OptionalLong;
import java.util.Set;
import java.util.regex.Pattern;

import io.axual.client.proxy.generic.client.StaticClientProxy;
import io.axual.client.proxy.generic.config.BaseClientProxyConfig;

public class StaticConsumerProxy<K, V, C extends BaseClientProxyConfig<ConsumerProxy<K, V>>> extends StaticClientProxy<ConsumerProxy<K, V>, C> implements ConsumerProxy<K, V> {
    public StaticConsumerProxy(C config) {
        super(config);
    }

    public StaticConsumerProxy(ClientProxyInitializer<ConsumerProxy<K, V>, C> initializer) {
        super(initializer);
    }

    @Override
    public Set<TopicPartition> assignment() {
        return proxiedObject.assignment();
    }

    @Override
    public Set<String> subscription() {
        return proxiedObject.subscription();
    }

    @Override
    public void subscribe(Collection<String> topics) {
        proxiedObject.subscribe(topics);
    }

    @Override
    public void subscribe(Collection<String> topics, ConsumerRebalanceListener listener) {
        proxiedObject.subscribe(topics, listener);
    }

    @Override
    public void assign(Collection<TopicPartition> partitions) {
        proxiedObject.assign(partitions);
    }

    @Override
    public void subscribe(Pattern pattern, ConsumerRebalanceListener listener) {
        proxiedObject.subscribe(pattern, listener);
    }

    @Override
    public void subscribe(Pattern pattern) {
        proxiedObject.subscribe(pattern);
    }

    @Override
    public void unsubscribe() {
        proxiedObject.unsubscribe();
    }

    /**
     * @deprecated
     */
    @Deprecated
    @Override
    public ConsumerRecords<K, V> poll(long timeout) {
        return proxiedObject.poll(timeout);
    }

    @Override
    public ConsumerRecords<K, V> poll(Duration duration) {
        return proxiedObject.poll(duration);
    }

    @Override
    public void commitSync() {
        proxiedObject.commitSync();
    }

    @Override
    public void commitSync(Duration duration) {
        proxiedObject.commitSync(duration);
    }

    @Override
    public void commitSync(Map<TopicPartition, OffsetAndMetadata> offsets) {
        proxiedObject.commitSync(offsets);
    }

    @Override
    public void commitSync(Map<TopicPartition, OffsetAndMetadata> offsets, Duration duration) {
        proxiedObject.commitSync(offsets, duration);
    }

    @Override
    public void commitAsync() {
        proxiedObject.commitAsync();
    }

    @Override
    public void commitAsync(OffsetCommitCallback callback) {
        proxiedObject.commitAsync(callback);
    }

    @Override
    public void commitAsync(Map<TopicPartition, OffsetAndMetadata> offsets, OffsetCommitCallback callback) {
        proxiedObject.commitAsync(offsets, callback);
    }

    @Override
    public void seek(TopicPartition topicPartition, OffsetAndMetadata offsetAndMetadata) {
        proxiedObject.seek(topicPartition, offsetAndMetadata);
    }

    @Override
    public void seek(TopicPartition partition, long offset) {
        proxiedObject.seek(partition, offset);
    }

    @Override
    public void seekToBeginning(Collection<TopicPartition> partitions) {
        proxiedObject.seekToBeginning(partitions);
    }

    @Override
    public void seekToEnd(Collection<TopicPartition> partitions) {
        proxiedObject.seekToEnd(partitions);
    }

    @Override
    public long position(TopicPartition partition) {
        return proxiedObject.position(partition);
    }

    @Override
    public long position(TopicPartition topicPartition, Duration duration) {
        return proxiedObject.position(topicPartition, duration);
    }

    /**
     * @deprecated
     * @param partition
     * @return
     */
    @Deprecated
    @Override
    public OffsetAndMetadata committed(TopicPartition partition) {
        return proxiedObject.committed(partition);
    }

    /**
     * @deprecated
     * @param topicPartition
     * @param duration
     * @return
     */
    @Deprecated
    @Override
    public OffsetAndMetadata committed(TopicPartition topicPartition, Duration duration) {
        return proxiedObject.committed(topicPartition, duration);
    }

    @Override
    public Map<TopicPartition, OffsetAndMetadata> committed(Set<TopicPartition> partitions) {
        return proxiedObject.committed(partitions);
    }

    @Override
    public Map<TopicPartition, OffsetAndMetadata> committed(Set<TopicPartition> partitions,
        Duration timeout) {
        return proxiedObject.committed(partitions,timeout);
    }

    @Override
    public Map<MetricName, ? extends Metric> metrics() {
        return proxiedObject.metrics();
    }

    @Override
    public List<PartitionInfo> partitionsFor(String topic) {
        return proxiedObject.partitionsFor(topic);
    }

    @Override
    public List<PartitionInfo> partitionsFor(String topic, Duration duration) {
        return proxiedObject.partitionsFor(topic, duration);
    }

    @Override
    public Map<String, List<PartitionInfo>> listTopics() {
        return proxiedObject.listTopics();
    }

    @Override
    public Map<String, List<PartitionInfo>> listTopics(Duration duration) {
        return proxiedObject.listTopics(duration);
    }

    @Override
    public Set<TopicPartition> paused() {
        return proxiedObject.paused();
    }

    @Override
    public void pause(Collection<TopicPartition> partitions) {
        proxiedObject.pause(partitions);
    }

    @Override
    public void resume(Collection<TopicPartition> partitions) {
        proxiedObject.resume(partitions);
    }

    @Override
    public Map<TopicPartition, OffsetAndTimestamp> offsetsForTimes(Map<TopicPartition, Long> timestampsToSearch) {
        return proxiedObject.offsetsForTimes(timestampsToSearch);
    }

    @Override
    public Map<TopicPartition, OffsetAndTimestamp> offsetsForTimes(Map<TopicPartition, Long> timestampsToSearch, Duration duration) {
        return proxiedObject.offsetsForTimes(timestampsToSearch, duration);
    }

    @Override
    public Map<TopicPartition, Long> beginningOffsets(Collection<TopicPartition> partitions) {
        return proxiedObject.beginningOffsets(partitions);
    }

    @Override
    public Map<TopicPartition, Long> beginningOffsets(Collection<TopicPartition> partitions, Duration duration) {
        return proxiedObject.beginningOffsets(partitions, duration);
    }

    @Override
    public Map<TopicPartition, Long> endOffsets(Collection<TopicPartition> partitions) {
        return proxiedObject.endOffsets(partitions);
    }

    @Override
    public Map<TopicPartition, Long> endOffsets(Collection<TopicPartition> collection, Duration duration) {
        return proxiedObject.endOffsets(collection, duration);
    }

    @Override
    public OptionalLong currentLag(TopicPartition topicPartition) {
        return proxiedObject.currentLag(topicPartition);
    }

    @Override
    public ConsumerGroupMetadata groupMetadata() {
        return proxiedObject.groupMetadata();
    }

    @Override
    public void enforceRebalance() {
        proxiedObject.enforceRebalance();
    }

    @Override
    public void enforceRebalance(String reason) {
        proxiedObject.enforceRebalance(reason);
    }

    @Override
    public void close(Duration duration) {
        proxiedObject.close(duration);
    }

    @Override
    public void wakeup() {
        proxiedObject.wakeup();
    }
}
