package io.axual.client.proxy.axual.consumer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.RangeAssignor;

import java.util.Map;

import io.axual.client.proxy.axual.generic.AxualProxyConfig;
import io.axual.client.proxy.generic.consumer.ConsumerProxy;
import io.axual.client.proxy.wrapped.consumer.WrappedConsumerFactory;

public class AxualConsumerConfig<K, V> extends AxualProxyConfig<ConsumerProxy<K, V>> {
    public static final String BACKING_FACTORY_CONFIG = "axualconsumer.backing.factory";
    public static final String CHAIN_CONFIG = "axualconsumer.chain";

    public AxualConsumerConfig(Map<String, Object> configs) {
        super(addDefaultFactory(configs, BACKING_FACTORY_CONFIG, WrappedConsumerFactory.class),
                "consumer",
                BACKING_FACTORY_CONFIG,
                CHAIN_CONFIG);

        // Set group id to application id
        putDownstream(ConsumerConfig.GROUP_ID_CONFIG, getApplicationId());

        // Enforce a default partition assignment strategy that allows consumers with the same
        // consumer group id to subscribe to different topics
        putDownstreamIfAbsent(ConsumerConfig.PARTITION_ASSIGNMENT_STRATEGY_CONFIG, RangeAssignor.class.getName());
    }
}
