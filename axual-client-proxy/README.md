# Axual Client Proxy

The Kafka Proxy implementation provides a simple way to migrate from a pure Kafka implementation 
to the Axual platform. 
It enables a full fledged Axual platform functionality with minimal code wise effort limited to 
replacing of some key Kafka components and imports with their Axual counterparts.
In order to achieve full Axual support, the developer needs to provide the necessary proxies 
through configuration.

## Packaging
The packages under `io.axual.client.proxy` are separated based on functionality.
Each of them plugs the functionality into a finally constructed chain of proxies which defines the
behavior of the producer or consumer object.

## Proxies
The proxies currently available are:
 * `header` appends the Axual platform headers which are used internally. 
 On the Axual platform this header should be enabled.
 * `logging` writes intermediate calls to a log. Is helpful for debugging purposes.
 * `resolving` facilitates multi-tenancy and multiple environments.
 * `switching` adds the switching functionality, enabling users to facilitate the active-active 
 Axual configuration. 
 This covers cases where a cluster is unavailable and internally handles this case by creating a 
 new Producer or Consumer pointed at a cluster that is available.
 
## How to use
Refer to the 
[Axual Proxy Documentation](https://docs.axual.io/client/5.8.1/how-to/kafkaproxy.html).
