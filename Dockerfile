FROM openjdk:11.0.7-jre-slim-buster
MAINTAINER Team Altair <altair@axual.io>

EXPOSE 8080

RUN mkdir -p /app /config /schemaJars /javaLibs \
  && apt-get update -y \
  && apt-get install -y --no-install-recommends curl \
  && rm -rf /var/lib/apt/lists/*
ADD axual-platform-test-standalone/target/*-exec.jar /app/standalone.jar
ENV LOADER_PATH=/config,/schemaJars,/javaLibs
VOLUME /config /schemaJars /javaLibs
WORKDIR /app
HEALTHCHECK --interval=2s --timeout=10s --retries=30 --start-period=5s \
  CMD curl -f http://127.0.0.1:8080/actuator/health || exit 1

ENTRYPOINT ["java","-jar", "/app/standalone.jar"]