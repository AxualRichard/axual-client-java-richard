package io.axual.serde.avro;

/*-
 * ========================LICENSE_START=================================
 * axual-serde
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.serialization.Serializer;

import java.util.HashMap;
import java.util.Map;

import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import io.confluent.kafka.serializers.KafkaAvroSerializer;

public class BaseAvroSerializer<T> implements Serializer<T> {
    private KafkaAvroSerializer serializer = null;

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        final BaseAvroSerializerConfig config = new BaseAvroSerializerConfig(new HashMap<>(configs));
        final SchemaRegistryClient schemaRegistryClient = SchemaRegistryUtil.createSrClient(
                config.getSslConfig(),
                config.getInnerConfig());

        if (serializer != null) {
            serializer.close();
        }

        serializer = new KafkaAvroSerializer(schemaRegistryClient);
        serializer.configure(configs, isKey);
    }

    @Override
    public byte[] serialize(String topic, Headers headers, T object) {
        return serialize(topic, object);
    }

    @Override
    public byte[] serialize(String topic, T object) {
        return serializer.serialize(topic, object);
    }

    @Override
    public void close() {
        if (serializer != null) {
            serializer.close();
        }
    }
}
