package io.axual.serde.avro;

/*-
 * ========================LICENSE_START=================================
 * axual-serde
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.Map;

import io.axual.common.config.BaseConfig;
import io.axual.common.config.SslConfig;
import io.axual.common.tools.KafkaUtil;
import io.confluent.kafka.serializers.KafkaAvroSerializerConfig;

public class BaseAvroSerializerConfig extends BaseConfig {
    private final KafkaAvroSerializerConfig innerConfig;
    private SslConfig sslConfig;

    public BaseAvroSerializerConfig(Map<String, Object> configs) {
        super(configs);
        innerConfig = new KafkaAvroSerializerConfig(configs);
        sslConfig = KafkaUtil.parseSslConfig(configs);
    }

    public KafkaAvroSerializerConfig getInnerConfig() {
        return innerConfig;
    }

    public SslConfig getSslConfig() {
        return sslConfig;
    }
}
