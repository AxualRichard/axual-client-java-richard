package io.axual.serde.avro;

/*-
 * ========================LICENSE_START=================================
 * axual-serde
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

import io.axual.common.config.PasswordConfig;
import io.axual.common.config.SslConfig;
import io.axual.common.exception.ClientException;

/**
 * @deprecated this will be removed in an upcoming release
 */
@Deprecated
public final class SslHelper {
    private static final Logger LOG = LoggerFactory.getLogger(SslHelper.class);

    private SslHelper() {
        // All methods are static
    }

    public static KeyManagerFactory getKeyManagerFactory(final String keystoreLocation, final PasswordConfig keystorePassword, final PasswordConfig keyPassword) throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException, UnrecoverableKeyException {
        return getKeyManagerFactory(keystoreLocation, keystorePassword, keyPassword, null);
    }

    public static KeyManagerFactory getKeyManagerFactory(final String keystoreLocation, final PasswordConfig keystorePassword, final PasswordConfig keyPassword, final SslConfig.KeystoreType keystoreType) throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException, UnrecoverableKeyException {
        KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());

        if (keystoreLocation != null && keystorePassword != null && keyPassword != null) {

            KeyStore keyStore = KeyStore.getInstance(keystoreType == null ? SslConfig.KeystoreType.JKS.name() : keystoreType.name());
            try (FileInputStream keyStoreInput = new FileInputStream(keystoreLocation)) {
                keyStore.load(keyStoreInput, keystorePassword.getValue().toCharArray());
            }
            keyManagerFactory.init(keyStore, keyPassword.getValue().toCharArray());
        }

        return keyManagerFactory;
    }

    private static TrustManagerFactory getTrustManagerFactory(final String truststoreLocation, final PasswordConfig truststorePassword) throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException {
        return getTrustManagerFactory(truststoreLocation, truststorePassword, null);
    }

    private static TrustManagerFactory getTrustManagerFactory(final String truststoreLocation, final PasswordConfig truststorePassword, SslConfig.TruststoreType truststoreType) throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException {
        TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());

        if (truststoreLocation != null && truststorePassword != null) {
            KeyStore trustStore = KeyStore.getInstance(truststoreType == null ? SslConfig.TruststoreType.JKS.name() : truststoreType.name());

            try (FileInputStream trustStoreInput = new FileInputStream(truststoreLocation)) {
                trustStore.load(trustStoreInput, truststorePassword.getValue().toCharArray());
            }
            trustManagerFactory.init(trustStore);
        }

        return trustManagerFactory;
    }

    /**
     * @deprecated
     */
    @Deprecated
    public static SSLSocketFactory createSSLSocketFactory(final String protocol, final String keystoreLocation, final PasswordConfig keystorePassword, final PasswordConfig keyPassword,final String truststoreLocation, final PasswordConfig truststorePassword) throws UnrecoverableKeyException, CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException, KeyManagementException {
        return createSSLSocketFactory(protocol, keystoreLocation, keystorePassword, keyPassword, null, truststoreLocation, truststorePassword, null);
    }

    /**
     * @deprecated
     */
    @Deprecated
    public static SSLSocketFactory createSSLSocketFactory(final String protocol, final String keystoreLocation, final PasswordConfig keystorePassword, final PasswordConfig keyPassword, final SslConfig.KeystoreType keystoreType, final String truststoreLocation, final PasswordConfig truststorePassword, SslConfig.TruststoreType truststoreType) throws UnrecoverableKeyException, CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException, KeyManagementException {

        KeyManagerFactory keyManagerFactory = null;

        if (keystoreLocation == null || keystorePassword == null || keyPassword == null) {
            LOG.info("No keystore location, keystore password or key password set");
        }

        keyManagerFactory = getKeyManagerFactory(keystoreLocation, keystorePassword, keyPassword);

        TrustManagerFactory trustManagerFactory = getTrustManagerFactory(truststoreLocation, truststorePassword);

        // Get an SSL context.
        SSLContext sslContext = SSLContext.getInstance(protocol);

        // Initialise our SSL context from the key/trust managers
        if (trustManagerFactory != null && keyManagerFactory != null) {
            sslContext.init(keyManagerFactory.getKeyManagers(), trustManagerFactory.getTrustManagers(), new SecureRandom());
        } else {
            throw new ClientException("Ssl configuration failed, key- and trustmanager factories not initialized");
        }
        return sslContext.getSocketFactory();
    }

    /**
     * @deprecated
     */
    @Deprecated
    public static SSLSocketFactory createSSLSocketFactory(final SslConfig config) {
        // extract SSL settings
        try {
            final String protocol = config.getSslProtocol();
            final String trustStoreLocation = config.getTruststoreLocation();
            final PasswordConfig truststorePassword = config.getTruststorePassword();
            final String keyStoreLocation = config.getKeystoreLocation();
            final PasswordConfig keystorePassword = config.getKeystorePassword();
            final PasswordConfig keyPassword = config.getKeyPassword();
            return SslHelper.createSSLSocketFactory(protocol, keyStoreLocation, keystorePassword, keyPassword, config.getKeystoreType(), trustStoreLocation, truststorePassword, config.getTruststoreType());
        } catch (IOException | CertificateException | NoSuchAlgorithmException | UnrecoverableKeyException | KeyStoreException | KeyManagementException e) {
            throw new ClientException(String.format("Exception occurred while creating SSLSocketFactory from %s", e.getMessage()), e);
        }
    }
}


