package io.axual.serde.valueheader;

/*-
 * ========================LICENSE_START=================================
 * axual-serde
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.serialization.Serializer;

import java.nio.ByteBuffer;
import java.util.Map;

import io.axual.serde.SerdeConstants;

public class ValueHeaderSerializer implements Serializer<ValueHeader> {
    private ByteBuffer buffer = null;

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        // Prepare a default header buffer for serialization
        buffer = ByteBuffer.allocate(SerdeConstants.HEADER_SIZE + SerdeConstants.EXTENDED_HEADER_SIZE);
        buffer.putShort((short) SerdeConstants.MAGIC_INT);
        buffer.putShort((short) SerdeConstants.VERSION_V2);
        buffer.putInt(SerdeConstants.EXTENDED_HEADER_SIZE);
    }

    @Override
    public byte[] serialize(String topic, ValueHeader header) {
        // Encode the flags in the header
        buffer.put(SerdeConstants.COPY_FLAGS_OFFSET, header.getCopyFlags());
        buffer.putLong(SerdeConstants.MESSAGE_ID_MSB_OFFSET, header.getMessageId().getMostSignificantBits());
        buffer.putLong(SerdeConstants.MESSAGE_ID_LSB_OFFSET, header.getMessageId().getLeastSignificantBits());
        buffer.putLong(SerdeConstants.SERIALIZATION_TIME_OFFSET, header.getSerializationTimestamp());
        return buffer.array();
    }

    @Override
    public void close() {
        // nothing to close
    }
}
