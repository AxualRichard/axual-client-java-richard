package io.axual.serde.valueheader;

/*-
 * ========================LICENSE_START=================================
 * axual-serde
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.UUID;

public class ValueHeader {
    private UUID messageId;
    private Long serializationTimestamp;
    private byte copyFlags;

    public ValueHeader() {
        this(UUID.randomUUID(), System.currentTimeMillis(), (byte) 0);
    }

    public ValueHeader(UUID messageId) {
        this(messageId != null ? messageId : UUID.randomUUID(), System.currentTimeMillis(), (byte) 0);
    }

    public ValueHeader(UUID messageId, Long serializationTimestamp, byte copyFlags) {
        this.messageId = messageId != null ? messageId : UUID.randomUUID();
        this.serializationTimestamp = serializationTimestamp;
        this.copyFlags = copyFlags;
    }

    public UUID getMessageId() {
        return messageId;
    }

    public Long getSerializationTimestamp() {
        return serializationTimestamp;
    }

    public byte getCopyFlags() {
        return copyFlags;
    }

    public boolean isLevelCopy(int level) {
        if (level >= 1 && level <= 31) {
            int flag = 1 << (level - 1);
            return (copyFlags & flag) != 0;
        }
        return false;
    }
}
