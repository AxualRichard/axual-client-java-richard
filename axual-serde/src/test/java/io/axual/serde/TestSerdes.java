package io.axual.serde;

/*-
 * ========================LICENSE_START=================================
 * axual-serde
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serializer;
import org.junit.Test;
import org.mockito.internal.util.reflection.Whitebox;

import java.io.IOException;
import java.util.HashMap;

import io.axual.client.test.TestObject;
import io.axual.serde.avro.SpecificAvroDeserializer;
import io.axual.serde.avro.SpecificAvroSerializer;
import io.axual.serde.utils.SerdeUtils;
import io.confluent.kafka.schemaregistry.client.MockSchemaRegistryClient;
import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import io.confluent.kafka.schemaregistry.client.rest.exceptions.RestClientException;
import io.confluent.kafka.serializers.KafkaAvroDeserializer;
import io.confluent.kafka.serializers.KafkaAvroDeserializerConfig;
import io.confluent.kafka.serializers.KafkaAvroSerializer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;

public class TestSerdes {
    private final String topic;
    private final TestObject content;
    private final Serializer<TestObject> keySerializer;
    private final Serializer<TestObject> valueSerializer;
    private final Deserializer<TestObject> keyDeserializer;
    private final Deserializer<TestObject> valueDeserializer;

    public TestSerdes() {
        content = TestObject.newBuilder().setName("axualplatform").setTimestamp(System.currentTimeMillis()).build();
        topic = "TEST-TOPIC";

        //add a kafka avro serializer and deserializer, this way we also test some of the confluent code
        SchemaRegistryClient schemaRegistry = new MockSchemaRegistryClient();
        try {
            schemaRegistry.register(topic + "-key", TestObject.getClassSchema());
            schemaRegistry.register(topic + "-value", TestObject.getClassSchema());
        } catch (RestClientException | IOException e) {
            e.printStackTrace();
        }

        HashMap<String, String> props = new HashMap<>();
        props.put(KafkaAvroDeserializerConfig.SCHEMA_REGISTRY_URL_CONFIG, "http://bogus.url");
        props.put(KafkaAvroDeserializerConfig.SPECIFIC_AVRO_READER_CONFIG, "true");

        KafkaAvroSerializer mockSerializer = new KafkaAvroSerializer(schemaRegistry);
        keySerializer = new SpecificAvroSerializer<>();
        valueSerializer = new SpecificAvroSerializer<>();
        Whitebox.setInternalState(keySerializer, "serializer", mockSerializer);
        Whitebox.setInternalState(valueSerializer, "serializer", mockSerializer);

        KafkaAvroDeserializer mockDeserializer = new KafkaAvroDeserializer(schemaRegistry, props);
        keyDeserializer = new SpecificAvroDeserializer<>();
        valueDeserializer = new SpecificAvroDeserializer<>();
        Whitebox.setInternalState(keyDeserializer, "deserializer", mockDeserializer);
        Whitebox.setInternalState(valueDeserializer, "deserializer", mockDeserializer);
    }

    @Test
    public void keySerializer_nullValue_nullReturned() {
        assertNull(keySerializer.serialize(topic, null));
    }

    @Test
    public void keySerializerDeserializer_correctObject_sameReturned() {
        byte[] message = keySerializer.serialize(topic, content);
        keySerializer.close();
        assertFalse(SerdeUtils.containsValueHeader(message));
        assertEquals(content, keyDeserializer.deserialize(topic, message));
        keyDeserializer.close();
    }

    @Test
    public void valueSerializerDeserializer_correctObject_sameReturned() {
        byte[] message = valueSerializer.serialize(topic, content);
        valueSerializer.close();
        assertEquals(content, valueDeserializer.deserialize(topic, message));
        valueDeserializer.close();
    }

    @Test
    public void valueDeserializer_nullValue_nullReturned() {
        assertNull(valueDeserializer.deserialize(topic, null));
    }

    @Test
    public void valueSerializerDeserializer_nullObject_sameReturned() {
        byte[] message = valueSerializer.serialize(topic, null);
        assertNull(valueDeserializer.deserialize(topic, message));
    }
}
