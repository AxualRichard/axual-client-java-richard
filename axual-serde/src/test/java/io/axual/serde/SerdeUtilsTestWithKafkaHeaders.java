package io.axual.serde;

/*-
 * ========================LICENSE_START=================================
 * axual-serde
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import junit.framework.TestCase;

import org.apache.kafka.common.header.internals.RecordHeaders;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serializer;
import org.junit.Before;
import org.junit.Test;
import org.mockito.internal.util.reflection.Whitebox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import io.axual.client.test.TestObject;
import io.axual.serde.avro.SpecificAvroDeserializer;
import io.axual.serde.avro.SpecificAvroSerializer;
import io.axual.serde.utils.SerdeUtils;
import io.confluent.kafka.schemaregistry.client.MockSchemaRegistryClient;
import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import io.confluent.kafka.serializers.KafkaAvroDeserializer;
import io.confluent.kafka.serializers.KafkaAvroDeserializerConfig;
import io.confluent.kafka.serializers.KafkaAvroSerializer;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;

public class SerdeUtilsTestWithKafkaHeaders {
    private static final Logger LOGGER = LoggerFactory.getLogger(SerdeUtilsTestWithKafkaHeaders.class);
    private static final String TOPIC_NAME = "axual-dummy";

    private TestObject content;
    private Serializer<TestObject> keySerializer;
    private Serializer<TestObject> valueSerializer;
    private Deserializer<TestObject> keyDeserializer;
    private Deserializer<TestObject> valueDeserializer;

    @Before
    public void setUp() {
        content = TestObject.newBuilder()
                .setName("axual")
                .setTimestamp(System.currentTimeMillis())
                .build();

        SchemaRegistryClient schemaRegistry = new MockSchemaRegistryClient();
        try {
            schemaRegistry.register(TOPIC_NAME + "-key", TestObject.getClassSchema());
            schemaRegistry.register(TOPIC_NAME + "-value", TestObject.getClassSchema());
        } catch (Exception e) {
            // Eat
        }

        Map<String, String> props = new HashMap<>();
        props.put(KafkaAvroDeserializerConfig.SCHEMA_REGISTRY_URL_CONFIG, "http://dummy.url");
        props.put(KafkaAvroDeserializerConfig.SPECIFIC_AVRO_READER_CONFIG, "true");

        KafkaAvroSerializer mockSerializer = new KafkaAvroSerializer(schemaRegistry);
        keySerializer = new SpecificAvroSerializer<>();
        Whitebox.setInternalState(keySerializer, "serializer", mockSerializer);
        keySerializer.configure(props, true);

        valueSerializer = new SpecificAvroSerializer<>();
        Whitebox.setInternalState(valueSerializer, "serializer", mockSerializer);
        valueSerializer.configure(props, false);

        KafkaAvroDeserializer mockDeserializer = new KafkaAvroDeserializer(schemaRegistry, props);
        keyDeserializer = new SpecificAvroDeserializer<>();
        valueDeserializer = new SpecificAvroDeserializer<>();
        Whitebox.setInternalState(keyDeserializer, "deserializer", mockDeserializer);
        Whitebox.setInternalState(valueDeserializer, "deserializer", mockDeserializer);
        keyDeserializer.configure(props, true);
        valueDeserializer.configure(props, false);
    }

//    @Test
//    public void verify_Default_KafkaHeaders() {
//        LOGGER.info("Initializing Kafka Headers with default value headers...");
//        Headers headers = new RecordHeaders();
//        valueSerializer.serialize(TOPIC_NAME, headers, content);
//        assertNotNull(MESSAGE_ID_HEADER + " header not found", headers.lastHeader(MESSAGE_ID_HEADER));
//        assertFalse("First Copy Bit should be unset",
//                SerdeUtilsWithoutOffset.isFirstOrderCopyWithoutOffset(headers.lastHeader(COPY_FLAGS_HEADER).value()));
//        assertFalse("Second Copy Bit should be unset",
//                SerdeUtilsWithoutOffset.isSecondOrderCopyWithoutOffset(headers.lastHeader(COPY_FLAGS_HEADER).value()));
//        assertNotNull(MESSAGE_ID_HEADER + " header not found", headers.lastHeader(MESSAGE_ID_HEADER));
//        assertNotNull(MESSAGE_ID_HEADER + " header not set",
//                SerdeUtilsWithoutOffset.getMessageIdWithoutOffset(headers.lastHeader(MESSAGE_ID_HEADER).value()));
//        assertNotNull(SERIALIZATION_TIME_HEADER + " header not set", headers.lastHeader(SERIALIZATION_TIME_HEADER));
//    }

//    @Test
//    public void verify_setCopy_KafkaHeaders() {
//        Headers headers = new RecordHeaders();
//        valueSerializer.serialize(TOPIC_NAME, headers, content);
//        LOGGER.info("Setting First and Second Order Copy bit...");
//        SerdeUtilsWithoutOffset.setFirstOrderCopyWithoutOffset(headers.lastHeader(COPY_FLAGS_HEADER).value());
//        SerdeUtilsWithoutOffset.setSecondOrderCopyWithoutOffset(headers.lastHeader(COPY_FLAGS_HEADER).value());
//        assertTrue("First Copy Bit should be set",
//                SerdeUtilsWithoutOffset.isFirstOrderCopyWithoutOffset(headers.lastHeader(COPY_FLAGS_HEADER).value()));
//        assertTrue("Second Copy Bit should be set",
//                SerdeUtilsWithoutOffset.isSecondOrderCopyWithoutOffset(headers.lastHeader(COPY_FLAGS_HEADER).value()));
//    }

    @Test
    public void keySerializer_nullValue_nullReturned() {
        TestCase.assertNull("Serialized key is not null",
                keySerializer.serialize(TOPIC_NAME, new RecordHeaders(), null));
    }

    @Test
    public void keySerializerDeserializer_correctObject_sameReturned() {
        byte[] message = keySerializer.serialize(TOPIC_NAME, new RecordHeaders(), content);
        keySerializer.close();
        TestCase.assertEquals("Deserialized key message not same as original",
                content, keyDeserializer.deserialize(TOPIC_NAME, new RecordHeaders(), message));
    }

    @Test
    public void valueSerializer_nullValue_nonNullReturned() {
        byte[] message = valueSerializer.serialize(TOPIC_NAME, new RecordHeaders(), null);
        assertNotNull("Serialized value is null", message);
        assertTrue("Serialized value should have value headers", SerdeUtils.containsValueHeader(message));
        valueSerializer.close();
    }

    @Test
    public void valueDeserializer_nullValue_nullReturned() {
        TestCase.assertNull("Deserialized value is not null",
                valueDeserializer.deserialize(TOPIC_NAME, new RecordHeaders(), null));
        valueDeserializer.close();
    }

    @Test
    public void valueSerializer_kafkaHeadersPresent_ValueHeadersPresent() {
        RecordHeaders headers = new RecordHeaders();
        byte[] message = valueSerializer.serialize(TOPIC_NAME, headers, content);
        valueSerializer.close();
        assertTrue("Value headers missing!", SerdeUtils.containsValueHeader(message));
        assertTrue("Kafka headers missing!", headers.toArray().length > 0);
    }

//    @Test
//    public void valueSerializer_compareHeaders() {
//        RecordHeaders headers = new RecordHeaders();
//        byte[] message = valueSerializer.serialize(TOPIC_NAME, headers, content);
//        valueSerializer.close();
//
//        assertNotNull(headers.lastHeader(COPY_FLAGS_HEADER));
//        byte copyFlags = SerdeUtils.importCopyFlags(message);
//        boolean kafkaHeaderFirstCopyBit =
//                SerdeUtilsWithoutOffset.isFirstOrderCopyWithoutOffset(headers.lastHeader(COPY_FLAGS_HEADER).value());
//        boolean kafkaHeaderSecondCopyBit =
//                SerdeUtilsWithoutOffset.isSecondOrderCopyWithoutOffset(headers.lastHeader(COPY_FLAGS_HEADER).value());
//
//        assertEquals("First Copy Bit mismatch!", BitUtils.isBitSet(copyFlags, 0), kafkaHeaderFirstCopyBit);
//        assertEquals("Second Copy Bit mismatch!", BitUtils.isBitSet(copyFlags, 1), kafkaHeaderSecondCopyBit);
//
//        assertNotNull(headers.lastHeader(MESSAGE_ID_HEADER));
//        UUID valueHeaderMessageId = SerdeUtils.getMessageId(message);
//        UUID kafkaHeaderMessageId =
//                SerdeUtilsWithoutOffset.getMessageIdWithoutOffset(headers.lastHeader(MESSAGE_ID_HEADER).value());
//        assertEquals("Message Id mismatch!", valueHeaderMessageId, kafkaHeaderMessageId);
//
//        assertNotNull(headers.lastHeader(SERIALIZATION_TIME_HEADER));
//        long valueHeaderSerializationTimestamp = SerdeUtils.getSerializationTimestamp(message);
//        long kafkaHeaderSerializationTimestamp =
//                SerdeUtilsWithoutOffset.getSerializationTimestampWithoutOffset(
//                        headers.lastHeader(SERIALIZATION_TIME_HEADER).value());
//        assertEquals("Serialization Timestamp mismatch!", valueHeaderSerializationTimestamp, kafkaHeaderSerializationTimestamp);
//    }

    @Test
    public void valueSerializerDeserializer_correctObject_sameReturned() {
        RecordHeaders headers = new RecordHeaders();
        byte[] message = valueSerializer.serialize(TOPIC_NAME, headers, content);
        valueSerializer.close();
        TestCase.assertEquals("Deserialized key message not same as original",
                content, valueDeserializer.deserialize(TOPIC_NAME, headers, message));
        valueDeserializer.close();
    }

    @Test
    public void valueDeserializer_incorrectObject_noValueHeaderFound() {
        RecordHeaders headers = new RecordHeaders();
        byte[] message = valueSerializer.serialize(TOPIC_NAME, headers, content);
        message[0] = 0;
        assertFalse("Custom Value Headers found - should be absent",
                SerdeUtils.containsValueHeader(message));
        valueSerializer.close();
    }
}
