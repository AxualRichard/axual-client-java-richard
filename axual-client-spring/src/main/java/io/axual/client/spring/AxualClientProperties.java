package io.axual.client.spring;

/*-
 * ========================LICENSE_START=================================
 * axual-client-spring
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import javax.validation.constraints.NotBlank;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import io.axual.common.config.ClientConfig;
import io.axual.common.config.PasswordConfig;
import io.axual.common.config.SslConfig;
import lombok.Data;

@Data
@Component
@ConfigurationProperties("axual.client")
public class AxualClientProperties {
    @NotBlank
    private String applicationId;
    @NotBlank
    private String applicationVersion;
    @NotBlank
    private String endpoint;
    @NotBlank
    private String tenant;
    @NotBlank
    private String environment;

    @NotBlank
    private String sslKeyPassword;
    @NotBlank
    private String sslKeystoreLocation;
    @NotBlank
    private String sslKeystorePassword;
    @NotBlank
    private String sslTruststoreLocation;
    @NotBlank
    private String sslTruststorePassword;

    private Boolean enableHostnameVerification = false;

    private Boolean enableValidateTruststore = true;

    public ClientConfig asClientConfig() {
        return ClientConfig.newBuilder()
                .setApplicationId(applicationId)
                .setApplicationVersion(applicationVersion)
                .setEndpoint(endpoint)
                .setTenant(tenant)
                .setEnvironment(environment)
                .setSslConfig(SslConfig.newBuilder()
                        .setKeyPassword(new PasswordConfig(sslKeyPassword))
                        .setKeystoreLocation(sslKeystoreLocation)
                        .setKeystorePassword(new PasswordConfig(sslKeystorePassword))
                        .setTruststoreLocation(sslTruststoreLocation)
                        .setTruststorePassword(new PasswordConfig(sslTruststorePassword))
                        .setEnableHostnameVerification(enableHostnameVerification)
                        .setEnableValidateTruststore(enableValidateTruststore)
                        .build())
                .build();
    }

}
