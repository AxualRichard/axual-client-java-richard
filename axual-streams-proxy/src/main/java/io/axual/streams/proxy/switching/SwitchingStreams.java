package io.axual.streams.proxy.switching;

/*-
 * ========================LICENSE_START=================================
 * axual-streams-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.Metric;
import org.apache.kafka.common.MetricName;
import org.apache.kafka.common.serialization.Serializer;
import org.apache.kafka.streams.KeyQueryMetadata;
import org.apache.kafka.streams.StoreQueryParameters;
import org.apache.kafka.streams.errors.StreamsUncaughtExceptionHandler;
import org.apache.kafka.streams.processor.StreamPartitioner;
import org.apache.kafka.streams.processor.ThreadMetadata;
import org.apache.kafka.streams.state.StreamsMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import io.axual.client.proxy.switching.discovery.DiscoverySubscriber;
import io.axual.client.proxy.switching.generic.SwitchingProxy;
import io.axual.common.annotation.InterfaceStability;
import io.axual.common.concurrent.LockedObject;
import io.axual.common.tools.MapUtil;
import io.axual.common.tools.SleepUtil;
import io.axual.discovery.client.tools.DiscoveryConfigParserV2;
import io.axual.streams.proxy.generic.factory.UncaughtExceptionHandlerFactory;
import io.axual.streams.proxy.generic.proxy.StreamsProxy;

/**
 * Class to coordinate between the actual KafkaStreams topology, the restarterThread and the
 * streamTemplate. This makes sure the streaming is started and closed correctly, and restarted when
 * the configuration received from the CDAPI has changed.
 */
@InterfaceStability.Evolving
public class SwitchingStreams extends SwitchingProxy<StreamsProxy, SwitchingStreamsConfig> implements StreamsProxy {
    private static final Logger LOG = LoggerFactory.getLogger(SwitchingStreams.class);

    private final AtomicBoolean running = new AtomicBoolean(false);
    private Thread restarterThread;

    public SwitchingStreams(Map<String, Object> configs) {
        super(new SwitchingStreamsConfig(configs),
                new DiscoverySubscriber<>(
                        new DiscoveryConfigParserV2().parse(configs),
                        SwitchingStreams.class.getSimpleName(),
                        new StreamsSwitcher(),
                        false));
    }

    public SwitchingStreams(Properties properties) {
        this(MapUtil.objectToStringMap(properties));
    }

    @Override
    public void setStateListener(final org.apache.kafka.streams.KafkaStreams.StateListener listener) {
        try (LockedObject<StreamsProxy>.ReadLock readLock = getReadLock()) {
            readLock.object.setStateListener(listener);
        }
    }

    @Override
    public org.apache.kafka.streams.KafkaStreams.State state() {
        try (LockedObject<StreamsProxy>.ReadLock readLock = getReadLock()) {
            return readLock.object.state();
        }
    }

    @Override
    public Map<MetricName, ? extends Metric> metrics() {
        try (LockedObject<StreamsProxy>.ReadLock readLock = getReadLock()) {
            return readLock.object.metrics();
        }
    }

    @Override
    public synchronized void start() {
        startStreaming();
    }

    @Override
    public void stop() {
        close();
    }

    @Override
    public void close(Duration timeout) {
        stopStreaming();
        super.close(timeout);
    }

    @Override
    public void cleanUp() {
        // Method is present here for compatibility reasons with Kafka's KafkaStreams class.
        // No operation here since we already cleanup when we close.
    }

    @Override
    public void setUncaughtExceptionHandler(StreamsUncaughtExceptionHandler eh) {
        try (LockedObject<StreamsProxy>.ReadLock readLock = getReadLock()) {
            readLock.object.setUncaughtExceptionHandler(eh);
        }
    }

    /**
     * @deprecated use {@link io.axual.streams.proxy.switching.SwitchingStreams#setUncaughtExceptionHandler(StreamsUncaughtExceptionHandler)} instead.
     */
    @Deprecated
    @Override
    public void setUncaughtExceptionHandler(Thread.UncaughtExceptionHandler eh) {
        try (LockedObject<StreamsProxy>.ReadLock readLock = getReadLock()) {
            readLock.object.setUncaughtExceptionHandler(eh);
        }
    }

    /**
     *
     * @deprecated
     */
    @Deprecated
    @Override
    public Collection<StreamsMetadata> allMetadata() {
        try (LockedObject<StreamsProxy>.ReadLock readLock = getReadLock()) {
            return readLock.object.allMetadata();
        }
    }

    /**
     *
     * @deprecated
     */
    @Deprecated
    @Override
    public Collection<StreamsMetadata> allMetadataForStore(final String storeName) {
        try (LockedObject<StreamsProxy>.ReadLock readLock = getReadLock()) {
            return readLock.object.allMetadataForStore(storeName);
        }
    }

    /**
     *
     * @deprecated
     */
    @Deprecated
    @Override
    public Set<ThreadMetadata> localThreadsMetadata() {
        try (LockedObject<StreamsProxy>.ReadLock readLock = getReadLock()) {
            return readLock.object.localThreadsMetadata();
        }
    }

    @Override
    public <K> KeyQueryMetadata queryMetadataForKey(String storeName, K key, Serializer<K> keySerializer) {
        try (LockedObject<StreamsProxy>.ReadLock readLock = getReadLock()) {
            return readLock.object.queryMetadataForKey(storeName, key, keySerializer);
        }
    }

    @Override
    public <K> KeyQueryMetadata queryMetadataForKey(String storeName, K key, StreamPartitioner<? super K, ?> partitioner) {
        try (LockedObject<StreamsProxy>.ReadLock readLock = getReadLock()) {
            return readLock.object.queryMetadataForKey(storeName, key, partitioner);
        }
    }

    @Override
    public <T> T store(StoreQueryParameters<T> storeQueryParameters) {
        try (LockedObject<StreamsProxy>.ReadLock readLock = getReadLock()) {
            return readLock.object.store(storeQueryParameters);
        }
    }

    @Override
    public Optional<String> addStreamThread() {
        try (LockedObject<StreamsProxy>.ReadLock readLock = getReadLock()) {
            return readLock.object.addStreamThread();
        }
    }

    @Override
    public Optional<String> removeStreamThread() {
        try (LockedObject<StreamsProxy>.ReadLock readLock = getReadLock()) {
            return readLock.object.removeStreamThread();
        }
    }

    @Override
    public Optional<String> removeStreamThread(Duration timeout) {
        try (LockedObject<StreamsProxy>.ReadLock readLock = getReadLock()) {
            return readLock.object.removeStreamThread(timeout);
        }
    }

    @Override
    public Collection<org.apache.kafka.streams.StreamsMetadata> metadataForAllStreamsClients() {
        try (LockedObject<StreamsProxy>.ReadLock readLock = getReadLock()) {
            return readLock.object.metadataForAllStreamsClients();
        }
    }

    @Override
    public Collection<org.apache.kafka.streams.StreamsMetadata> streamsMetadataForStore(String storeName) {
        try (LockedObject<StreamsProxy>.ReadLock readLock = getReadLock()) {
            return readLock.object.streamsMetadataForStore(storeName);
        }
    }

    @Override
    public Set<org.apache.kafka.streams.ThreadMetadata> metadataForLocalThreads() {
        try (LockedObject<StreamsProxy>.ReadLock readLock = getReadLock()) {
            return readLock.object.metadataForLocalThreads();
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // End of public interface of SwitchingStreams
    ///////////////////////////////////////////////////////////////////////////////////////////////

    private synchronized void startStreaming() {
        if (running.compareAndSet(false, true)) {
            UncaughtExceptionHandlerFactory factory = config.getUncaughtExceptionHandlerFactory();
            StreamsUncaughtExceptionHandler handler = factory.create(this);
            try (LockedObject<StreamsProxy>.ReadLock readLock = getReadLock()) {
                // Start streaming
                if (handler != null) {
                    readLock.object.setUncaughtExceptionHandler(handler);
                }
                readLock.object.start();
            }

            // Create a restarter thread that restarts streaming upon DISCOVERY result change
            if (restarterThread == null || !restarterThread.isAlive()) {
                restarterThread = new Thread(new Restarter(), "streamRestarter");
                restarterThread.setDaemon(true);
                restarterThread.start();
            }
        } else {
            LOG.warn("Could not start streaming cause it appears it is already/still running");
        }
    }

    private synchronized void stopStreaming() {
        if (running.get()) {
            if (restarterThread != null) {
                // Interrupt the restarter thread, no need to wait for it to finish though
                restarterThread.interrupt();
                restarterThread = null;
            }
            try (LockedObject<StreamsProxy>.WriteLock writeLock = getWriteLock()) {
                writeLock.getObject().close();
                writeLock.getObject().cleanUp();
            }
            running.set(false);
        } else {
            LOG.info("Close was called on StreamRunner but was already stopped.");
        }
    }

    private class Restarter implements Runnable {
        @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted() && running.get()) {
                if (maybeReplaceProxiedObject(false)) {
                    // Reset the running flag to allow initialization of the new backing streams
                    if (!running.compareAndSet(true, false)) {
                        LOG.warn("Replaced a Streams instance which was not running, should not happen");
                    }
                    startStreaming();
                }

                // If still running then sleep
                if (running.get()) {
                    SleepUtil.sleep(Duration.ofSeconds(1));
                }
            }
        }
    }
}
