package io.axual.streams.proxy.axual;

/*-
 * ========================LICENSE_START=================================
 * axual-streams-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.admin.Admin;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.streams.KafkaClientSupplier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import io.axual.client.proxy.axual.admin.AxualAdminClient;
import io.axual.client.proxy.axual.admin.AxualAdminConfig;
import io.axual.client.proxy.axual.consumer.AxualConsumer;
import io.axual.client.proxy.axual.consumer.AxualConsumerConfig;
import io.axual.client.proxy.axual.producer.AxualProducer;
import io.axual.client.proxy.axual.producer.AxualProducerConfig;
import io.axual.client.proxy.generic.admin.WrappedAdmin;
import io.axual.common.annotation.InterfaceStability;
import io.axual.common.exception.ConfigurationException;

@InterfaceStability.Evolving
public class AxualClientSupplier implements KafkaClientSupplier {
    public static final String CHAIN_CONFIG = "axualclientsupplier.chain";

    private static final Logger LOG = LoggerFactory.getLogger(AxualClientSupplier.class);

    private final KafkaClientSupplier backingClientSupplier;

    public AxualClientSupplier(KafkaClientSupplier backingClientSupplier) {
        this.backingClientSupplier = backingClientSupplier;
    }

    private String getChainConfig(Map<String, Object> configs) {
        Object result = configs.get(CHAIN_CONFIG);
        if (result instanceof String) {
            return (String) result;
        }

        LOG.error("AxualClientSupplier proxy chain not configured: {}", configs);
        throw new ConfigurationException("ClientSupplier proxy chain not configured");
    }

    @Override
    public Admin getAdmin(Map<String, Object> configs) {
        Map<String, Object> conf = new HashMap<>(configs);
        conf.put(AxualAdminConfig.CHAIN_CONFIG, getChainConfig(configs));
        conf.put(AxualAdminConfig.BACKING_FACTORY_CONFIG, new SupplierAdminFactory(backingClientSupplier));
        return new WrappedAdmin(new AxualAdminClient(conf));
    }

    @Override
    public Producer<byte[], byte[]> getProducer(Map<String, Object> configs) {
        Map<String, Object> conf = new HashMap<>(configs);
        conf.put(AxualProducerConfig.CHAIN_CONFIG, getChainConfig(configs));
        conf.put(AxualProducerConfig.BACKING_FACTORY_CONFIG, new SupplierProducerFactory(backingClientSupplier));
        return new AxualProducer<>(conf);
    }

    @Override
    public Consumer<byte[], byte[]> getConsumer(Map<String, Object> configs) {
        Map<String, Object> conf = new HashMap<>(configs);
        conf.put(AxualConsumerConfig.CHAIN_CONFIG, getChainConfig(configs));
        conf.put(AxualConsumerConfig.BACKING_FACTORY_CONFIG, new SupplierConsumerFactory(backingClientSupplier));
        return new AxualConsumer<>(conf);
    }

    @Override
    public Consumer<byte[], byte[]> getRestoreConsumer(Map<String, Object> configs) {
        return getConsumer(configs);
    }

    @Override
    public Consumer<byte[], byte[]> getGlobalConsumer(Map<String, Object> configs) {
        return getConsumer(configs);
    }
}
