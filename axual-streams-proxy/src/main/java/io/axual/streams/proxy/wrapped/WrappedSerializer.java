package io.axual.streams.proxy.wrapped;

/*-
 * ========================LICENSE_START=================================
 * axual-streams-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Map;

public class WrappedSerializer<T> implements Serializer<T> {
    private Serializer<T> backingSerializer = null;

    public void setBackingSerializer(Serializer<T> backingSerializer) {
        this.backingSerializer = backingSerializer;
    }

    @Override
    public void configure(Map configs, boolean isKey) {
        backingSerializer.configure(configs, isKey);
    }

    @Override
    public byte[] serialize(String topic, Headers headers, T data) {
        return backingSerializer.serialize(topic, headers, data);
    }

    @Override
    public void close() {
        backingSerializer.close();
    }

    @Override
    public byte[] serialize(String topic, T data) {
        return backingSerializer.serialize(topic, data);
    }
}
