package io.axual.streams.proxy.wrapped;

/*-
 * ========================LICENSE_START=================================
 * axual-streams-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.serialization.Deserializer;

import java.util.Map;

public class WrappedDeserializer<T> implements Deserializer<T> {
    private Deserializer<T> backingDeserializer = null;

    public void setBackingDeserializer(Deserializer<T> backingDeserializer) {
        this.backingDeserializer = backingDeserializer;
    }

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        backingDeserializer.configure(configs, isKey);
    }

    @Override
    public T deserialize(String topic, Headers headers, byte[] data) {
        return backingDeserializer.deserialize(topic, headers, data);
    }

    @Override
    public void close() {
        backingDeserializer.close();
    }

    @Override
    public T deserialize(String topic, byte[] data) {
        return backingDeserializer.deserialize(topic, data);
    }
}
