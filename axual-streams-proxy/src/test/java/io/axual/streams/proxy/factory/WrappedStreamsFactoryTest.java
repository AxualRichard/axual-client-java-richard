package io.axual.streams.proxy.factory;

/*-
 * ========================LICENSE_START=================================
 * axual-streams-proxy
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import static org.junit.Assert.assertEquals;

import io.axual.common.tools.ResourceUtil;
import io.axual.streams.proxy.generic.factory.TopologyFactory;
import io.axual.streams.proxy.generic.streams.Streams;
import io.axual.streams.proxy.wrapped.WrappedStreamsConfig;
import io.axual.streams.proxy.wrapped.WrappedStreamsFactory;
import java.util.HashMap;
import java.util.Map;
import org.apache.kafka.common.config.SslConfigs;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsConfig;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WrappedStreamsFactoryTest {
    private static final Logger LOG = LoggerFactory.getLogger(WrappedStreamsFactoryTest.class);
    private static final String TOPIC = "input-topic";

  @Test
  public void createStreams() {
    WrappedStreamsFactory factory = new WrappedStreamsFactory();
    TopologyFactory topologyFactory = builder -> {
        builder.stream(TOPIC).foreach((k,v)->{throw new RuntimeException("Should never reach this");});
        return builder.build();
    };
    // create minimal set of properties
    Map<String, Object> configs = new HashMap<>();
    configs
        .put(WrappedStreamsConfig.TOPOLOGY_FACTORY_CONFIG, topologyFactory);
    configs.put(StreamsConfig.APPLICATION_ID_CONFIG, "io.axual.teststream");
    configs.put(SslConfigs.SSL_KEY_PASSWORD_CONFIG, "notsecret");
    configs.put(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG,
        ResourceUtil.getResourceAsFile("ssl/axual.client.keystore.jks"));
    configs.put(SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG, "notsecret");
    configs.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG,
        ResourceUtil.getResourceAsFile("ssl/axual.client.truststore.jks"));
    configs.put(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG, "notsecret");
    configs.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9999");
    Streams streams = factory.create(configs);
    assertEquals(KafkaStreams.State.CREATED, streams.state());
  }
}
