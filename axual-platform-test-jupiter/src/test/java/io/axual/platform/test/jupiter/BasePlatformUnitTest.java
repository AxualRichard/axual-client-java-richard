package io.axual.platform.test.jupiter;

/*-
 * ========================LICENSE_START=================================
 * axual-platform-test-jupiter
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import io.axual.platform.test.core.PlatformUnit;
import java.util.Arrays;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class BasePlatformUnitTest extends AbstractPlatformUnitTest {

  private static final Logger LOG = LoggerFactory.getLogger(BasePlatformUnitTest.class);

  @RegisterExtension
  BasePlatformUnit platformUnit = new BasePlatformUnit(new PlatformUnit(2,true));

  BasePlatformUnitTest() {
    super(LOG);
  }

  @Test
  void testBase_ClusterA() {
    runTest(platformUnit.platform(), platformUnit.platform().getCluster(0),
        Arrays.asList(platformUnit.platform().getCluster(0), platformUnit.platform().getCluster(1)), 50, 10000, 30000);
  }

  @Test
  void testBase_ClusterB() {
    runTest(platformUnit.platform(), platformUnit.platform().getCluster(0),
        Arrays.asList(platformUnit.platform().getCluster(0), platformUnit.platform().getCluster(1)), 50, 10000, 30000);
  }
}
