package io.axual.platform.test.jupiter;

/*-
 * ========================LICENSE_START=================================
 * axual-platform-test-jupiter
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */


import io.axual.common.annotation.InterfaceStability;
import io.axual.platform.test.core.InstanceUnit;
import io.axual.platform.test.core.PlatformUnit;
import io.axual.platform.test.core.StreamConfig;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

/**
 * The base class implementing the JUnit Jupiter callbacks to start and stop the Axual Test Platform
 * for each test.
 */
@InterfaceStability.Evolving
public class BasePlatformUnit<T extends BasePlatformUnit> implements BeforeEachCallback,
    AfterEachCallback {

    private final PlatformUnit platform;

    /**
     * Constructs the base platform according to the provided PlatformUnit configuration
     *
     * @param platform the platform configuration to use
     */
    BasePlatformUnit(final PlatformUnit platform) {
        this.platform = platform;
    }

    /**
     * Starts the Axual Test Platform before each test
     */
    @Override
    public void beforeEach(ExtensionContext extensionContext) {
        platform.start();
    }

    /**
     * Stops the Axual Test Platform before each test after each test
     */
    @Override
    public void afterEach(ExtensionContext extensionContext) {
        platform.stop();
    }


    /**
     * Get the platform configuration used
     *
     * @return the current test platform configuration
     */
    public PlatformUnit platform() {
        return platform;
    }

    /**
     * Get the instance configuration used
     *
     * @return the current instance configuration of the test platform
     */
    public InstanceUnit instance() {
        return platform.getInstance();
    }

    /**
     * Add a stream to the current configuration
     *
     * @param streamConfig The configuration for the specific Stream
     * @return the platform extension
     */
    public T addStream(StreamConfig streamConfig) {
        platform().addStream(streamConfig);
        return (T) this;
    }
}
