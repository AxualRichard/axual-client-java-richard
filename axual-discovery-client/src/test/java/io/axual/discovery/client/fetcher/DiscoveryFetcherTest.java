package io.axual.discovery.client.fetcher;

/*-
 * ========================LICENSE_START=================================
 * axual-discovery-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import com.google.common.base.Charsets;

import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.ByteArrayInputStream;

import javax.net.ssl.SSLContext;

import io.axual.common.config.PasswordConfig;
import io.axual.common.config.SslConfig;
import io.axual.common.config.SslConfig.KeystoreType;
import io.axual.common.config.SslConfig.TruststoreType;
import io.axual.common.test.TestUtils;
import io.axual.common.tools.SslUtil;
import io.axual.discovery.client.DiscoveryConfig;
import io.axual.discovery.client.DiscoveryResult;
import io.axual.discovery.client.exception.ConfigurationFetchFailedException;
import io.axual.discovery.client.exception.DiscoveryException;

import static java.net.HttpURLConnection.HTTP_FORBIDDEN;
import static java.net.HttpURLConnection.HTTP_NOT_FOUND;
import static java.net.HttpURLConnection.HTTP_NO_CONTENT;
import static java.net.HttpURLConnection.HTTP_OK;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.powermock.api.mockito.PowerMockito.whenNew;

@RunWith(PowerMockRunner.class)
@PrepareForTest({
        HttpClients.class,
        DiscoveryFetcher.class,
        SslUtil.class
})
@PowerMockIgnore({"javax.net.ssl.*", "jdk.internal.reflect.*"})
public class DiscoveryFetcherTest {
    private static final String DISCOVERY_API_BOOTSTRAP_SERVERS_NAME = "bootstrap.servers";
    private static final String DISCOVERY_API_SCHEMA_REGISTRY_URL_NAME = "schema.registry.url";
    private static final String DISCOVERY_API_REST_PROXY_URL_NAME = "rest.proxy.url";
    private static final String DISCOVERY_API_TTL_NAME = "ttl";
    private static final String DISCOVERY_API_DISTRIBUTOR_TIMEOUT_NAME = "distributor.timeout";

    private static final String DISCOVERY_API_BOOTSTRAP_SERVERS_VALUE = "bootstrap.over.there:6444";
    private static final String DISCOVERY_API_SCHEMA_REGISTRY_URL_VALUE = "http://schema.over.there:3222";
    private static final String DISCOVERY_API_REST_PROXY_URL_VALUE = "http://rest.over.there:9666";
    private static final String DISCOVERY_API_TTL_VALUE = "3000";
    private static final String DISCOVERY_API_DISTRIBUTOR_TIMEOUT_VALUE = "6000";

    private static final String DISCOVERY_RESPONSE = String.format("{" +
                    "\"%s\": \"%s\"," +
                    "\"%s\": \"%s\"," +
                    "\"%s\": \"%s\"," +
                    "\"%s\": \"%s\"," +
                    "\"%s\": \"%s\"" +
                    "}"
            , DISCOVERY_API_BOOTSTRAP_SERVERS_NAME, DISCOVERY_API_BOOTSTRAP_SERVERS_VALUE
            , DISCOVERY_API_SCHEMA_REGISTRY_URL_NAME, DISCOVERY_API_SCHEMA_REGISTRY_URL_VALUE
            , DISCOVERY_API_TTL_NAME, DISCOVERY_API_TTL_VALUE
            , DISCOVERY_API_DISTRIBUTOR_TIMEOUT_NAME, DISCOVERY_API_DISTRIBUTOR_TIMEOUT_VALUE
            , DISCOVERY_API_REST_PROXY_URL_NAME, DISCOVERY_API_REST_PROXY_URL_VALUE
    );

    private static final String APPLICATIONID = "io.axual.unit-test";
    private static final String APPLICATIONVERSION = "1.0.0";

    private static final String DISCOVERY_API_URL_HTTP = "http://local:9043/somewhere/";
    private static final String DISCOVERY_API_URL_HTTPS = "https://local:9043/somewhere/";
    private static final String DISCOVERY_API_URL_FAULT = "ftp://local:9043/somewhere/";

    private static final String SSLKEYSTORELOCATION = TestUtils.KEYSTORE_JKS_PATH;
    private static final String SSLTRUSTSTORELOCATION = TestUtils.TRUSTSTORE_JKS_PATH;
    private static final PasswordConfig SSLKEYPASSWORD = new PasswordConfig("notsecret");
    private static final PasswordConfig SSLKEYSTOREPASSWORD = new PasswordConfig("notsecret");
    private static final PasswordConfig SSLTRUSTSTOREPASSWORD = new PasswordConfig("notsecret");

    @Mock
    CloseableHttpClient mockedHttpClient;

    @Mock
    HttpClientBuilder mockedClientBuilder;

    @Mock
    SSLContext mockedSslContext;

    @Mock
    HttpGet mockHttpGetRequest;

    @Mock
    CloseableHttpResponse mockedHttpResponse;

    @Mock
    StatusLine mockedStatusLine;

    @Mock
    HttpEntity mockedEntity;

    protected DiscoveryConfig createDiscoveryConfig(final String discoveryUrl) {
        return DiscoveryConfig.newBuilder()
                .setApplicationId(APPLICATIONID)
                .setApplicationVersion(APPLICATIONVERSION)
                .setEndpoint(discoveryUrl)
                .setSslConfig(createSslConfig())
                .build();
    }

    private SslConfig createSslConfig() {
        return SslConfig.newBuilder()
                .setKeystoreType(KeystoreType.JKS)
                .setKeyPassword(SSLKEYPASSWORD)
                .setKeystoreLocation(SSLKEYSTORELOCATION)
                .setKeystorePassword(SSLKEYSTOREPASSWORD)
                .setTruststoreType(TruststoreType.JKS)
                .setTruststoreLocation(SSLTRUSTSTORELOCATION)
                .setTruststorePassword(SSLTRUSTSTOREPASSWORD)
                .build();
    }

    protected void validateProviderProperties(final DiscoveryResult discoveryResult) {
        assertNotNull(discoveryResult);
        assertTrue(discoveryResult.getConfigs().containsKey(DISCOVERY_API_BOOTSTRAP_SERVERS_NAME));
        assertTrue(discoveryResult.getConfigs().containsKey(DISCOVERY_API_SCHEMA_REGISTRY_URL_NAME));
        assertTrue(discoveryResult.getConfigs().containsKey(DISCOVERY_API_TTL_NAME));
        assertTrue(discoveryResult.getConfigs().containsKey(DISCOVERY_API_DISTRIBUTOR_TIMEOUT_NAME));
        assertTrue(discoveryResult.getConfigs().containsKey(DISCOVERY_API_REST_PROXY_URL_NAME));

        assertEquals(DISCOVERY_API_BOOTSTRAP_SERVERS_VALUE, discoveryResult.getConfigs().get(DISCOVERY_API_BOOTSTRAP_SERVERS_NAME));
        assertEquals(DISCOVERY_API_SCHEMA_REGISTRY_URL_VALUE, discoveryResult.getConfigs().get(DISCOVERY_API_SCHEMA_REGISTRY_URL_NAME));
        assertEquals(DISCOVERY_API_TTL_VALUE, discoveryResult.getConfigs().get(DISCOVERY_API_TTL_NAME));
        assertEquals(DISCOVERY_API_DISTRIBUTOR_TIMEOUT_VALUE, discoveryResult.getConfigs().get(DISCOVERY_API_DISTRIBUTOR_TIMEOUT_NAME));
        assertEquals(DISCOVERY_API_REST_PROXY_URL_VALUE, discoveryResult.getConfigs().get(DISCOVERY_API_REST_PROXY_URL_NAME));
    }

    @Before
    public void prepareTest() throws Exception {
        mockStatic(HttpClients.class);
        mockStatic(SslUtil.class);

        final ByteArrayInputStream bais = new ByteArrayInputStream(DISCOVERY_RESPONSE.getBytes(Charsets.UTF_8));

        when(HttpClients.custom()).thenReturn(mockedClientBuilder);
        when(SslUtil.createSslContext(createSslConfig())).thenReturn(mockedSslContext);
        when(mockedClientBuilder.build()).thenReturn(mockedHttpClient);

        whenNew(HttpGet.class).withArguments(anyString()).thenReturn(mockHttpGetRequest);

        when(mockedHttpResponse.getStatusLine()).thenReturn(mockedStatusLine);
        when(mockedHttpResponse.getEntity()).thenReturn(mockedEntity);
        when(mockedEntity.getContent()).thenReturn(bais);

        when(mockedHttpClient, "execute", any(HttpUriRequest.class)).thenReturn(mockedHttpResponse);
    }

    @Test
    public void valid_OK_Http() throws Exception {
        when(mockedStatusLine.getStatusCode()).thenReturn(HTTP_OK);

        DiscoveryConfig config = createDiscoveryConfig(DISCOVERY_API_URL_HTTP);
        DiscoveryFetcher discoveryFetcher = new DiscoveryFetcher(config);

        DiscoveryResult discoveryResult = discoveryFetcher.executeRequest(null);
        validateProviderProperties(discoveryResult);

        // Validate the number of calls to the mocked objects
        verify(mockedClientBuilder, times(1)).build();
        verify(mockedHttpClient, times(1)).execute(mockHttpGetRequest);
        verify(mockedHttpResponse, times(1)).getStatusLine();
        verify(mockedStatusLine, times(1)).getStatusCode();
        verify(mockedHttpResponse, times(1)).getEntity();
        verify(mockedEntity, times(1)).getContent();
    }

    @Test
    public void valid_Forbidden_Http() throws Exception {
        when(mockedStatusLine.getStatusCode()).thenReturn(HTTP_FORBIDDEN);

        DiscoveryConfig config = createDiscoveryConfig(DISCOVERY_API_URL_HTTP);
        DiscoveryFetcher discoveryFetcher = new DiscoveryFetcher(config);

        DiscoveryResult discoveryResult = discoveryFetcher.executeRequest(null);
        assertNull(discoveryResult);

        // Validate the number of calls to the mocked objects
        verify(mockedClientBuilder, times(1)).build();
        verify(mockedHttpClient, times(1)).execute(mockHttpGetRequest);
        verify(mockedHttpResponse, times(1)).getStatusLine();
        verify(mockedStatusLine, times(1)).getStatusCode();
        verify(mockedHttpResponse, times(0)).getEntity();
        verify(mockedEntity, times(0)).getContent();
    }

    @Test
    public void valid_NoContent_Http() throws Exception {
        when(mockedStatusLine.getStatusCode()).thenReturn(HTTP_NO_CONTENT);

        DiscoveryConfig config = createDiscoveryConfig(DISCOVERY_API_URL_HTTP);
        DiscoveryFetcher discoveryFetcher = new DiscoveryFetcher(config);

        DiscoveryResult discoveryResult = discoveryFetcher.executeRequest(null);
        assertNull(discoveryResult);

        // Validate the number of calls to the mocked objects
        verify(mockedClientBuilder, times(1)).build();
        verify(mockedHttpClient, times(1)).execute(mockHttpGetRequest);
        verify(mockedHttpResponse, times(1)).getStatusLine();
        verify(mockedStatusLine, times(1)).getStatusCode();
        verify(mockedHttpResponse, times(0)).getEntity();
        verify(mockedEntity, times(0)).getContent();
    }

    @Test
    public void valid_NotFound_Http() throws Exception {
        when(mockedStatusLine.getStatusCode()).thenReturn(HTTP_NOT_FOUND);

        DiscoveryConfig config = createDiscoveryConfig(DISCOVERY_API_URL_HTTP);
        DiscoveryFetcher discoveryFetcher = new DiscoveryFetcher(config);

        DiscoveryResult discoveryResult = discoveryFetcher.executeRequest(null);
        assertNull(discoveryResult);

        // Validate the number of calls to the mocked objects
        verify(mockedClientBuilder, times(1)).build();
        verify(mockedHttpClient, times(1)).execute(mockHttpGetRequest);
        verify(mockedHttpResponse, times(1)).getStatusLine();
        verify(mockedStatusLine, times(1)).getStatusCode();
        verify(mockedHttpResponse, times(0)).getEntity();
        verify(mockedEntity, times(0)).getContent();
    }

    @Test(expected = ConfigurationFetchFailedException.class)
    public void valid_RequestException_Http() throws Exception {
        when(mockedHttpClient, "execute", any(HttpUriRequest.class)).thenThrow(new NullPointerException("ExplicitFail"));
        when(mockedStatusLine.getStatusCode()).thenReturn(HTTP_NOT_FOUND);

        DiscoveryConfig config = createDiscoveryConfig(DISCOVERY_API_URL_HTTP);
        DiscoveryFetcher discoveryFetcher = new DiscoveryFetcher(config);

        discoveryFetcher.executeRequest(null);
    }


    @Test
    public void valid_OK_Https() throws Exception {
        when(mockedStatusLine.getStatusCode()).thenReturn(HTTP_OK);

        DiscoveryConfig config = createDiscoveryConfig(DISCOVERY_API_URL_HTTPS);
        DiscoveryFetcher discoveryFetcher = new DiscoveryFetcher(config);

        DiscoveryResult discoveryResult = discoveryFetcher.executeRequest(null);
        validateProviderProperties(discoveryResult);

        // Validate the number of calls to the mocked objects
        verify(mockedClientBuilder, times(1)).build();
        verify(mockedHttpClient, times(1)).execute(mockHttpGetRequest);
        verify(mockedHttpResponse, times(1)).getStatusLine();
        verify(mockedStatusLine, times(1)).getStatusCode();
        verify(mockedHttpResponse, times(1)).getEntity();
        verify(mockedEntity, times(1)).getContent();
    }

    @Test
    public void valid_NOK_Https() throws Exception {
        when(mockedStatusLine.getStatusCode()).thenReturn(HTTP_FORBIDDEN);

        DiscoveryConfig config = createDiscoveryConfig(DISCOVERY_API_URL_HTTPS);
        DiscoveryFetcher discoveryFetcher = new DiscoveryFetcher(config);

        DiscoveryResult discoveryResult = discoveryFetcher.executeRequest(null);
        assertNull(discoveryResult);

        // Validate the number of calls to the mocked objects
        verify(mockedClientBuilder, times(1)).build();
        verify(mockedHttpClient, times(1)).execute(mockHttpGetRequest);
        verify(mockedHttpResponse, times(1)).getStatusLine();
        verify(mockedStatusLine, times(1)).getStatusCode();
        verify(mockedHttpResponse, times(0)).getEntity();
        verify(mockedEntity, times(0)).getContent();
    }

    @Test(expected = DiscoveryException.class)
    public void valid_InvalidURL() {
        DiscoveryConfig config = createDiscoveryConfig(DISCOVERY_API_URL_FAULT);
        new DiscoveryFetcher(config);
    }


    @Test(expected = DiscoveryException.class)
    public void closed_executeRequest() throws Exception {
        when(mockedStatusLine.getStatusCode()).thenReturn(HTTP_OK);

        DiscoveryConfig config = createDiscoveryConfig(DISCOVERY_API_URL_HTTP);
        DiscoveryFetcher fetcher = new DiscoveryFetcher(config);
        fetcher.close();
        fetcher.executeRequest(null);
    }
}
