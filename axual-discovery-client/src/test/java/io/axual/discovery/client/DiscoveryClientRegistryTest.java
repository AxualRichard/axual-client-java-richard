package io.axual.discovery.client;

/*-
 * ========================LICENSE_START=================================
 * axual-discovery-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import io.axual.common.config.PasswordConfig;
import io.axual.common.config.SslConfig;
import io.axual.discovery.client.fetcher.DiscoveryLoader;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({DiscoveryClientRegistry.class, DiscoveryLoader.class})
@PowerMockIgnore("jdk.internal.reflect.*")
public class DiscoveryClientRegistryTest {
    private static final String APPLICATIONID = "io.axual.unit-test";
    private static final String APPLICATIONVERSION = "1.0.0";

    private static final String DISCOVERYAPIENDPOINT = "https://local:9043/somewhere/";

    private static final PasswordConfig SSLKEYPASSWORD_VALID = new PasswordConfig("notsecret");
    private static final String SSLKEYSTORELOCATION_VALID = "ssl/axual.client.keystore.jks";
    private static final PasswordConfig SSLKEYSTOREPASSWORD_VALID = new PasswordConfig("notsecret");
    private static final String SSLTRUSTSTORELOCATION_VALID = "ssl/axual.client.truststore.jks";
    private static final PasswordConfig SSLTRUSTSTOREPASSWORD_VALID = new PasswordConfig("notsecret");

    @Mock
    DiscoveryClient discoveryClient;

    @Mock
    DiscoveryLoader discoveryLoader;

    private DiscoveryConfig createDiscoveryConfig() {
        return DiscoveryConfig.newBuilder()
                .setApplicationId(APPLICATIONID)
                .setApplicationVersion(APPLICATIONVERSION)
                .setEndpoint(DISCOVERYAPIENDPOINT)
                .setSslConfig(SslConfig.newBuilder()
                        .setKeyPassword(SSLKEYPASSWORD_VALID)
                        .setKeystoreLocation(SSLKEYSTORELOCATION_VALID)
                        .setKeystorePassword(SSLKEYSTOREPASSWORD_VALID)
                        .setTruststoreLocation(SSLTRUSTSTORELOCATION_VALID)
                        .setTruststorePassword(SSLTRUSTSTOREPASSWORD_VALID)
                        .build())
                .build();
    }

    @Before
    public void setup(){
        DiscoveryClientRegistry.cleanUp();
    }

    @Test
    public void valid_register_client_proxy() throws Exception {
        PowerMockito.spy(DiscoveryClientRegistry.class);
        PowerMockito.whenNew(DiscoveryLoader.class).withAnyArguments().thenReturn(discoveryLoader);
        when(discoveryLoader.getDiscoveryResult()).thenReturn(new DiscoveryResult());
        DiscoveryClientRegistry.register(createDiscoveryConfig(), discoveryClient);
        verify(discoveryClient, times(1)).onDiscoveryPropertiesChange(any(DiscoveryResult.class));
    }

    @Test
    public void valid_unregister_client_proxy() throws Exception {
        PowerMockito.spy(DiscoveryClientRegistry.class);
        PowerMockito.whenNew(DiscoveryLoader.class).withAnyArguments().thenReturn(discoveryLoader);
        when(discoveryLoader.getDiscoveryResult()).thenReturn(new DiscoveryResult());
        DiscoveryConfig config = createDiscoveryConfig();
        DiscoveryClientRegistry.register(config, discoveryClient);
        DiscoveryClientRegistry.unregister(config,discoveryClient);
        verify(discoveryLoader,atLeastOnce()).close();
    }

    @Test
    public void valid_cleanup() throws Exception {
        PowerMockito.spy(DiscoveryClientRegistry.class);
        PowerMockito.whenNew(DiscoveryLoader.class).withAnyArguments().thenReturn(discoveryLoader);
        when(discoveryLoader.getDiscoveryResult()).thenReturn(new DiscoveryResult());
        DiscoveryConfig config = createDiscoveryConfig();
        DiscoveryClientRegistry.register(config, discoveryClient);
        DiscoveryClientRegistry.cleanUp();
        verify(discoveryLoader,atLeastOnce()).close();
    }

}
