package io.axual.discovery.client.exception;

/*-
 * ========================LICENSE_START=================================
 * axual-discovery-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ConfigurationFetchFailedExceptionTest {
    protected static final String URL = "http://unittest:443/shouldfail";
    protected static final Exception CAUSE = new RuntimeException("Unit Test Exception");

    @Test
    public void construction() {
        final String expectedMessage = String.format(ConfigurationFetchFailedException.MESSAGE_FORMAT, URL);
        ConfigurationFetchFailedException configurationFetchFailedException = new ConfigurationFetchFailedException(URL, CAUSE);
        assertEquals(expectedMessage, configurationFetchFailedException.getMessage());
        assertEquals(CAUSE, configurationFetchFailedException.getCause());
    }

}
