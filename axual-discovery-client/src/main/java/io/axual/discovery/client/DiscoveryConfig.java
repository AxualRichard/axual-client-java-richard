package io.axual.discovery.client;

/*-
 * ========================LICENSE_START=================================
 * axual-discovery-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

import io.axual.common.config.SslConfig;
import io.axual.common.tools.StringUtil;

public class DiscoveryConfig {
    private final String applicationId;
    private final String applicationVersion;
    private final String endpoint;
    private final Map<String, String> parameters;
    private final SslConfig sslConfig;

    private DiscoveryConfig(Builder builder) {
        this.applicationId = builder.applicationId;
        this.applicationVersion = builder.applicationVersion;
        this.endpoint = builder.endpoint;
        this.parameters = new TreeMap<>();
        if (builder.parameters != null) {
            parameters.putAll(builder.parameters);
        }
        this.sslConfig = builder.sslConfig;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DiscoveryConfig other = (DiscoveryConfig) o;

        if (!Objects.equals(applicationId, other.applicationId)) {
            return false;
        }
        if (!Objects.equals(applicationVersion, other.applicationVersion)) {
            return false;
        }
        if (!Objects.equals(endpoint, other.endpoint)) {
            return false;
        }
        if (!Objects.equals(parameters, other.parameters)) {
            return false;
        }
        return Objects.equals(sslConfig, other.sslConfig);
    }

    @Override
    public int hashCode() {
        int result = applicationId != null ? applicationId.hashCode() : 0;
        result = 31 * result + (applicationVersion != null ? applicationVersion.hashCode() : 0);
        result = 31 * result + (endpoint != null ? endpoint.hashCode() : 0);
        result = 31 * result + (parameters != null ? parameters.hashCode() : 0);
        result = 31 * result + (sslConfig != null ? sslConfig.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return StringUtil.join(",",
                applicationId,
                applicationVersion,
                endpoint,
                parameters.toString(),
                sslConfig != null ? sslConfig.toString() : "null"
        );
    }

    public String getApplicationId() {
        return applicationId;
    }

    public String getApplicationVersion() {
        return applicationVersion;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public Map<String, String> getParameters() {
        return parameters;
    }

    public SslConfig getSslConfig() {
        return sslConfig;
    }

    public static class Builder {
        private String applicationId;
        private String applicationVersion = "unknown";
        private String endpoint;
        private Map<String, String> parameters;
        private SslConfig sslConfig;

        public String getApplicationId() {
            return applicationId;
        }

        public Builder setApplicationId(String applicationId) {
            this.applicationId = applicationId;
            return this;
        }

        public String getApplicationVersion() {
            return applicationVersion;
        }

        public Builder setApplicationVersion(String applicationVersion) {
            this.applicationVersion = applicationVersion;
            return this;
        }

        public String getEndpoint() {
            return endpoint;
        }

        public Builder setEndpoint(String endpoint) {
            this.endpoint = endpoint;
            return this;
        }

        public Map<String, String> getParameters() {
            return parameters;
        }

        public Builder setParameters(Map<String, String> parameters) {
            this.parameters = parameters;
            return this;
        }

        public SslConfig getSslConfig() {
            return sslConfig;
        }

        public Builder setSslConfig(SslConfig sslConfig) {
            this.sslConfig = sslConfig;
            return this;
        }

        public DiscoveryConfig build() {
            return new DiscoveryConfig(this);
        }
    }
}
