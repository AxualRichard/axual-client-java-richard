package io.axual.discovery.client.tools;

/*-
 * ========================LICENSE_START=================================
 * axual-discovery-client
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import java.util.Properties;

public class VersionUtil {
    public static String getProjectVersion(Class clazz) {
        String version = clazz.getPackage().getImplementationVersion();
        if (version == null) {
            Properties prop = new Properties();
            try {
                prop.load(clazz.getClassLoader().getResourceAsStream("/META-INF/MANIFEST.MF"));
                version = prop.getProperty("Implementation-Version", "unknown");
            } catch (Exception e) {
                version = "unknown";
            }
        }
        return version;
    }
}
