package io.axual.platform.test.core;

/*-
 * ========================LICENSE_START=================================
 * axual-platform-test-core
 * %%
 * Copyright (C) 2020 - 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.avro.AvroRuntimeException;
import org.apache.avro.Schema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class SchemaNormalizer {

    private static final Logger log = LoggerFactory.getLogger(SchemaNormalizer.class);
    private static final String NORMALIZED_DOC = "";

    private SchemaNormalizer() {
        // Hide public constructor
    }

    /**
     * Normalize a schema.
     *
     * @param schema a schema.
     * @return the same functional schema in normalized form.
     */
    static Schema normalizeSchema(Schema schema) {
        log.debug("normalizeSchema(schema)");
        return normalizeSchema(schema, new HashMap<>());
    }

    /**
     * Normalize a schema.
     *
     * @param schema            a schema.
     * @param alreadyNormalized a Map indicating if the fields in the schema were already normalized.
     * @return the same schema functionally, in normalized form.
     */
    private static Schema normalizeSchema(Schema schema, Map<String, Boolean> alreadyNormalized) {
        log.debug("normalizeSchema({}, {}, {})", schema.getName(), schema.getType(), alreadyNormalized);

        // if it's a nested type RECORD, check if it was already normalized and update our administration
        if (schema.getType().equals(Schema.Type.RECORD)) {
            String key = createKey(schema);
            if (alreadyNormalized.containsKey(key)) {
                // don't normalize again
                return schema;
            } else {
                log.debug("Adding key '{}' to map", key);
                alreadyNormalized.put(key, true);
            }
        }

        final Schema result;
        switch (schema.getType()) {
            case RECORD:
                result = Schema.createRecord(schema.getName(), NORMALIZED_DOC, schema.getNamespace(), false, normalizeFields(schema.getFields(), alreadyNormalized));
                break;
            case ENUM:
                result = Schema.createEnum(schema.getName(), NORMALIZED_DOC, schema.getNamespace(), schema.getEnumSymbols());
                break;
            case ARRAY:
                result = Schema.createArray(normalizeSchema(schema.getElementType(), alreadyNormalized));
                break;
            case FIXED:
                result = Schema.createFixed(schema.getName(), NORMALIZED_DOC, schema.getNamespace(), schema.getFixedSize());
                break;
            case UNION:
                result = Schema.createUnion(normalizeSchemasList(schema.getTypes(), alreadyNormalized));
                break;
            case MAP:
                result = Schema.createMap(normalizeSchema(schema.getValueType()));
                break;
            default:
                log.debug("case default: {}, {}", schema.getName(), schema.getType());
                result = Schema.create(schema.getType());
        }
        schema.getObjectProps().forEach(result::addProp);
        return result;
    }

    private static List<Schema> normalizeSchemasList(List<Schema> schemas, Map<String, Boolean> alreadyNormalized) {
        if (schemas == null) {
            log.debug("normalizeSchemasList( null schemas, {} )", alreadyNormalized);
            return Collections.emptyList();
        }
        log.debug("normalizeSchemasList( {} schemas, {} )", schemas.size(), alreadyNormalized);

        final List<Schema> result = new ArrayList<>(schemas.size());
        for (Schema schema : schemas) {
            result.add(normalizeSchema(schema, alreadyNormalized));
        }
        return result;
    }

    private static Schema.Field normalizeField(Schema.Field field, Map<String, Boolean> alreadyNormalized) {
        final Schema.Field result = new Schema.Field(field.name(), normalizeSchema(field.schema(), alreadyNormalized), NORMALIZED_DOC, field.defaultVal(), field.order());
        field.getObjectProps().forEach(result::addProp);
        return result;
    }

    private static List<Schema.Field> normalizeFields(List<Schema.Field> fields, Map<String, Boolean> alreadyNormalized) {
        List<Schema.Field> result = new ArrayList<>(fields.size());
        for (Schema.Field field : fields) {
            result.add(normalizeField(field, alreadyNormalized));
        }
        return result;
    }

    /**
     * Create a key for the internal map.
     *
     * @param schema a schema.
     * @return the schema namespace (if any), concatenated with the schema name.
     */
    private static String createKey(Schema schema) {
        String namespace = "";
        try {
            namespace = schema.getNamespace() == null ? "" : schema.getNamespace();
        } catch (AvroRuntimeException e) {
            // not namespaced, leave as is
        }
        return namespace + ":" + schema.getName();
    }
}
