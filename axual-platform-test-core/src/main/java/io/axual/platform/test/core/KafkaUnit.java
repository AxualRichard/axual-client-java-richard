package io.axual.platform.test.core;

/*-
 * ========================LICENSE_START=================================
 * axual-platform-test-core
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

/**
 * Copyright (C) 2014 Christopher Batey
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License") you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

import java.util.concurrent.atomic.AtomicBoolean;
import org.apache.commons.io.FileUtils;
import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.CreateTopicsResult;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.common.config.SslConfigs;
import org.apache.kafka.common.config.types.Password;
import org.apache.kafka.common.utils.Time;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;

import io.axual.common.annotation.InterfaceStability;
import io.axual.common.config.PasswordConfig;
import kafka.server.KafkaConfig;
import kafka.server.KafkaServer;
import scala.Option;

@InterfaceStability.Evolving
public class KafkaUnit {
    private static final Logger LOG = LoggerFactory.getLogger(KafkaUnit.class);
    private static final String LOCALHOST = "localhost";
    private static final String BIND_ALL = "0.0.0.0";

    private KafkaServer broker;

    private ZookeeperUnit zookeeper;
    private final String zookeeperString;
    private final String brokerString;
    private final String bindAddress;
    private final String advertisedAddress;
    private final String zkBindAddress;
    private int zkPort;
    private int brokerPort;
    private File logDir;
    private Map<String, Object> kafkaBrokerConfig = new HashMap<>();
    private static AtomicInteger brokerIdCounter = new AtomicInteger(0);
    private int brokerId;
    private final AtomicBoolean isRunning = new AtomicBoolean(false);

    public KafkaUnit() {
        this(null, null, getEphemeralPort(), getEphemeralPort());
    }

    public KafkaUnit(String bindAddress, String advertisedAddress, int zkPort, int brokerPort) {
        this.bindAddress = bindAddress != null ? bindAddress : BIND_ALL;
        this.advertisedAddress = advertisedAddress != null ? advertisedAddress : LOCALHOST;
        this.zkBindAddress = getHostIPv4Address(this.bindAddress);
        this.zkPort = zkPort;
        this.brokerPort = brokerPort;
        this.zookeeperString = this.zkBindAddress + ":" + zkPort;
        this.brokerString = String.format("%s:%d", this.advertisedAddress, this.brokerPort);
        this.brokerId = brokerIdCounter.incrementAndGet();
    }

    private static String getHostIPv4Address(String name) {
        if (BIND_ALL.equals(name)) {
            name = LOCALHOST;
        }
        try {
            return InetAddress.getByName(name).getHostAddress();
        } catch (UnknownHostException e) {
            throw new IllegalStateException("Cannot find '" + name + "'", e);
        }
    }

    private static synchronized int getEphemeralPort() {
        try {
            try (ServerSocket socket = new ServerSocket(0)) {
                return socket.getLocalPort();
            }
        } catch (IOException e) {
            throw new RuntimeException("Could not allocate free port for KafkaUnit");
        }
    }

    public void startup() {
        zookeeper = new ZookeeperUnit(zkPort);
        zookeeper.startup();

        LOG.info("Starting Kafka on port {}", brokerPort);
        try {
            logDir = Files.createTempDirectory("kafka").toFile();
        } catch (IOException e) {
            throw new RuntimeException("Unable to start Kafka", e);
        }
        kafkaBrokerConfig.put("zookeeper.connect", zookeeperString);
        kafkaBrokerConfig.put("broker.id", Integer.toString(brokerId));
        kafkaBrokerConfig.put("host.name", bindAddress);
        kafkaBrokerConfig.put("port", "" + brokerPort);
        kafkaBrokerConfig.put("log.dir", logDir.getAbsolutePath());
        kafkaBrokerConfig.put("log.flush.interval.messages", String.valueOf(1));
        kafkaBrokerConfig.put("offsets.topic.replication.factor", "1");
        kafkaBrokerConfig.put("min.insync.replicas", "1");
        kafkaBrokerConfig.put("transaction.state.log.min.isr", "1");
        kafkaBrokerConfig.put("transaction.state.log.replication.factor", "1");
        kafkaBrokerConfig.put("auto.create.topics.enable", "false");

        broker = new KafkaServer(new KafkaConfig(kafkaBrokerConfig), Time.SYSTEM, Option.apply("axual-"), false);
        broker.startup();
        isRunning.set(true);
    }

    public String getBootstrapServer() {
        return brokerString;
    }

    public int getZkPort() {
        return zkPort;
    }

    public int getBrokerPort() {
        return brokerPort;
    }

    public void createTopic(String topicName) {
        createTopic(topicName, 1);
    }

    private static void copy(final Map<String, Object> source, final Map<String, Object> target, String key, Object defaultValue) {
        target.put(key, source.getOrDefault(key, defaultValue));
    }

    public void createTopic(String topicName, Integer numPartitions) {
        Map<String, Object> adminClientProperties = new HashMap<>();
        adminClientProperties.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaBrokerConfig.get("advertised.listeners"));
        copy(kafkaBrokerConfig, adminClientProperties, CommonClientConfigs.SECURITY_PROTOCOL_CONFIG, "SSL");
        copy(kafkaBrokerConfig, adminClientProperties, SslConfigs.SSL_ENDPOINT_IDENTIFICATION_ALGORITHM_CONFIG, "");
        copy(kafkaBrokerConfig, adminClientProperties, SslConfigs.SSL_ENABLED_PROTOCOLS_CONFIG, null);
        copy(kafkaBrokerConfig, adminClientProperties, SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG, null);
        copy(kafkaBrokerConfig, adminClientProperties, SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG, null);
        copy(kafkaBrokerConfig, adminClientProperties, SslConfigs.SSL_KEY_PASSWORD_CONFIG, null);
        copy(kafkaBrokerConfig, adminClientProperties, SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, null);
        copy(kafkaBrokerConfig, adminClientProperties, SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG, null);

        try (AdminClient adminClient = AdminClient.create(adminClientProperties)) {
            NewTopic newTopic = new NewTopic(topicName, numPartitions, (short) 1);
            LOG.info("Executing: CreateTopic {} partitions {}", topicName, numPartitions);
            CreateTopicsResult result = adminClient.createTopics(Collections.singleton(newTopic));
            try {
                result.all().get();
                LOG.info("CreateTopic {} partitions {} done", topicName, numPartitions);
            } catch (InterruptedException e) {
                LOG.warn("CreateTopic {} partitions {} failed", topicName, numPartitions, e);
                Thread.currentThread().interrupt();
            } catch (ExecutionException e) {
                LOG.warn("CreateTopic {} partitions {} failed", topicName, numPartitions, e);
            }
        }
    }

    public void shutdown() {
        if (broker != null) {
            LOG.info("Shutting down Kafka on port {}", brokerPort);
            broker.shutdown();
            broker.awaitShutdown();
            try {
                FileUtils.cleanDirectory(logDir);
            } catch (IOException e) {
                LOG.warn("Could not clean the directory {}", logDir.getAbsolutePath());
            }
            try {
                Files.delete(logDir.toPath());
            } catch (IOException e) {
                LOG.error("Error deleting the directory {}", logDir.getAbsolutePath());
            }
        }
        if (zookeeper != null) {
            zookeeper.shutdown();
        }
        isRunning.set(false);
    }

    /**
     * Set custom broker configuration. See avaliable config keys in the kafka documentation:
     * http://kafka.apache.org/documentation.html#brokerconfigs
     */
    public final void setKafkaBrokerConfig(String configKey, Object configValue) {
        if (configValue instanceof PasswordConfig) {
            configValue = new Password(((PasswordConfig) configValue).getValue());
        }
        kafkaBrokerConfig.put(configKey, configValue);
    }

    public boolean isRunning(){
        return zookeeper.isRunning() && isRunning.get();
    }
}
