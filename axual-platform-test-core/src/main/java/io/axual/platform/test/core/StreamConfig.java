package io.axual.platform.test.core;

/*-
 * ========================LICENSE_START=================================
 * axual-platform-test-core
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.avro.Schema;

import io.axual.common.annotation.InterfaceStability;

@InterfaceStability.Evolving
public class StreamConfig {
    private String name;
    private String environment;
    private Schema keySchema;
    private Schema valueSchema;
    private Integer partitions;

    public String getName() {
        return name;
    }

    public StreamConfig setName(String name) {
        this.name = name;
        return this;
    }

    public String getEnvironment() {
        return environment;
    }

    public StreamConfig setEnvironment(String environment) {
        this.environment = environment;
        return this;
    }

    public Schema getKeySchema() {
        return keySchema;
    }

    public StreamConfig setKeySchema(Schema keySchema) {
        this.keySchema = keySchema;
        return this;
    }

    public Schema getValueSchema() {
        return valueSchema;
    }

    public StreamConfig setValueSchema(Schema valueSchema) {
        this.valueSchema = valueSchema;
        return this;
    }

    public Integer getPartitions() {
        return partitions;
    }

    public StreamConfig setPartitions(Integer partitions) {
        this.partitions = partitions;
        return this;
    }
}
