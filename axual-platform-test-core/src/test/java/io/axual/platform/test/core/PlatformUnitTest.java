package io.axual.platform.test.core;

/*-
 * ========================LICENSE_START=================================
 * axual-platform-test-core
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.powermock.api.mockito.PowerMockito.whenNew;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.net.ssl.*")
@PrepareForTest({
        ClusterUnit.class,
        InstanceUnit.class,
        PlatformUnit.class
})
public class PlatformUnitTest {

    private static final ClusterUnit mockClusterUnit = PowerMockito.mock(ClusterUnit.class);
    private static final InstanceUnit mockInstanceUnit = PowerMockito.mock(InstanceUnit.class);

    @Before
    public void init() throws Exception {
        initMocks(this);

        whenNew(ClusterUnit.class).withAnyArguments().thenReturn(mockClusterUnit);
        whenNew(InstanceUnit.class).withAnyArguments().thenReturn(mockInstanceUnit);
    }

    @Test
    public void initByNumberOfClusters() {
        final int CLUSTERS = 1;
        try (final PlatformUnit target = new PlatformUnit(CLUSTERS, true)) {
            assertEquals(CLUSTERS, target.getClusterCount());
            assertEquals(mockClusterUnit, target.getCluster(0));
            assertEquals(mockInstanceUnit, target.getInstance());
        }
    }

    @Test
    public void testAddStream() {
        try (final PlatformUnit target = new PlatformUnit(1, true)) {

            StreamConfig config = new StreamConfig();
            target.addStream(config);

            verify(mockInstanceUnit, times(1)).addStream(config);
        }
    }

    @Test
    public void testStart() {
        final int CLUSTERS = 1;
        try (final PlatformUnit target = new PlatformUnit(CLUSTERS, true)) {

            target.start();

            verify(mockInstanceUnit, times(1)).start();
            verify(mockClusterUnit, times(1)).start();
        }
    }

    @Test(expected = IllegalStateException.class)
    public void testStartNoClusters()  {
        try (final PlatformUnit target = new PlatformUnit(0, true)) {
            target.start();
        }
    }
}
