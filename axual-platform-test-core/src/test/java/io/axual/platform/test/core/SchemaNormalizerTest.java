package io.axual.platform.test.core;

/*-
 * ========================LICENSE_START=================================
 * axual-platform-test-core
 * %%
 * Copyright (C) 2020 - 2021 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.avro.Schema;
import org.apache.avro.SchemaParseException;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Contains code snippets from the Schema Registry v4.1.3
 */
public class SchemaNormalizerTest {
    private static final Logger LOG = LoggerFactory.getLogger(SchemaNormalizerTest.class);

    /**
     * Convert a schema string into a schema object and a canonical schema string.
     *
     * @return A schema object and a canonical representation of the schema string. Return null if
     * there is any parsing error.
     */
    public static AvroSchema parseSchema(String schemaString) {
        return parseSchema(schemaString, true);
    }

    public static AvroSchema parseSchema(String schemaString, boolean normalize) {
        try {
            Schema.Parser parser1 = new Schema.Parser();
            Schema schema = parser1.parse(schemaString);
            if (normalize) {
                schema = SchemaNormalizer.normalizeSchema(schema);
            }
            return new AvroSchema(schema, schema.toString());
        } catch (SchemaParseException e) {
            return null;
        }
    }

    @Test
    public void parseSchema_SchemasWithOptionalAttributesInRoot_Equal() {
        // prepare
        String schemaStr = "{\n" +
                "  \"type\": \"record\",\n" +
                "  \"name\": \"schemaName\",\n" +
                "  \"namespace\": \"io.axual.client.example.schema\",\n" +
                "  \"fields\": []\n" +
                "}";
        String schemaWithOptionalStr = "{\n" +
                "  \"type\": \"record\",\n" +
                "  \"name\": \"schemaName\",\n" +
                "  \"doc\": \"some description\",\n" + // optional attribute
                "  \"namespace\": \"io.axual.client.example.schema\",\n" +
                "  \"fields\": []\n" +
                "}";

        // act
        AvroSchema schema = parseSchema(schemaStr);
        AvroSchema schemaWithOptional = parseSchema(schemaWithOptionalStr);

        // assert
        assertEquals(schema.schemaObj, schemaWithOptional.schemaObj);
        assertEquals(schema.canonicalString, schemaWithOptional.canonicalString);
    }

    @Test
    public void parseSchema_SchemasWithDifferenceAttributesOrderInRoot_Equal() {
        // prepare
        String schemaStr = "{\n" +
                "  \"type\": \"record\",\n" +
                "  \"name\": \"schemaName\",\n" +
                "  \"doc\": \"some description\",\n" +
                "  \"namespace\": \"io.axual.client.example.schema\",\n" +
                "  \"fields\": []\n" +
                "}";
        String schemaWithDifferenceAttributesOrderStr = "{\n" + // reverse order of keys
                "  \"fields\": [],\n" +
                "  \"namespace\": \"io.axual.client.example.schema\",\n" +
                "  \"doc\": \"some description\",\n" +
                "  \"name\": \"schemaName\",\n" +
                "  \"type\": \"record\"\n" +
                "}";

        // act
        AvroSchema schema = parseSchema(schemaStr);
        AvroSchema schemaWithDifferenceAttributesOrder = parseSchema(schemaWithDifferenceAttributesOrderStr);

        // assert
        assertEquals(schema.schemaObj, schemaWithDifferenceAttributesOrder.schemaObj);
        assertEquals(schema.canonicalString, schemaWithDifferenceAttributesOrder.canonicalString);
    }

    @Test
    public void parseSchema_SchemasWithOptionalAttributesInField_Equal() {
        // prepare
        String schemaStr = "{\n" +
                "  \"type\": \"record\",\n" +
                "  \"name\": \"schemaName\",\n" +
                "  \"namespace\": \"io.axual.client.example.schema\",\n" +
                "  \"fields\": [\n" +
                "    {\n" +
                "      \"name\": \"timestamp\",\n" +
                "      \"type\": \"long\"\n" + // without 'doc' attribute
                "    }]\n" +
                "}";

        String schemaWithOptionalAttributesInFieldStr = "{\n" +
                "  \"type\": \"record\",\n" +
                "  \"name\": \"schemaName\",\n" +
                "  \"namespace\": \"io.axual.client.example.schema\",\n" +
                "  \"fields\": [\n" +
                "    {\n" +
                "      \"name\": \"timestamp\",\n" +
                "      \"type\": \"long\",\n" +
                "      \"doc\": \"Timestamp of the event\"\n" + // added optional field
                "    }]\n" +
                "}";

        // act
        AvroSchema schema = parseSchema(schemaStr);
        AvroSchema schemaWithOptionalAttributesInField = parseSchema(schemaWithOptionalAttributesInFieldStr);

        // assert
        assertEquals(schema.schemaObj, schemaWithOptionalAttributesInField.schemaObj);
        assertEquals(schema.canonicalString, schemaWithOptionalAttributesInField.canonicalString);
    }

    @Test
    public void parseSchema_SchemasWithDifferenceAttributesOrderInField_Equal() {
        // prepare
        String schemaStr = "{\n" +
                "  \"type\": \"record\",\n" +
                "  \"name\": \"schemaName\",\n" +
                "  \"namespace\": \"io.axual.client.example.schema\",\n" +
                "  \"fields\": [\n" +
                "    {\n" +
                "      \"name\": \"message\",\n" + // `name` 1st `type` 2nd
                "      \"type\": \"string\"\n" +
                "    }]\n" +
                "}";

        String schemasWithDifferenceAttributesOrderInFieldStr = "{\n" +
                "  \"type\": \"record\",\n" +
                "  \"name\": \"schemaName\",\n" +
                "  \"namespace\": \"io.axual.client.example.schema\",\n" +
                "  \"fields\": [\n" +
                "    {\n" +
                "      \"type\": \"string\",\n" + // `type` 1st `name` 2nd
                "      \"name\": \"message\"\n" +
                "    }]\n" +
                "}";

        // Act
        AvroSchema schema = parseSchema(schemaStr);
        AvroSchema schemasWithDifferenceAttributesOrderInField = parseSchema(schemasWithDifferenceAttributesOrderInFieldStr);

        // Assert
        assertEquals(schema.schemaObj, schemasWithDifferenceAttributesOrderInField.schemaObj);
        assertEquals(schema.canonicalString, schemasWithDifferenceAttributesOrderInField.canonicalString);
    }

    @Test
    public void parseSchema_NestedSchemasWithDifferenceAttributesOrderInField_Equal() {
        String nestedSchemaStr =
                "{\n" +
                        "  \"type\": \"record\",\n" +
                        "  \"name\": \"Schema\",\n" +
                        "  \"namespace\": \"io.axual.client.example.schema\",\n" +
                        "  \"fields\": [\n" +
                        "    {\n" +
                        "      \"name\": \"name\",\n" +
                        "      \"type\": \"string\"\n" +
                        "    },\n" +
                        "    {\n" +
                        "      \"name\": \"innerSchema\",\n" +
                        "      \"type\": {\n" +
                        "        \"type\": \"record\",\n" +
                        "        \"name\": \"NestedSchema\",\n" +
                        "        \"namespace\": \"io.axual.client.example.schema\",\n" +
                        "        \"fields\": [\n" +
                        "          {\n" +
                        "            \"name\": \"innerName\",\n" + // `name` 1st `type` 2nd
                        "            \"type\": \"string\"\n" +
                        "          }\n" +
                        "        ]\n" +
                        "      }\n" +
                        "    }\n" +
                        "  ]\n" +
                        "}";

        String schemaWithDifferenceAttributesOrderInNestedSchemaStr =
                "{\n" +
                        "  \"type\": \"record\",\n" +
                        "  \"name\": \"Schema\",\n" +
                        "  \"namespace\": \"io.axual.client.example.schema\",\n" +
                        "  \"fields\": [\n" +
                        "    {\n" +
                        "      \"name\": \"name\",\n" +
                        "      \"type\": \"string\"\n" +
                        "    },\n" +
                        "    {\n" +
                        "      \"name\": \"innerSchema\",\n" +
                        "      \"type\": {\n" +
                        "        \"type\": \"record\",\n" +
                        "        \"name\": \"NestedSchema\",\n" +
                        "        \"namespace\": \"io.axual.client.example.schema\",\n" +
                        "        \"fields\": [\n" +
                        "          {\n" +
                        "            \"type\": \"string\",\n" + // `type` 1st `name` 2nd
                        "            \"name\": \"innerName\"\n" +
                        "          }\n" +
                        "        ]\n" +
                        "      }\n" +
                        "    }\n" +
                        "  ]\n" +
                        "}";


        // act
        AvroSchema schema = parseSchema(nestedSchemaStr);
        AvroSchema schemaWithDifferenceAttributesOrderInNestedSchema = parseSchema(schemaWithDifferenceAttributesOrderInNestedSchemaStr);

        // assert
        assertEquals(schema.schemaObj, schemaWithDifferenceAttributesOrderInNestedSchema.schemaObj);
        assertEquals(schema.canonicalString, schemaWithDifferenceAttributesOrderInNestedSchema.canonicalString);
    }

    @Test
    public void parseSchema_NestedSchemasOfSameType() throws Exception {
        final List<String> schemas = new ArrayList<>();
        final List<AvroSchema> normalizedSchemas = new ArrayList<>();
        // given a schema that has a field referencing it's own type
        schemas.add(getSchemaFromResource("schemas/schema-with-same-nested-schema.avsc"));
        schemas.add(getSchemaFromResource("schemas/schema-with-same-nested-schema2.avsc"));
        schemas.add(getSchemaFromResource("schemas/schema-self-ref-array-item.avsc"));
        schemas.add(getSchemaFromResource("schemas/schema-self-ref-union.avsc"));

        schemas.forEach(schema -> {
            AvroSchema avroSchema = parseSchema(schema);
            // the schema should be parsed without infinite recursion
            assertNotNull("We should return a parsed schema", avroSchema);
            normalizedSchemas.add(avroSchema);
        });

        // and the parsed schema should still be the same
        assertEquals(normalizedSchemas.get(0).schemaObj, normalizedSchemas.get(1).schemaObj);
        assertEquals(normalizedSchemas.get(0).canonicalString, normalizedSchemas.get(1).canonicalString);
    }

    @Test
    public void parseSchema_unionOfNullAndSelf() throws Exception {
        // given a schema containing a union of null and it's own type
        final String schemaWithNullUnion = getSchemaFromResource("schemas/schema-with-null-union.avsc");
        assertNotNull(schemaWithNullUnion);

        // the schema should be parsed with a non-null result
        final AvroSchema parsed = parseSchema(schemaWithNullUnion);

        assertNotNull(parsed);
    }

    @Test
    public void parseSchema_withJavaType() throws Exception {
        // given a schema containing a java type and it's own type
        final String schemaWithJavaType = getSchemaFromResource("schemas/schema-with-java-type.avsc");
        assertNotNull(schemaWithJavaType);

        // the schema should be parsed with a non-null result
        final AvroSchema parsed = parseSchema(schemaWithJavaType);

        assertNotNull("A schema containing a Java AVRO type and a selfreference should be normalized", parsed);
    }

    @Test
    public void parseSchema_withLogicalType() throws Exception {
        // given a schema containing a rabo custom date type with logicalType
        final String schemaWithCustomType = getSchemaFromResource("schemas/schema-with-logicaltype.avsc");
        assertNotNull(schemaWithCustomType);

        // the schema should be parsed with a non-null result
        final AvroSchema parsed = parseSchema(schemaWithCustomType);

        assertNotNull("A schema containing a logical type should be normalized", parsed);
    }

    @Test
    public void parseSchema_withNestedEnumAndDefault() throws Exception {
        // given a schema containing a rabo custom date type with logicalType
        final String schemaWithCustomType = getSchemaFromResource("schemas/schema-deeply-nested-enum-default.avsc");
        assertNotNull(schemaWithCustomType);

        // the schema should be parsed with a non-null result
        final AvroSchema parsed = parseSchema(schemaWithCustomType);

        assertNotNull("A schema with a deeply nested enum and null default should be normalized", parsed);
    }

    private String getSchemaFromResource(String resourcePath) throws IOException, URISyntaxException {
        ClassLoader classLoader = getClass().getClassLoader();
        URL resourceURL = classLoader.getResource(resourcePath);
        File file = new File(resourceURL.toURI());
        return FileUtils.readFileToString(file, "UTF-8");
    }

    static class AvroSchema {

        public final Schema schemaObj;
        public final String canonicalString;

        public AvroSchema(Schema schemaObj, String canonicalString) {
            this.schemaObj = schemaObj;
            this.canonicalString = canonicalString;
        }
    }
}
