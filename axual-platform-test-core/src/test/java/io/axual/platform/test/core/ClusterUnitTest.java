package io.axual.platform.test.core;

/*-
 * ========================LICENSE_START=================================
 * axual-platform-test-core
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.powermock.api.mockito.PowerMockito.whenNew;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.net.ssl.*")
@PrepareForTest({ClusterUnit.class, KafkaUnit.class})
public class ClusterUnitTest {

    private static final String NAME = "name";
    private static final String BIND_ADDRESS = "0.0.0.0";
    private static final String ADVERTISED_ADDRESS = "1.1.1.1";

    private static final String DEFAULT_GROUP_PATTERN = "{tenant}-{instance}-{environment}-{group}";
    private static final String DEFAULT_TOPIC_PATTERN = "{tenant}-{instance}-{environment}-{topic}";

    private KafkaUnit mockKafkaUnit;

    private ClusterUnit target;

    private ClusterUnitConfig validClusterUnitConfig() {
        ClusterUnitConfig config = new ClusterUnitConfig();
        config.setName(NAME);
        config.setBindAddress(BIND_ADDRESS);
        config.setAdvertisedAddress(ADVERTISED_ADDRESS);
        config.setSslConfig(null);
        config.setGroupPattern(DEFAULT_GROUP_PATTERN);
        config.setTopicPattern(DEFAULT_TOPIC_PATTERN);

        return config;
    }

    @Before
    public void init() throws Exception {
        initMocks(this);
        mockKafkaUnit = PowerMockito.mock(KafkaUnit.class);
        whenNew(KafkaUnit.class).withAnyArguments().thenReturn(mockKafkaUnit);
        when(mockKafkaUnit.getBrokerPort()).thenReturn(1234);
        PowerMockito.doNothing().when(mockKafkaUnit).setKafkaBrokerConfig(any(), any());

        target = new ClusterUnit(validClusterUnitConfig());
    }

    @Test
    public void initClusterUnit() {
        assertEquals(NAME, target.getName());
        assertEquals(NAME, target.toString());
    }

    @Test
    public void testStart() {
        target.start();

        verify(mockKafkaUnit, times(1)).startup();
    }

    @Test
    public void testStop() {
        target.stop();

        verify(mockKafkaUnit, times(1)).shutdown();
    }

    @Test
    public void testRegisterRawStream() {
        target.registerRawTopic("stream", 2);

        verify(mockKafkaUnit, times(1)).createTopic("stream", 2);
    }
}
