package io.axual.client.proxy.consumer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy-integrationtest
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.Rule;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import io.axual.client.proxy.helper.Helper;
import io.axual.client.proxy.resolving.consumer.ResolvingConsumer;
import io.axual.client.proxy.resolving.consumer.ResolvingConsumerConfig;
import io.axual.client.proxy.resolving.generic.ResolvingClientProxyConfig;
import io.axual.client.proxy.wrapped.consumer.WrappedConsumerFactory;
import io.axual.common.config.ClientConfig;
import io.axual.common.config.CommonConfig;
import io.axual.common.resolver.GroupPatternResolver;
import io.axual.common.resolver.TopicPatternResolver;
import io.axual.platform.test.core.InstanceUnit;
import io.axual.platform.test.core.StreamConfig;
import io.axual.platform.test.junit4.SingleClusterPlatformUnit;

import static io.axual.platform.test.core.PlatformUnit.generateClusterConfig;
import static io.axual.platform.test.core.PlatformUnit.generateClusterName;
import static io.axual.platform.test.core.PlatformUnit.generateInstanceConfig;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ResolvingConsumerIT {
    private static final Logger LOG = LoggerFactory.getLogger(ResolvingConsumerIT.class);
    private static final String STREAM = "general-random-ResolvingConsumerIT";
    private static final String TEST_CONTENT = "Test content";
    private static final long TTL = 0;
    private static final int MESSAGES_TO_PRODUCE = 10;

    @Rule
    public SingleClusterPlatformUnit platform = new SingleClusterPlatformUnit(
            generateInstanceConfig(1, false),
            generateClusterConfig(generateClusterName(0)).setUseValueHeaders(true))
            .addStream(new StreamConfig().setName(STREAM));

    @Test
    public void shouldConsumeFromHardCodedProducerTopic() throws InterruptedException {
        final InstanceUnit instance = platform.instance();
        instance.getDiscoveryUnit().setTtl(TTL);

        final ClientConfig clientConfig = instance.getClientConfig("io.axual.test");

        // Set up producer and consumer context
        Map<String, Object> context = new HashMap<>();
        context.put(CommonConfig.TENANT, instance.getTenant());
        context.put(CommonConfig.INSTANCE, instance.getName());
        context.put(CommonConfig.ENVIRONMENT, InstanceUnit.DEFAULT_ENVIRONMENT);

        // Create producer
        Map<String, Object> configs = Helper.getGenericProducerConfigs(clientConfig, platform.cluster().getBootstrapServer(), "Kafka");
        Producer<String, String> producer = new KafkaProducer<>(configs);

        // Setup
        CountDownLatch latch = new CountDownLatch(2);
        ExecutorService threadPool = Executors.newFixedThreadPool(2);

        // Produce thread
        threadPool.submit(() -> {
            Thread.currentThread().setName("Producer thread");
            try {
                LOG.info("Producing.");
                ProducerRecord<String, String> producerRecord = new ProducerRecord<>(
                        platform.cluster().resolveTopic(STREAM, context),
                        TEST_CONTENT,
                        TEST_CONTENT);
                int i = MESSAGES_TO_PRODUCE - 1;
                do {
                    producer.send(producerRecord,
                            (metadata, e) -> {
                                if (e != null) {
                                    LOG.error("Exception: ", e);
                                } else {
                                    LOG.info("The offset of the record just sent is: {}", metadata.offset());
                                }
                            });
                } while (i-- > 0);
            } catch (Exception e) {
                LOG.error("Exception when producing: ", e);
            }
            latch.countDown();
        });

        // Create consumer
        configs = Helper.getGenericConsumerConfigs(clientConfig, platform.cluster().getBootstrapServer(), "Resolving");
        configs.putAll(context);
        configs.put(ResolvingConsumerConfig.GROUP_ID_RESOLVER_CONFIG, GroupPatternResolver.class.getName());
        configs.put(GroupPatternResolver.GROUP_ID_PATTERN_CONFIG, "{tenant}-{instance}-{environment}-{group}");
        configs.put(ResolvingClientProxyConfig.TOPIC_RESOLVER_CONFIG, TopicPatternResolver.class.getName());
        configs.put(TopicPatternResolver.TOPIC_PATTERN_CONFIG, "{tenant}-{instance}-{environment}-{topic}");
        configs.put(ResolvingConsumerConfig.BACKING_FACTORY_CONFIG, WrappedConsumerFactory.class.getName());
        Consumer<String, String> consumer = new ResolvingConsumer<>(configs);

        final Queue<ConsumerRecord<String, String>> queue = new LinkedBlockingQueue<>();
        // Consume thread
        threadPool.submit(() -> {
            Thread.currentThread().setName("Consumer thread");
            consumer.subscribe(Collections.singletonList(STREAM));

            while (queue.size() < 5) {
                ConsumerRecords<String, String> read = consumer.poll(500);
                read.forEach(record -> {
                    LOG.info("Consumer consumed. Offset: {}.", record.offset());
                    queue.add(record);
                });
                consumer.commitSync();
            }
            latch.countDown();
        });

        assertTrue("Expected latch to be 0 in 30 seconds, Consumers or producers not done", latch.await(30, SECONDS));


        consumer.close();
        producer.close();

        assertEquals(MESSAGES_TO_PRODUCE, queue.size());
    }
}
