package io.axual.client.proxy.producer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy-integrationtest
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.Rule;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.Queue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import io.axual.client.proxy.axual.producer.AxualProducer;
import io.axual.common.config.ClientConfig;
import io.axual.common.config.SslConfig;
import io.axual.platform.test.core.InstanceUnit;
import io.axual.platform.test.core.StreamConfig;
import io.axual.platform.test.junit4.DualClusterPlatformUnit;

import static io.axual.client.proxy.helper.Helper.createAxualConsumer;
import static io.axual.client.proxy.helper.Helper.createAxualProducer;
import static io.axual.platform.test.core.PlatformUnit.generateClusterConfig;
import static io.axual.platform.test.core.PlatformUnit.generateClusterName;
import static io.axual.platform.test.core.PlatformUnit.generateInstanceConfig;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class AxualProducerPKCS12IT {

    private static final Logger LOG = LoggerFactory.getLogger(AxualProducerIT.class);
    private static final String STREAM = "general-random-AxualProducerPKCS12IT";
    private static final String TEST_CONTENT = "Test Content";
    private static final long TTL = 0;

    @Rule
    public DualClusterPlatformUnit platform = new DualClusterPlatformUnit(
            generateInstanceConfig(2, false, SslConfig.KeystoreType.PKCS12),
            generateClusterConfig(generateClusterName(0), SslConfig.KeystoreType.PKCS12).setUseValueHeaders(true),
            generateClusterConfig(generateClusterName(1), SslConfig.KeystoreType.PKCS12).setUseValueHeaders(true))
            .addStream(new StreamConfig().setName(STREAM));

    private void produceMessages(final AxualProducer<String, String> producer, final int count)
            throws ExecutionException, InterruptedException {
        ProducerRecord<String, String> producerRecord = new ProducerRecord<>(STREAM, TEST_CONTENT, TEST_CONTENT);
        int i = count - 1;
        do {
            producer.send(producerRecord,
                    (metadata, e) -> {
                        if (e != null) {
                            LOG.error("Exception: ", e);
                        } else {
                            LOG.info("The offset of the record just sent is: {}", metadata.offset());
                        }
                    });
        } while (i-- > 0);
    }

    private Runnable getConsumerThread(final Consumer<String, String> consumer, final String name, final Queue<ConsumerRecord<String, String>> queue, CountDownLatch latch) {
        return () -> {
            Thread.currentThread().setName(String.format("Consumer %s thread", name));
            consumer.subscribe(Collections.singletonList(STREAM));

            while (queue.size() < 5) {
                ConsumerRecords<String, String> read = consumer.poll(500);
                read.forEach(record -> {
                    LOG.info("Consumer {} consumed. Offset: {}.", name, record.offset());
                    queue.add(record);
                });
                consumer.commitSync();
            }
            latch.countDown();
        };
    }

    @Test
    public void shouldProduceToOtherClusterAfterSwitch() throws InterruptedException {
        final InstanceUnit instance = platform.instance();

        final int messagesPerCluster = 5;
        instance.getDiscoveryUnit().setTtl(TTL);

        final ClientConfig clientConfigProducer = instance.getClientConfig("io.axual.test");
        instance.getDiscoveryUnit().directApplicationTo(clientConfigProducer, platform.clusterA());

        // create consumersconfig
        final ClientConfig clientConfigConsumerA = instance.getClientConfig("io.axual.testa");
        instance.getDiscoveryUnit().directApplicationTo(clientConfigConsumerA, platform.clusterA());

        final Consumer<String, String> consumerA = createAxualConsumer(clientConfigConsumerA, instance.getDiscoveryUnit().getUrl());
        final Queue<ConsumerRecord<String, String>> queueA = new LinkedBlockingQueue<>();

        final ClientConfig clientConfigConsumerB = instance.getClientConfig("io.axual.testb");
        instance.getDiscoveryUnit().directApplicationTo(clientConfigConsumerB, platform.clusterB());
        final Consumer<String, String> consumerB = createAxualConsumer(clientConfigConsumerB, instance.getDiscoveryUnit().getUrl());
        final Queue<ConsumerRecord<String, String>> queueB = new LinkedBlockingQueue<>();

        // create producer
        AxualProducer<String, String> producer = createAxualProducer(clientConfigProducer, instance.getDiscoveryUnit().getUrl());

        assertEquals(SslConfig.KeystoreType.PKCS12, clientConfigProducer.getSslConfig().getKeystoreType());
        assertEquals(SslConfig.TruststoreType.PKCS12, clientConfigProducer.getSslConfig().getTruststoreType());

        assertEquals(SslConfig.KeystoreType.PKCS12, clientConfigConsumerA.getSslConfig().getKeystoreType());
        assertEquals(SslConfig.TruststoreType.PKCS12, clientConfigConsumerA.getSslConfig().getTruststoreType());

        assertEquals(SslConfig.KeystoreType.PKCS12, clientConfigConsumerB.getSslConfig().getKeystoreType());
        assertEquals(SslConfig.TruststoreType.PKCS12, clientConfigConsumerB.getSslConfig().getTruststoreType());

        CountDownLatch latch = new CountDownLatch(3);
        ExecutorService threadPool = Executors.newFixedThreadPool(3);

        // p r o d u c e
        threadPool.submit(() -> {
            Thread.currentThread().setName("Producer thread");
            try {
                instance.directApplicationTo(clientConfigProducer, platform.clusterA());
                LOG.info("Producing.");
                produceMessages(producer, messagesPerCluster);

                LOG.info("Before switch command");
                instance.directApplicationTo(clientConfigProducer, platform.clusterB());
                LOG.info("After switch command");

                LOG.info("Producing.");
                produceMessages(producer, messagesPerCluster);
            } catch (Exception e) {
                LOG.error("Exc: ", e);
            }
            latch.countDown();
        });

        // c o n s u m e
        threadPool.submit(getConsumerThread(consumerA, "A", queueA, latch));
        threadPool.submit(getConsumerThread(consumerB, "B", queueB, latch));

        assertTrue("Expected latch to be 0 in 30 seconds, Consumers or producers not done", latch.await(30, SECONDS));

        consumerB.close();
        consumerA.close();
        producer.close();

        assertEquals(queueA.size(), queueB.size());
        assertEquals(messagesPerCluster, queueA.size());
    }
}
