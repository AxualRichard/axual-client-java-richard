package io.axual.client.proxy.helper;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy-integrationtest
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Map;

import io.axual.client.proxy.axual.consumer.AxualConsumer;
import io.axual.client.proxy.axual.consumer.AxualConsumerConfig;
import io.axual.client.proxy.axual.producer.AxualProducer;
import io.axual.client.proxy.axual.producer.AxualProducerConfig;
import io.axual.client.proxy.generic.registry.ProxyChain;
import io.axual.client.proxy.generic.registry.ProxyConfigs;
import io.axual.client.proxy.generic.registry.ProxyTypeRegistry;
import io.axual.client.proxy.logging.core.LogLevel;
import io.axual.client.proxy.logging.core.LoggingConfig;
import io.axual.client.proxy.resolving.producer.ResolvingProducerConfig;
import io.axual.client.proxy.wrapped.producer.WrappedProducerFactory;
import io.axual.common.config.ClientConfig;
import io.axual.common.tools.KafkaUtil;

public class Helper {
    public static Map<String, Object> getGenericProducerConfigs(final ClientConfig clientConfig, final String discoveryUrl, final String type) {
        Map<String, Object> configs = KafkaUtil.getKafkaConfigs(clientConfig);

        configs.put(ResolvingProducerConfig.BACKING_FACTORY_CONFIG, WrappedProducerFactory.class.getName());

        // Kafka Broker bootstrap server addresses, needed for the actual connection to Kafka brokers
        configs.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, discoveryUrl);

        // Instructs the Kafka Client which serializer to use for the values
        configs.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        // Instructs the Kafka Client which serializer to use for the keys
        configs.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        configs.put(ProducerConfig.ACKS_CONFIG, "-1");
        configs.put(ProducerConfig.MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION, "1");
        configs.put(ProducerConfig.RETRIES_CONFIG, "" + Integer.MAX_VALUE);

        // Identifies this specific cluster producer
        configs.put(ConsumerConfig.CLIENT_ID_CONFIG, "UnitTest" + type + "Producer");

        return configs;
    }

    public static AxualProducer<String, String> createAxualProducer(final ClientConfig clientConfig, final String discoveryUrl) {
        return createAxualProducer(clientConfig, discoveryUrl, LogLevel.DEBUG, LogLevel.DEBUG);
    }

    public static AxualProducer<String, String> createAxualProducer(final ClientConfig clientConfig, final String discoveryUrl, LogLevel helper1, LogLevel helper2) {
        Map<String, Object> configs = getGenericProducerConfigs(clientConfig, discoveryUrl, "Axual");

        configs.put(AxualProducerConfig.CHAIN_CONFIG, ProxyChain.newBuilder()
                .append(ProxyTypeRegistry.SWITCHING_PROXY_ID)
                .append(ProxyTypeRegistry.LOGGING_PROXY_ID, ProxyConfigs.newBuilder()
                        .add(LoggingConfig.NAME_CONFIG, "helper1")
                        .add(LoggingConfig.LOGLEVEL_CONFIG, helper1)
                        .build())
                .append(ProxyTypeRegistry.RESOLVING_PROXY_ID)
                .append(ProxyTypeRegistry.LOGGING_PROXY_ID, ProxyConfigs.newBuilder()
                        .add(LoggingConfig.NAME_CONFIG, "helper2")
                        .add(LoggingConfig.LOGLEVEL_CONFIG, helper2)
                        .build())
                .append(ProxyTypeRegistry.LINEAGE_PROXY_ID)
                .append(ProxyTypeRegistry.HEADER_PROXY_ID)
                .build());
        return new AxualProducer<>(configs);
    }

    public static Map<String, Object> getGenericConsumerConfigs(final ClientConfig clientConfig, final String discoveryUrl, final String type) {
        Map<String, Object> configs = KafkaUtil.getKafkaConfigs(clientConfig);

        // Kafka Broker bootstrap server addresses, needed for the actual connection to Kafka brokers
        configs.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, discoveryUrl);

        // Instructs the Kafka Client which serializer to use for the values
        configs.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());

        // Instructs the Kafka Client which serializer to use for the keys
        configs.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());

        // Sets the consumer group id to which this consumer belongs.
        configs.put(ConsumerConfig.GROUP_ID_CONFIG, clientConfig.getApplicationId());

        // Identifies this specific cluster consumer
        configs.put(ConsumerConfig.CLIENT_ID_CONFIG, "UnitTest" + type + "Consumer");

        // Reset the offset to earliest, otherwise will jump to end
        configs.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

        configs.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, "100");

        return configs;
    }

    public static AxualConsumer<String, String> createAxualConsumer(final ClientConfig clientConfig, final String discoveryUrl) {
        return createAxualConsumer(clientConfig, discoveryUrl, LogLevel.DEBUG, LogLevel.DEBUG);
    }

    public static AxualConsumer<String, String> createAxualConsumer(final ClientConfig clientConfig, final String discoveryUrl, LogLevel helper1, LogLevel helper2) {
        Map<String, Object> configs = getGenericConsumerConfigs(clientConfig, discoveryUrl, "Axual");

        configs.put(AxualConsumerConfig.CHAIN_CONFIG, ProxyChain.newBuilder()
                .append(ProxyTypeRegistry.SWITCHING_PROXY_ID)
                .append(ProxyTypeRegistry.LOGGING_PROXY_ID, ProxyConfigs.newBuilder()
                        .add(LoggingConfig.NAME_CONFIG, "helper1")
                        .add(LoggingConfig.LOGLEVEL_CONFIG, helper1)
                        .build())
                .append(ProxyTypeRegistry.RESOLVING_PROXY_ID)
                .append(ProxyTypeRegistry.LOGGING_PROXY_ID, ProxyConfigs.newBuilder()
                        .add(LoggingConfig.NAME_CONFIG, "helper2")
                        .add(LoggingConfig.LOGLEVEL_CONFIG, helper2)
                        .build())
                .append(ProxyTypeRegistry.LINEAGE_PROXY_ID)
                .append(ProxyTypeRegistry.HEADER_PROXY_ID)
                .build());

        return new AxualConsumer<>(configs);
    }
}
