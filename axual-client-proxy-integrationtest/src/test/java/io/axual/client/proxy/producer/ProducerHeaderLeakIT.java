package io.axual.client.proxy.producer;

/*-
 * ========================LICENSE_START=================================
 * axual-client-proxy-integrationtest
 * %%
 * Copyright (C) 2020 Axual B.V.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================LICENSE_END==================================
 */

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.LongSerializer;
import org.awaitility.Awaitility;
import org.junit.Rule;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import io.axual.client.proxy.axual.consumer.AxualConsumer;
import io.axual.client.proxy.axual.consumer.AxualConsumerConfig;
import io.axual.client.proxy.axual.producer.AxualProducer;
import io.axual.client.proxy.axual.producer.AxualProducerConfig;
import io.axual.client.proxy.generic.registry.ProxyChain;
import io.axual.client.proxy.generic.registry.ProxyChainElement;
import io.axual.client.proxy.generic.registry.ProxyTypeRegistry;
import io.axual.client.proxy.lineage.LineageHeaders;
import io.axual.client.proxy.lineage.core.LineageConfig;
import io.axual.common.config.ClientConfig;
import io.axual.common.tools.KafkaUtil;
import io.axual.platform.test.core.StreamConfig;
import io.axual.platform.test.junit4.SingleClusterPlatformUnit;

import static io.axual.platform.test.core.PlatformUnit.generateClusterConfig;
import static io.axual.platform.test.core.PlatformUnit.generateClusterName;
import static io.axual.platform.test.core.PlatformUnit.generateInstanceConfig;
import static io.axual.serde.utils.HeaderUtils.addIntegerHeader;
import static io.axual.serde.utils.HeaderUtils.addUuidHeader;
import static io.axual.serde.utils.HeaderUtils.decodeIntegerHeader;
import static io.axual.serde.utils.HeaderUtils.decodeUuidHeader;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.junit.Assert.*;

public class ProducerHeaderLeakIT {
    public static final String STREAM_NAME_INPUT_STREAM_WITH_HEADERS = "source-stream-with-header-ProducerHeaderLeakIT";
    public static final String STREAM_NAME_INPUT_STREAM_WITHOUT_HEADERS = "source-stream-without-header-ProducerHeaderLeakIT";
    public static final String APP_ID_VERIFIER = "copyheaderleakit-verifier";
    public static final String APP_ID_PRODUCER_WITH_HEADERS = "copyheaderleakit-producer-stream-with-headers";
    public static final String APP_ID_PRODUCER_WITHOUT_HEADERS = "copyheaderleakit-producer-stream-without-headers";
    public static final int COPY_FLAG_STREAM = 0b00000001;
    public static final int PARTITIONS = 1;
    public static final ProxyChain PROXY_CHAIN_VERIFIER = ProxyChain.newBuilder()
            .append(ProxyTypeRegistry.SWITCHING_PROXY_ID)
            .append(ProxyTypeRegistry.RESOLVING_PROXY_ID)
            .build();
    private static final Logger LOG = LoggerFactory.getLogger(ProducerHeaderLeakIT.class);
    @Rule
    public SingleClusterPlatformUnit platformUnit = new SingleClusterPlatformUnit(
            generateInstanceConfig(1, false),
            generateClusterConfig(generateClusterName(0)).setUseValueHeaders(true))
            .addStream(new StreamConfig()
                    .setName(STREAM_NAME_INPUT_STREAM_WITH_HEADERS).setPartitions(PARTITIONS))
            .addStream(new StreamConfig()
                    .setName(STREAM_NAME_INPUT_STREAM_WITHOUT_HEADERS).setPartitions(PARTITIONS));

    private <K, V> Thread createVerifierThread(String stream,
                                               ClientConfig clientConfig,
                                               AtomicBoolean keepRunnning,
                                               AtomicBoolean isRunnning,
                                               LinkedList<ConsumerRecord<K, V>> consumedRecords,
                                               Deserializer<K> keyDeserializer, Deserializer<V> valueDeserializer) {
        isRunnning.set(false);
        return new Thread(() -> {
            Thread.currentThread().setName(String.format("Verifier-%s", stream));
            LOG.info("Verifier stream {}: start, {} messages in list", stream, consumedRecords.size());

            Map<String, Object> consumerConfig = KafkaUtil.getKafkaConfigs(clientConfig);
            consumerConfig.put(AxualConsumerConfig.CHAIN_CONFIG, PROXY_CHAIN_VERIFIER);
            consumerConfig.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
            consumerConfig.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");
            consumerConfig.put(ConsumerConfig.ALLOW_AUTO_CREATE_TOPICS_CONFIG, "false");

            try (AxualConsumer<K, V> consumer = new AxualConsumer<K, V>(consumerConfig, keyDeserializer, valueDeserializer)) {
                List<PartitionInfo> streamPartitions = consumer.partitionsFor(stream);
                List<TopicPartition> toAssign = streamPartitions.stream().map((a) -> new TopicPartition(a.topic(), a.partition())).collect(Collectors.toList());
                consumer.assign(toAssign);
                consumer.seekToEnd(toAssign);
                while (keepRunnning.get()) {
                    ConsumerRecords<K, V> records = consumer.poll(Duration.ofMillis(1000));
                    for (ConsumerRecord<K, V> record : records) {
                        consumedRecords.add(record);
                    }
                    LOG.info("Verifier stream {}: Consumed {} new messages, total consumed {}", stream, records.count(), consumedRecords.size());
                    consumer.commitSync();
                    isRunnning.set(true);
                }
            }

            isRunnning.set(false);
            LOG.info("Verifier stream {}: end, consumed {} messages", stream, consumedRecords.size());
        });
    }

    @Test
    public void testProduceWithCustomHeaders() throws Exception {
        // Start proxy consumer using PROXY_CHAIN_VERIFIER listening on STREAM_NAME_OUTPUT_STREAM_WITHOUT_HEADERS, store all Consumer Record Object received in LinkedList
        // Start Producer for STREAM_NAME_INPUT_STREAM_WITH_HEADERS
        // Produce 10 messaged in iteration with payload: Key Long iter, Value Long iter, MessageId new UUID(iter,iter), CopyFlag COPY_FLAG_STREAM
        // Wait for at most 30 seconds to receive the messages with the consumer.
        // Verify that the received messages are not using these headers
        final String source = STREAM_NAME_INPUT_STREAM_WITH_HEADERS;
        final String target = STREAM_NAME_INPUT_STREAM_WITH_HEADERS;

        final LinkedList<ProducerRecord<Long, Long>> producerRecords = new LinkedList<>();
        final LinkedList<ConsumerRecord<Long, Long>> consumerRecords = new LinkedList<>();

        final ClientConfig verifierClientConfig = platformUnit.instance().getClientConfig(APP_ID_VERIFIER, false);
        final AtomicBoolean verifierKeepRunning = new AtomicBoolean(true);
        final AtomicBoolean verifierIsRunning = new AtomicBoolean(false);
        final Thread verifierThread = createVerifierThread(target, verifierClientConfig, verifierKeepRunning, verifierIsRunning, consumerRecords, new LongDeserializer(), new LongDeserializer());
        verifierThread.setDaemon(false);
        verifierThread.start();

        Awaitility.await("Wait for consumer to start")
                .atMost(30, SECONDS)
                .pollInterval(100, MILLISECONDS)
                .until(() -> verifierIsRunning.get());


        try {
            final Map<String, Object> lineageConfig = new HashMap<>(2);
            lineageConfig.put(LineageConfig.ENABLE_SYSTEM_PRODUCE, true);
            final ProxyChain proxyChainProducer = ProxyChain.newBuilder()
                    .append(ProxyTypeRegistry.SWITCHING_PROXY_ID)
                    .append(ProxyTypeRegistry.RESOLVING_PROXY_ID)
                    .append(ProxyChainElement.newBuilder()
                            .setProxyId(ProxyTypeRegistry.LINEAGE_PROXY_ID)
                            .setConfigs(lineageConfig)
                            .build())
                    .build();

            final ClientConfig producerClientConfig = platformUnit.instance().getClientConfig(APP_ID_PRODUCER_WITH_HEADERS, false);
            final Map<String, Object> producerConfig = KafkaUtil.getKafkaConfigs(producerClientConfig);
            producerConfig.put(AxualProducerConfig.CHAIN_CONFIG, proxyChainProducer);
            producerConfig.put(ProducerConfig.ACKS_CONFIG, "all");
            producerConfig.put(ProducerConfig.RETRIES_CONFIG, "0");
            try (Producer<Long, Long> producer = new AxualProducer<>(producerConfig, new LongSerializer(), new LongSerializer())) {
                final long timeout = System.currentTimeMillis() + 15000;// produce should not take longer than 15 seconds
                final AtomicLong counter = new AtomicLong(1);
                while (System.currentTimeMillis() < timeout && counter.get() <= 10) {
                    final Long count = counter.get();
                    ProducerRecord<Long, Long> producerRecord = new ProducerRecord<>(source, count, count);
                    addUuidHeader(producerRecord.headers(), LineageHeaders.MESSAGE_ID_HEADER, new UUID(count, count));
                    addIntegerHeader(producerRecord.headers(), LineageHeaders.COPY_FLAGS_HEADER, COPY_FLAG_STREAM);
                    Future<RecordMetadata> future = producer.send(producerRecord);
                    try {
                        future.get();
                        LOG.info("Producer {} - Produce of {} successful, continuing", source, count);
                        counter.incrementAndGet();
                        producerRecords.add(producerRecord);
                    } catch (InterruptedException | ExecutionException e) {
                        LOG.info("Producer {} - Produce of {} failed, retrying", source, count);
                    }
                }
                LOG.info("Producer {} - Ending produce loop with counter at {}", source, counter.get());
            }

            Awaitility.await("Wait for consumed messages to catch up to produced messages")
                    .atMost(30, SECONDS)
                    .pollInterval(100, MILLISECONDS)
                    .until(() -> consumerRecords.size() >= producerRecords.size());


        } finally {
            verifierKeepRunning.set(false);
            verifierThread.join();
        }

        assertFalse("Produced records list should not be empty", producerRecords.isEmpty());
        assertFalse("Consumed records list should not be empty", consumerRecords.isEmpty());
        assertThat("The produced records list should contain the same or less messages than the consumed records list", producerRecords.size(), lessThanOrEqualTo(consumerRecords.size()));
        for (ProducerRecord<Long, Long> producerRecord : producerRecords) {
            // for each record, find the consumer record
            final Long producedValue = producerRecord.value();

            Optional<ConsumerRecord<Long, Long>> foundCr = consumerRecords.stream().filter(cr -> producedValue == cr.value()).findFirst();
            assertTrue(String.format("No consumed record for value %d could be found", producedValue), foundCr.isPresent());
            final Header consumedMessageId = foundCr.get().headers().lastHeader(LineageHeaders.MESSAGE_ID_HEADER);
            if (consumedMessageId != null) {
                final Header producedMessageId = producerRecord.headers().lastHeader(LineageHeaders.MESSAGE_ID_HEADER);
                UUID producedId = new UUID(producedValue, producedValue);
                UUID consumedId = decodeUuidHeader(consumedMessageId);
                assertEquals("Message ID should be the same", producedId, consumedId);
            }

            final Header consumedCopyFlag = foundCr.get().headers().lastHeader(LineageHeaders.COPY_FLAGS_HEADER);
            if (consumedCopyFlag != null) {
                Integer notExpected = COPY_FLAG_STREAM;
                Integer copyFlag = decodeIntegerHeader(consumedCopyFlag);
                assertEquals("Copy Flag should be the same", notExpected, copyFlag);
            }
        }

    }

    @Test
    public void testProduceWithoutCustomHeaders() throws Exception {
        // Start proxy consumer using PROXY_CHAIN_VERIFIER listening on STREAM_NAME_OUTPUT_STREAM_WITHOUT_HEADERS, store all Consumer Record Object received in LinkedList
        // Start Producer for STREAM_NAME_INPUT_STREAM_WITH_HEADERS
        // Produce 10 messaged in iteration with payload: Key Long iter, Value Long iter, MessageId new UUID(iter,iter), CopyFlag COPY_FLAG_STREAM
        // Wait for at most 30 seconds to receive the messages with the consumer.
        // Verify that the received messages are not using these headers
        final String source = STREAM_NAME_INPUT_STREAM_WITHOUT_HEADERS;
        final String target = STREAM_NAME_INPUT_STREAM_WITHOUT_HEADERS;

        final LinkedList<ProducerRecord<Long, Long>> producerRecords = new LinkedList<>();
        final LinkedList<ConsumerRecord<Long, Long>> consumerRecords = new LinkedList<>();

        final ClientConfig verifierClientConfig = platformUnit.instance().getClientConfig(APP_ID_VERIFIER, false);
        final AtomicBoolean verifierKeepRunning = new AtomicBoolean(true);
        final AtomicBoolean verifierIsRunning = new AtomicBoolean(false);
        final Thread verifierThread = createVerifierThread(target, verifierClientConfig, verifierKeepRunning, verifierIsRunning, consumerRecords, new LongDeserializer(), new LongDeserializer());
        verifierThread.setDaemon(false);
        verifierThread.start();

        Awaitility.await("Wait for consumer to start")
                .atMost(30, SECONDS)
                .pollInterval(100, MILLISECONDS)
                .until(() -> verifierIsRunning.get());

        try {
            final Map<String, Object> lineageConfig = new HashMap<>(2);
            lineageConfig.put(LineageConfig.ENABLE_SYSTEM_PRODUCE, false);
            final ProxyChain proxyChainProducer = ProxyChain.newBuilder()
                    .append(ProxyTypeRegistry.SWITCHING_PROXY_ID)
                    .append(ProxyTypeRegistry.RESOLVING_PROXY_ID)
                    .append(ProxyChainElement.newBuilder()
                            .setProxyId(ProxyTypeRegistry.LINEAGE_PROXY_ID)
                            .setConfigs(lineageConfig)
                            .build())
                    .build();

            final ClientConfig producerClientConfig = platformUnit.instance().getClientConfig(APP_ID_PRODUCER_WITHOUT_HEADERS, false);
            final Map<String, Object> producerConfig = KafkaUtil.getKafkaConfigs(producerClientConfig);
            producerConfig.put(AxualProducerConfig.CHAIN_CONFIG, proxyChainProducer);
            producerConfig.put(ProducerConfig.ACKS_CONFIG, "all");
            producerConfig.put(ProducerConfig.RETRIES_CONFIG, "0");
            try (Producer<Long, Long> producer = new AxualProducer<>(producerConfig, new LongSerializer(), new LongSerializer())) {
                final long timeout = System.currentTimeMillis() + 15000;// produce should not take longer than 15 seconds
                final AtomicLong counter = new AtomicLong(1);
                while (System.currentTimeMillis() < timeout && counter.get() <= 10) {
                    final Long count = counter.get();
                    ProducerRecord<Long, Long> producerRecord = new ProducerRecord<>(source, count, count);
                    addUuidHeader(producerRecord.headers(), LineageHeaders.MESSAGE_ID_HEADER, new UUID(count, count));
                    addIntegerHeader(producerRecord.headers(), LineageHeaders.COPY_FLAGS_HEADER, COPY_FLAG_STREAM);
                    Future<RecordMetadata> future = producer.send(producerRecord);
                    try {
                        future.get();
                        LOG.info("Producer {} - Produce of {} successful, continuing", source, count);
                        counter.incrementAndGet();
                        producerRecords.add(producerRecord);
                    } catch (InterruptedException | ExecutionException e) {
                        LOG.info("Producer {} - Produce of {} failed, retrying", source, count);
                    }
                }
                LOG.info("Producer {} - Ending produce loop with counter at {}", source, counter.get());
            }

            Awaitility.await("Wait for consumed messages to catch up to produced messages")
                    .atMost(30, SECONDS)
                    .pollInterval(100, MILLISECONDS)
                    .until(() -> consumerRecords.size() >= producerRecords.size());


        } finally {
            verifierKeepRunning.set(false);
            verifierThread.join();
        }

        assertFalse("Produced records list should not be empty", producerRecords.isEmpty());
        assertFalse("Consumed records list should not be empty", consumerRecords.isEmpty());
        assertThat("The produced records list should contain the same or less messages than the consumed records list", producerRecords.size(), lessThanOrEqualTo(consumerRecords.size()));
        for (ProducerRecord<Long, Long> producerRecord : producerRecords) {
            // for each record, find the consumer record
            final Long producedValue = producerRecord.value();

            Optional<ConsumerRecord<Long, Long>> foundCr = consumerRecords.stream().filter(cr -> producedValue == cr.value()).findFirst();
            assertTrue(String.format("No consumed record for value %d could be found", producedValue), foundCr.isPresent());
            final Header consumedMessageId = foundCr.get().headers().lastHeader(LineageHeaders.MESSAGE_ID_HEADER);
            if (consumedMessageId != null) {
                UUID producedId = new UUID(producedValue, producedValue);
                UUID consumedId = decodeUuidHeader(consumedMessageId);
                assertNotEquals("Message ID should not be the same", producedId, consumedId);
            }

            final Header consumedCopyFlag = foundCr.get().headers().lastHeader(LineageHeaders.COPY_FLAGS_HEADER);
            if (consumedCopyFlag != null) {
                Integer notExpected = COPY_FLAG_STREAM;
                Integer copyFlag = decodeIntegerHeader(consumedCopyFlag);
                assertNotEquals("Copy Flag should not be the same", notExpected, copyFlag);
            }
        }
    }
}
