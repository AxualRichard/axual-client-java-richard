Axual Java Client
--------

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)
[![Pipeline Status](https://gitlab.com/axual-public/axual-client-java/badges/master/pipeline.svg)](https://gitlab.com/axual-public/axual-client-java/commits/master) 
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=axual-public-axual-client-java&metric=coverage&token=f69e2fc57855c1208bc5c604e1eb83b9b1d59002)](https://sonarcloud.io/dashboard?id=axual-public-axual-client-java)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=axual-public-axual-client-java&metric=sqale_rating&token=f69e2fc57855c1208bc5c604e1eb83b9b1d59002)](https://sonarcloud.io/dashboard?id=axual-public-axual-client-java)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=axual-public-axual-client-java&metric=alert_status&token=f69e2fc57855c1208bc5c604e1eb83b9b1d59002)](https://sonarcloud.io/dashboard?id=axual-public-axual-client-java)

Java client library that enables interaction with the Axual platform.

```plantuml
!include <cloudinsight/kafka>

'
' copied inline here from the plantuml/ folder.
'

frame "Producer app" as producer {
	component "Axual client" as prodclient
}
frame "Consumer app" as consumer {
	component "Axual client" as consclient
}

frame "Axual" as axual {

	component "Discovery API" as disco
	component "Schema registry" as registry
	queue "<$kafka>" as kafka1
	queue "<$kafka>" as kafka2

	disco -[hidden]-> registry
	registry -[hidden]-> kafka1
	kafka1 --> kafka2
	kafka2 --> kafka1

}

prodclient <-> disco
prodclient <-> registry
prodclient <-> kafka1
registry <-> consclient
disco <-> consclient
consclient <-> kafka2
```

## Prerequisites
You need to have [Java](http://www.oracle.com/technetwork/java/javase/downloads/index.html) 
and [Maven](https://maven.apache.org/download.cgi) installed.

Java 8 should be used for building in order to support both Java 8 and Java 11 at runtime.

Maven 3 is used to build the project.

## Installation
To install (skipping tests) run:
~~~
mvn clean install -DskipTests=True
~~~

## Testing
To run all unit tests under the project, run:
~~~
mvn test
~~~
For integration tests only:
~~~
mvn integration-test
~~~
For full verification of unit and integration tests:
~~~
mvn verify
~~~

## Project Overview
The project is divided into modules based functionality in order to be included separately depending
on the use case.

The submodules are as follows:

* [`axual-client`](axual-client/) 
  the high level client, provides a layer of abstraction over the Kafka internals

* [`axual-client-integrationtest`](axual-client-integrationtest/) 
  includes end-to-end tests for the high level client

* [`axual-client-proxy`](axual-client-proxy/) 
  pluggable proxy functionality logic that can be used as an alternative to the high level client

* [`axual-client-proxy-integrationtest`](axual-client-proxy-integrationtest/) 
  includes end-to-end tests for the proxy client

* ~~axual-client-spring~~: includes the spring wrapper around the client. Currently not maintained

* `axual-client-test-schema` contains a set of `avro` schemas that can be used for testing purposes

* [`axual-common`](axual-common/) 
  shared utility module

* [`axual-common-test`](axual-common-test/) 
  shared utility module imported in test scope

* [`axual-discovery-client`](axual-discovery-client/) 
  discovery-api scraping logic for client cluster routing. See 
  [`README.md`](axual-discovery-client/README.md) for more details on the service.

* `axual-platform-test-core` core implementation of the axual test platform

* `axual-platform-test-junit4` Provides the axual test platform as JUnit4 ExternalResource

* `axual-platform-test-jupiter` Provides the axual test platform as JUnit5/Jupiter extension

* [`axual-platform-test-standalone`](axual-platform-test-standalone/) 
  mocks the behavior of core platform services for testing purposes

* [`axual-serde`](axual-serde/)
  encapsulates serializer/deserializer functionalities for Kafka messages

* `axual-serde-test-schema`
  contains a set of `avro` schemas that can be used for testing of serde module

* [`axual-streams`](axual-streams/)
  axual wrapper around Kafka Streams functionality

* [`axual-streams-integrationtest`](axual-streams-integrationtest/) 
  includes end-to-end tests for axual streams

* [`axual-streams-proxy`](axual-streams-proxy/)
  pluggable proxy functionality logic that can be used as an alternative to the high level streams 
  client
  
* [`report`](report/)
   aggregates code coverage reports

## Usage
Documentation on how to use the client can be found in the 
[Axual Documentation](https://docs.axual.io/client/5.8.1/index.html).

## Examples
Simple use cases using the client code can be found in the 
[Axual Client Java Examples](https://gitlab.com/axual-public/axual-client-java-examples).

### Contributing ###

Axual is interested in building the community; we would welcome any thoughts or 
[patches](https://gitlab.com/axual-public/axual-client-java/-/issues).
You can reach us [here](https://axual.com/contact/).

See [contributing](https://gitlab.com/axual-public/axual-client-java/blob/master/CONTRIBUTING.md).
  